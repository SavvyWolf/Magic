use actions::Action;
use filter::{filter_to_json, json_to_filter, run_filter, FilterList};
use game::Game;
use gamestate::RcGameObject;
use rules::get_actions_registry;

use actions::Usability;
use json::{array, object, JsonValue};
use parse::ParseError;
use std::str::FromStr;
use strum_macros::{Display, EnumString};

/// A "Speed" for an activated ability
#[derive(EnumString, Display, Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Speed {
    ManaAbility,
    Instant,
    Sorcery,
}

/// An activated ability consists of a cost and an action
///
/// If the cost is paid, the action may be added to the action stack.
///
/// ActivatedAbilities are object-independant, any time they are activated they will be provided an object by the caller.
pub struct ActivatedAbility {
    /// The restriction on this activated ability (will only be usable if this filter isn't empty
    pub restriction: FilterList,
    /// The cost which must be paid to use this ActivatedAbility
    pub cost: Vec<Box<dyn Action>>,
    /// The actions pushed onto the action stack as a result of this ActivatedAbility
    pub action: Box<dyn Action>,
    /// The "Speed" of this ActivatedAbility, it may only activated at this speed
    pub speed: Speed,
}

impl Clone for ActivatedAbility {
    fn clone(&self) -> ActivatedAbility {
        ActivatedAbility {
            restriction: self.restriction.clone(),
            action: self.action.box_clone(),
            cost: self.cost.clone(),
            speed: self.speed,
        }
    }
}

impl ActivatedAbility {
    pub fn new(
        restriction: FilterList,
        cost: Vec<Box<dyn Action>>,
        action: Box<dyn Action>,
        speed: Speed,
    ) -> ActivatedAbility {
        ActivatedAbility {
            restriction,
            cost,
            action,
            speed,
        }
    }

    /// Given a game instance and a game object, will return a string description of this ActivatedAbility
    pub fn cost_description(&self, game: &Game, this: Option<&RcGameObject>) -> String {
        let mut hold = String::new();
        let mut first = true;
        for c in self.cost.iter() {
            if !first {
                hold.push_str(", ");
            }
            first = false;
            hold.push_str(&c.description(game, this, true));
        }
        hold
    }

    /// Returns true iff all this ActivatedAbility's costs can be performed and restrictions are met
    pub fn get_usability(&self, game: &Game, object: Option<&RcGameObject>) -> Usability {
        let mut usability = Usability::Usable;

        // Are restrictions met
        if run_filter(&self.restriction, game, object, Default::default())
            .0
            .is_empty()
        {
            return Usability::Unusable;
        }

        // Can costs be paid
        for c in &self.cost {
            match c.get_usability(game, object) {
                Usability::Usable => (),
                Usability::Unlikely => usability = Usability::Unlikely,
                Usability::Unusable => return Usability::Unusable,
            }
        }
        usability
    }

    pub fn to_json(&self) -> JsonValue {
        let mut cost = JsonValue::new_array();
        for c in &self.cost {
            cost.push(c.to_json()).expect("Cost is invalid");
        }

        let restriction = filter_to_json(&self.restriction);
        let action = array![self.action.to_json()];
        let speed = JsonValue::String(format!("{}", self.speed));

        object! {
            "restriction" => restriction,
            "cost" => cost,
            "action" => action,
            "speed" => speed
        }
    }

    pub fn from_json(json: &JsonValue) -> Result<ActivatedAbility, ParseError> {
        let areg = get_actions_registry();

        let mut cost = vec![];
        for c in json["cost"].members() {
            cost.push(areg.load_action(c)?);
        }

        let restriction = json_to_filter(&json["restriction"])?;
        let action = areg.load_action(&json["action"][0])?;
        let speed = Speed::from_str(
            &json["speed"]
                .as_str()
                .ok_or(ParseError::WrongType("speed"))?,
        )?;

        Ok(ActivatedAbility::new(restriction, cost, action, speed))
    }
}
