#![allow(dead_code)]

use super::StaticEffect;
use gamestate::RcGameObject;

pub type Layer = usize;
pub const COPY_EFFECTS: Layer = 0;
pub const CONTROL_CHANGING_EFFECTS: Layer = 1;
pub const TEXT_CHANGING_EFFECTS: Layer = 2;
pub const TYPE_CHANGING_EFFECTS: Layer = 3;
pub const COLOUR_CHANGING_EFFECTS: Layer = 4;
pub const ABILITY_ADDING_EFFECTS: Layer = 5;
pub const POWER_TOUGHNESS_CHANGING_EFFECTS: Layer = 6;
pub const ENGINE_COLLECT_ABILITIES: Layer = 7;

pub const NUM_LAYERS: usize = 8;

pub type LayerMap = [Vec<(RcGameObject, Box<dyn StaticEffect>)>; NUM_LAYERS];
