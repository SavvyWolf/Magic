use super::layers;
use actions::ActionEntry;
use actions::ExpectedUserInput;
use game::Game;
use gamestate::{PlayerId, RcGameObject};
use json::JsonValue;
use parse::{ParseError, TypedJson};
use rules::ReplacementEffect;

pub trait SEBoxClone {
    /// Return a Box containing a clone of this StaticEffect
    fn box_clone(&self) -> Box<dyn StaticEffect>;
}

pub trait StaticEffect: TypedJson + SEBoxClone {
    #[cfg_attr(tarpaulin, skip)]
    fn description(&self, _game: &Game) -> String {
        "(Unknown Static Effect)".to_owned()
    }
    fn modify_board(&self, _game: &mut Game, _source: &RcGameObject) {}
    fn get_actions(
        &self,
        _game: &Game,
        _source: &RcGameObject,
        _expected: ExpectedUserInput,
        _player: PlayerId,
    ) -> Vec<ActionEntry> {
        vec![]
    }
    fn layer(&self) -> layers::Layer;
    fn as_replacement_effect(&self) -> Option<&ReplacementEffect> {
        None
    }
    fn load_json(&self, _json: &JsonValue) -> Result<Box<dyn StaticEffect>, ParseError> {
        Ok(self.box_clone())
    }
}

impl Clone for Box<dyn StaticEffect> {
    fn clone(&self) -> Box<dyn StaticEffect> {
        self.box_clone()
    }
}
