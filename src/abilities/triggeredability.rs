use actions::{Action, ActionHookType};
use filter::{
    filter_to_json, json_to_filter, run_filter_on, sstype, FilterList, PlaceholderData,
    SelectionSet,
};
use game::Game;
use gamestate::RcGameObject;
use parse::ParseError;
use rules::get_actions_registry;

use std::str::FromStr;

use json::{object, JsonValue};

#[derive(Clone)]
pub struct TriggeredAbility {
    pub this_filter: FilterList,
    pub watch: FilterList,
    pub watch_type: ActionHookType,
    pub payload: Vec<Box<dyn Action>>,
}

impl TriggeredAbility {
    pub fn new(
        this_filter: FilterList,
        watch: FilterList,
        watch_type: ActionHookType,
        payload: Vec<Box<dyn Action>>,
    ) -> Self {
        Self {
            this_filter,
            watch,
            watch_type,
            payload,
        }
    }

    pub fn does_match(
        &self,
        game: &Game,
        source: Option<&RcGameObject>,
        set: SelectionSet<sstype::Resolved>,
        data: PlaceholderData,
    ) -> Option<PlaceholderData> {
        let (this_objects, _) = run_filter_on(
            &self.this_filter,
            game,
            source,
            SelectionSet::new_gameobject(source.unwrap().clone()),
            Default::default(),
        );
        if this_objects.is_empty() {
            return None;
        }

        let (objects, data) = run_filter_on(&self.watch, game, source, set, data);

        if objects.is_empty() {
            None
        } else {
            Some(data)
        }
    }

    pub fn to_json(&self) -> JsonValue {
        let this_filter = filter_to_json(&self.this_filter);
        let watch = filter_to_json(&self.watch);
        let watch_type = format!("{}", self.watch_type);
        let mut payload = JsonValue::new_array();
        for a in &self.payload {
            payload
                .push(a.to_json())
                .expect("Triggered ability action is invalid");
        }

        object! {
            "this_filter" => this_filter,
            "watch" => watch,
            "watch_type" => watch_type,
            "payload" => payload
        }
    }

    pub fn from_json(json: &JsonValue) -> Result<TriggeredAbility, ParseError> {
        let areg = get_actions_registry();

        let mut payload = vec![];
        for a in json["payload"].members() {
            payload.push(areg.load_action(a)?);
        }

        let this_filter = json_to_filter(&json["this_filter"])?;
        let watch = json_to_filter(&json["watch"])?;
        let watch_type = ActionHookType::from_str(
            &json["watch_type"]
                .as_str()
                .ok_or(ParseError::WrongType("watch_type"))?,
        )?;

        Ok(TriggeredAbility::new(
            this_filter,
            watch,
            watch_type,
            payload,
        ))
    }
}
