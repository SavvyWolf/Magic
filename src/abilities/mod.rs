mod activatedability;
pub mod layers;
mod staticeffect;
mod triggeredability;

pub use self::activatedability::ActivatedAbility;
pub use self::activatedability::Speed;
pub use self::staticeffect::SEBoxClone;
pub use self::staticeffect::StaticEffect;
pub use self::triggeredability::TriggeredAbility;

use actions::ActionEntry;
use actions::ExpectedUserInput;
use game::Game;
use gamestate::PlayerId;

use std::convert::TryInto;

pub fn get_actions(
    game: &Game,
    effects: layers::LayerMap,
    expected: ExpectedUserInput,
    player: PlayerId,
) -> (isize, Vec<ActionEntry>) {
    let mut out = vec![];
    for layer in effects.iter() {
        for (obj, se) in layer {
            out.append(&mut (*se).get_actions(game, obj, expected, player));
        }
    }
    (player.try_into().unwrap(), out)
}

pub fn update(game: &mut Game, effects: layers::LayerMap) {
    for layer in effects.iter() {
        for (obj, se) in layer {
            se.modify_board(game, obj);
        }
    }
}
