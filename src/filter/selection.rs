use filter::{Placeholder, PlaceholderData};
use gamestate::PlayerId;
use gamestate::RcGameObject;
use parse::ParseError;

use std::iter::FromIterator;
use std::marker::PhantomData;
use std::slice::Iter;
use std::str::FromStr;

use json::JsonValue;
use strum_macros::{Display, EnumString};

#[derive(EnumString, Display, Clone, Debug, PartialEq, Eq, Hash)]
pub enum SelectableKind {
    Player,
    Value,
    GameObject,
    Placeholder,
    Template,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Selectable {
    Player(PlayerId),
    Value(i32),
    GameObject(RcGameObject),
    Placeholder(Placeholder),
    Template(Placeholder),
}

impl Selectable {
    pub fn is_resolved(&self) -> bool {
        match self {
            Self::Player(_) => true,
            Self::Value(_) => true,
            Self::GameObject(_) => true,
            Self::Placeholder(_) => false,
            Self::Template(_) => false,
        }
    }

    pub fn kind(&self) -> SelectableKind {
        match self {
            Self::Player(_) => SelectableKind::Player,
            Self::Value(_) => SelectableKind::Value,
            Self::GameObject(_) => SelectableKind::GameObject,
            Self::Placeholder(_) => SelectableKind::Placeholder,
            Self::Template(_) => SelectableKind::Template,
        }
    }

    pub fn unwrap_player(&self) -> PlayerId {
        match self {
            Self::Player(p) => *p,
            _ => panic!("Tried to unwrap a non-player Selectable as a player"),
        }
    }

    pub fn unwrap_value(&self) -> i32 {
        match self {
            Self::Value(v) => *v,
            _ => panic!("Tried to unwrap a non-value Selectable as a value"),
        }
    }

    pub fn unwrap_gameobject(&self) -> &RcGameObject {
        match self {
            Self::GameObject(go) => go,
            _ => panic!("Tried to unwrap a non-gameobject Selectable as a gameobject"),
        }
    }

    #[allow(clippy::trivially_copy_pass_by_ref)]
    pub fn resolve(&self, data: &PlaceholderData) -> SelectionSet<sstype::Resolved> {
        match self {
            Self::Player(_) => SelectionSet::new(self.clone()),
            Self::Value(_) => SelectionSet::new(self.clone()),
            Self::GameObject(_) => SelectionSet::new(self.clone()),
            Self::Placeholder(p) => data.get(p).unwrap().clone(),
            Self::Template(_) => panic!("Tried to resolve a template"),
        }
    }

    pub fn apply_template(self, data: &PlaceholderData) -> SelectionSet<sstype::Resolved> {
        match self {
            Self::Player(_) => SelectionSet::new(self),
            Self::Value(_) => SelectionSet::new(self),
            Self::GameObject(_) => SelectionSet::new(self),
            Self::Placeholder(_) => SelectionSet::new(self),
            Self::Template(t) => data.get(&t).unwrap().clone(),
        }
    }

    pub fn to_json(&self) -> JsonValue {
        match self {
            Self::Player(p) => JsonValue::String(format!("P{}", p)),
            Self::Value(v) => JsonValue::String(format!("%{}", v)),
            Self::GameObject(_) => panic!("Tried to serialize a game object!"),
            Self::Placeholder(p) => JsonValue::String(format!("{}", p)),
            Self::Template(t) => JsonValue::String(format!("#{}", t)),
        }
    }

    pub fn from_json(obj: &str) -> Result<Self, ParseError> {
        match obj.chars().nth(0).unwrap() {
            'P' => Ok(Self::Player(PlayerId::from_str(&obj[1..])?)),
            '%' => Ok(Self::Value(i32::from_str(&obj[1..])?)),
            '$' => Ok(Self::Placeholder(Placeholder::from_str(obj)?)),
            '#' => Ok(Self::Template(Placeholder::from_str(&obj[1..])?)),
            _ => Err(ParseError::InvalidValue(
                "Selectable doesn't start with a special character",
            )),
        }
    }
}

pub mod sstype {
    use super::Selectable;

    pub trait SSType: Clone + Copy + Default + Eq + PartialEq {
        fn check(sel: &Selectable) -> bool;
    }

    #[derive(Clone, Copy, Default, Debug, Eq, PartialEq)]
    pub struct Resolved {}
    impl SSType for Resolved {
        fn check(sel: &Selectable) -> bool {
            match sel {
                Selectable::GameObject(_) => true,
                Selectable::Placeholder(_) => false,
                Selectable::Player(_) => true,
                Selectable::Template(_) => false,
                Selectable::Value(_) => true,
            }
        }
    }

    #[derive(Clone, Copy, Default, Debug, Eq, PartialEq)]
    pub struct Serial {}
    impl SSType for Serial {
        fn check(sel: &Selectable) -> bool {
            match sel {
                Selectable::GameObject(_) => false,
                Selectable::Placeholder(_) => true,
                Selectable::Player(_) => true,
                Selectable::Template(_) => true,
                Selectable::Value(_) => true,
            }
        }
    }

    #[derive(Clone, Copy, Default, Debug, Eq, PartialEq)]
    pub struct NoTemplate {}
    impl SSType for NoTemplate {
        fn check(sel: &Selectable) -> bool {
            match sel {
                Selectable::GameObject(_) => true,
                Selectable::Placeholder(_) => true,
                Selectable::Player(_) => true,
                Selectable::Template(_) => false,
                Selectable::Value(_) => true,
            }
        }
    }
}

#[derive(Default, Debug, Clone, Eq)]
pub struct SelectionSet<T: sstype::SSType> {
    set: Vec<Selectable>,
    has_placeholders: bool,
    has_templates: bool,
    type_phantom: PhantomData<T>,
}

impl<T: sstype::SSType> SelectionSet<T> {
    pub fn new(sel: Selectable) -> Self {
        let mut out: SelectionSet<T> = Default::default();
        out.insert(sel);
        out
    }

    pub fn new_gameobject(go: RcGameObject) -> Self {
        SelectionSet {
            set: vec![Selectable::GameObject(go)],
            ..Default::default()
        }
    }

    pub fn new_player(player: PlayerId) -> Self {
        SelectionSet {
            set: vec![Selectable::Player(player)],
            ..Default::default()
        }
    }

    pub fn new_value(value: i32) -> Self {
        SelectionSet {
            set: vec![Selectable::Value(value)],
            ..Default::default()
        }
    }

    pub fn new_placeholder(placeholder: Placeholder) -> Self {
        SelectionSet {
            set: vec![Selectable::Placeholder(placeholder)],
            has_placeholders: true,
            ..Default::default()
        }
    }

    pub fn new_template(placeholder: Placeholder) -> Self {
        SelectionSet {
            set: vec![Selectable::Template(placeholder)],
            has_templates: true,
            ..Default::default()
        }
    }

    pub fn insert(&mut self, selection: Selectable) {
        assert!(T::check(&selection));
        match selection {
            Selectable::Template(_) => {
                self.has_templates = true;
            }
            Selectable::Placeholder(_) => {
                self.has_placeholders = true;
            }
            _ => {}
        }
        self.set.push(selection);
    }

    pub fn insert_gameobject(&mut self, go: RcGameObject) {
        self.insert(Selectable::GameObject(go))
    }

    pub fn insert_player(&mut self, player: PlayerId) {
        self.insert(Selectable::Player(player))
    }

    pub fn insert_value(&mut self, value: i32) {
        self.insert(Selectable::Value(value))
    }

    pub fn append(&mut self, elements: &[Selectable]) {
        for e in elements {
            self.insert(e.clone());
        }
    }

    pub fn contains(&self, selectable: &Selectable) -> bool {
        self.set.contains(selectable)
    }

    pub fn is_empty(&self) -> bool {
        self.set.is_empty()
    }

    pub fn iter(&self) -> Iter<Selectable> {
        self.set.iter()
    }

    pub fn len(&self) -> usize {
        self.set.len()
    }

    pub fn only(&self) -> &Selectable {
        assert!(self.len() == 1);
        &self.set[0]
    }
}

impl<T: sstype::SSType> FromIterator<Selectable> for SelectionSet<T> {
    fn from_iter<I: IntoIterator<Item = Selectable>>(iter: I) -> Self {
        let mut c: SelectionSet<T> = Default::default();

        for i in iter {
            c.insert(i);
        }

        c
    }
}

impl<T: sstype::SSType> PartialEq<SelectionSet<T>> for SelectionSet<T> {
    fn eq(&self, other: &SelectionSet<T>) -> bool {
        self.set == other.set
    }
}
