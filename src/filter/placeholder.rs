use filter::{sstype, SelectionSet};

use std::collections::HashMap;

use strum_macros::{Display, EnumString};

pub type PlaceholderData = HashMap<Placeholder, SelectionSet<sstype::Resolved>>;

#[derive(EnumString, Display, Eq, PartialEq, Hash, Debug, Clone, Copy)]
pub enum Placeholder {
    #[strum(serialize = "$This")]
    This,
    #[strum(serialize = "$A")]
    A,
    #[strum(serialize = "$B")]
    B,
    #[strum(serialize = "$C")]
    C,
    #[strum(serialize = "$D")]
    D,
    #[strum(serialize = "$E")]
    E,
    #[strum(serialize = "$F")]
    F,

    #[strum(serialize = "$PPlayer")]
    PPlayer,
    #[strum(serialize = "$PLife")]
    PLife,
    #[strum(serialize = "$PDefender")]
    PDefender,
    #[strum(serialize = "$PAttacker")]
    PAttacker,
    #[strum(serialize = "$PDamage")]
    PDamage,
}
