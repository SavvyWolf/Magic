use cards::properties::{CardSubtype, CardSupertype, CardType};
use filter::{sstype, Placeholder, PlaceholderData, Selectable, SelectableKind, SelectionSet};
use game::Game;
use gamestate::PlayerId;
use gamestate::RcGameObject;
use gamestate::ZoneType;
use parse::ParseError;

use std::str::FromStr;

use json::{array, JsonValue};
use strum_macros::{Display, EnumString};

#[derive(EnumString, Display, Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum MathFn {
    Add,
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub enum Filter {
    Not(Box<Filter>),
    IsKind(SelectableKind),
    HasType(CardType),
    HasSubtype(CardSubtype),
    HasSupertype(CardSupertype),
    InZone(ZoneType),
    InHand(Selectable),
    InGraveyard(Selectable),
    Untapped,
    Tapped,
    Is(Selectable),
    IsAbsolutePlayer(PlayerId),
    Controller(Selectable),
    ControllerOf(Selectable),
    Bind(Placeholder),
    Update(Box<FilterList>),
    Literal(Selectable),
    Operation(MathFn, Selectable),
    Empty,
}

impl Filter {
    pub fn apply_template(self, data: &PlaceholderData) -> Self {
        if let Filter::InHand(p) = self {
            Filter::InHand(p.apply_template(data).only().clone())
        } else if let Filter::Is(p) = self {
            Filter::Is(p.apply_template(data).only().clone())
        } else if let Filter::InGraveyard(p) = self {
            Filter::InGraveyard(p.apply_template(data).only().clone())
        } else if let Filter::Controller(p) = self {
            Filter::Controller(p.apply_template(data).only().clone())
        } else if let Filter::ControllerOf(p) = self {
            Filter::ControllerOf(p.apply_template(data).only().clone())
        } else if let Filter::Literal(p) = self {
            Filter::Literal(p.apply_template(data).only().clone())
        } else if let Filter::Operation(m, p) = self {
            Filter::Operation(m, p.apply_template(data).only().clone())
        } else if let Filter::Not(s) = self {
            Filter::Not(Box::new(s.apply_template(data)))
        } else if let Filter::Update(s) = self {
            Filter::Update(Box::new(
                s.iter().cloned().map(|x| x.apply_template(data)).collect(),
            ))
        } else {
            self
        }
    }
}

pub type FilterList = Vec<Filter>;

fn matches_filter(object: &Selectable, filter: Filter, map: &mut PlaceholderData) -> bool {
    assert!(object.is_resolved());
    match filter {
        Filter::Not(n) => !matches_filter(object, *n, map),
        Filter::IsKind(sk) => object.kind() == sk,
        Filter::HasType(t) => match object {
            Selectable::GameObject(go) => go.borrow().is_type(t),
            _ => false,
        },
        Filter::HasSubtype(t) => match object {
            Selectable::GameObject(go) => go.borrow().is_subtype(t),
            _ => false,
        },
        Filter::HasSupertype(t) => match object {
            Selectable::GameObject(go) => go.borrow().is_supertype(t),
            _ => false,
        },
        Filter::InZone(z) => match object {
            Selectable::GameObject(go) => go.borrow().zone.unwrap() == z,
            _ => false,
        },
        Filter::InHand(h) => match object {
            Selectable::GameObject(go) => {
                let check = ZoneType::Hand(h.resolve(map).only().unwrap_player());
                go.borrow().zone.unwrap() == check
            }
            _ => false,
        },
        Filter::InGraveyard(h) => match object {
            Selectable::GameObject(go) => {
                let check = ZoneType::Graveyard(h.resolve(map).only().unwrap_player());
                go.borrow().zone.unwrap() == check
            }
            _ => false,
        },
        Filter::Untapped => match object {
            Selectable::GameObject(go) => !go.borrow().tapped,
            _ => false,
        },
        Filter::Tapped => match object {
            Selectable::GameObject(go) => go.borrow().tapped,
            _ => false,
        },
        Filter::Is(x) => object == x.resolve(map).only(),
        Filter::Controller(c) => match object {
            Selectable::GameObject(go) => c
                .resolve(map)
                .iter()
                .all(|sel| go.borrow().controller == sel.unwrap_player()),
            _ => false,
        },
        Filter::ControllerOf(go) => match object {
            Selectable::Player(p) => go
                .resolve(map)
                .iter()
                .all(|sel| *p == sel.unwrap_gameobject().borrow().controller),
            _ => false,
        },
        Filter::IsAbsolutePlayer(ap) => match object {
            Selectable::Player(p) => *p == ap,
            _ => false,
        },
        Filter::Bind(p) => match object {
            Selectable::GameObject(go) => {
                map.insert(p, SelectionSet::new_gameobject(go.clone()));
                true
            }
            Selectable::Player(pl) => {
                map.insert(p, SelectionSet::new_player(*pl));
                true
            }
            Selectable::Value(v) => {
                map.insert(p, SelectionSet::new_value(*v));
                true
            }
            _ => true,
        },
        Filter::Operation(_, _) => panic!("Filter::Operation should have been handled earlier"),
        Filter::Literal(_) => panic!("Filter::Literal should have been handled earlier"),
        Filter::Update(_) => panic!("Filter::Update should have been handled earlier"),
        Filter::Empty => panic!("Filter::Empty should have been handled earlier"),
    }
}

pub fn run_filter(
    filter: &[Filter],
    game: &Game,
    this: Option<&RcGameObject>,
    data: PlaceholderData,
) -> (SelectionSet<sstype::Resolved>, PlaceholderData) {
    // First, collect all the inputs
    let mut set: SelectionSet<sstype::Resolved> = Default::default();

    // Game objects
    for z in game.all_zones() {
        for go in z.iter() {
            set.insert_gameobject(go.clone());
        }
    }

    // Players
    for p in 0..game.num_players() {
        set.insert_player(p);
    }

    run_filter_on(filter, game, this, set, data)
}

pub fn run_filter_on(
    filter: &[Filter],
    game: &Game,
    this: Option<&RcGameObject>,
    mut set: SelectionSet<sstype::Resolved>,
    mut data: PlaceholderData,
) -> (SelectionSet<sstype::Resolved>, PlaceholderData) {
    // Make our map
    if let Some(go) = this {
        data.insert(Placeholder::This, SelectionSet::new_gameobject(go.clone()));
    }

    // Now run the filters
    for f in filter {
        // Some filters replace or set the filter list, so handle those specially
        match f {
            Filter::Literal(x) => set = SelectionSet::new(x.resolve(&data).only().clone()),
            Filter::Operation(fun, rhs) => {
                set = set
                    .iter()
                    .filter_map(|lhs| match lhs {
                        Selectable::Value(lhs) => {
                            let result;
                            match fun {
                                MathFn::Add => {
                                    result = lhs + rhs.resolve(&data).only().unwrap_value()
                                }
                            };
                            Some(Selectable::Value(result))
                        }
                        _ => None,
                    })
                    .collect();
            }
            Filter::Empty => {
                set = Default::default();
            }
            Filter::Update(sf) => {
                let (_, mut new_data) = run_filter_on(sf, game, None, set.clone(), data.clone());
                for (k, v) in new_data.drain() {
                    data.insert(k, v);
                }
            }

            _ => {
                set = set
                    .iter()
                    .filter(|&x| matches_filter(&x, f.clone(), &mut data))
                    .cloned()
                    .collect();
            }
        }
    }

    (set, data)
}

pub fn run_filter_one(
    filter: &[Filter],
    game: &Game,
    this: Option<&RcGameObject>,
    data: PlaceholderData,
) -> (Selectable, PlaceholderData) {
    let (selset, data) = run_filter(filter, game, this, data);

    assert_eq!(selset.len(), 1);

    let vec = selset.only().clone();
    (vec, data)
}

pub fn filter_to_json(filter: &[Filter]) -> JsonValue {
    let mut ret = JsonValue::new_array();

    for f in filter {
        ret.push(single_to_json(f.clone()))
            .expect("Filter is invalid");
    }

    ret
}

pub fn json_to_filter(json: &JsonValue) -> Result<FilterList, ParseError> {
    let mut ret = vec![];

    for f in json.members() {
        ret.push(json_to_single(f)?);
    }

    Ok(ret)
}

fn single_to_json(filter: Filter) -> JsonValue {
    match filter {
        Filter::Not(n) => array!["Not", single_to_json(*n)],
        Filter::IsKind(k) => array!["IsKind", format!("{}", k)],
        Filter::HasType(t) => array!["HasType", format!("{}", t)],
        Filter::HasSubtype(t) => array!["HasSubtype", format!("{}", t)],
        Filter::HasSupertype(t) => array!["HasSupertype", format!("{}", t)],
        Filter::InZone(z) => array!["InZone", format!("{}", z)],
        Filter::InHand(h) => array!["InHand", h.to_json()],
        Filter::InGraveyard(h) => array!["InGraveyard", h.to_json()],
        Filter::Untapped => array!["Untapped"],
        Filter::Tapped => array!["Tapped"],
        Filter::Is(x) => array!["Is", format!("{}", x.to_json())],
        Filter::IsAbsolutePlayer(p) => array!["IsAbsolutePlayer", p],
        Filter::Controller(p) => array!["Controller", format!("{}", p.to_json())],
        Filter::ControllerOf(go) => array!["ControllerOf", format!("{}", go.to_json())],
        Filter::Bind(p) => array!["Bind", format!("{}", p)],
        Filter::Literal(p) => array!["Literal", format!("{}", p.to_json())],
        Filter::Operation(fun, p) => {
            array!["Operation", format!("{}", fun), format!("{}", p.to_json())]
        }
        Filter::Update(sub) => array!["Update", filter_to_json(&sub)],
        Filter::Empty => array!["Empty"],
    }
}

fn json_to_single(json: &JsonValue) -> Result<Filter, ParseError> {
    let as_filter = |i| -> Result<Filter, ParseError> { json_to_single(&json[i]) };
    let as_sk = |i: usize| -> Result<SelectableKind, ParseError> {
        Ok(SelectableKind::from_str(
            json[i]
                .as_str()
                .ok_or(ParseError::WrongType("SelectableKind"))?,
        )?)
    };
    let as_type = |i: usize| -> Result<CardType, ParseError> {
        Ok(CardType::from_str(
            json[i].as_str().ok_or(ParseError::WrongType("CardType"))?,
        )?)
    };
    let as_subtype = |i: usize| -> Result<CardSubtype, ParseError> {
        Ok(CardSubtype::from_str(
            json[i]
                .as_str()
                .ok_or(ParseError::WrongType("CardSubtype"))?,
        )?)
    };
    let as_supertype = |i: usize| -> Result<CardSupertype, ParseError> {
        Ok(CardSupertype::from_str(
            json[i]
                .as_str()
                .ok_or(ParseError::WrongType("CardSupertype"))?,
        )?)
    };
    let as_zonetype = |i: usize| -> Result<ZoneType, ParseError> {
        Ok(ZoneType::from_str(
            json[i].as_str().ok_or(ParseError::WrongType("ZoneType"))?,
        )?)
    };
    let as_placeholder = |i: usize| -> Result<Placeholder, ParseError> {
        Ok(Placeholder::from_str(
            json[i]
                .as_str()
                .ok_or(ParseError::WrongType("Placeholder"))?,
        )?)
    };
    let as_mathfn = |i: usize| -> Result<MathFn, ParseError> {
        Ok(MathFn::from_str(
            json[i].as_str().ok_or(ParseError::WrongType("MathFn"))?,
        )?)
    };
    let as_sel = |i: usize| -> Result<Selectable, ParseError> {
        Ok(Selectable::from_json(
            json[i]
                .as_str()
                .ok_or(ParseError::WrongType("Placeholderable"))?,
        )?)
    };

    let out = match json[0].as_str().unwrap() {
        "Not" => Filter::Not(Box::new(as_filter(1)?)),
        "IsKind" => Filter::IsKind(as_sk(1)?),
        "HasType" => Filter::HasType(as_type(1)?),
        "HasSubtype" => Filter::HasSubtype(as_subtype(1)?),
        "HasSupertype" => Filter::HasSupertype(as_supertype(1)?),
        "InZone" => Filter::InZone(as_zonetype(1)?),
        "InHand" => Filter::InHand(as_sel(1)?),
        "InGraveyard" => Filter::InGraveyard(as_sel(1)?),
        "Untapped" => Filter::Untapped,
        "Tapped" => Filter::Tapped,
        "Is" => Filter::Is(as_sel(1)?),
        "IsAbsolutePlayer" => {
            Filter::IsAbsolutePlayer(json[1].as_usize().ok_or(ParseError::WrongType("player"))?)
        }
        "Controller" => Filter::Controller(as_sel(1)?),
        "ControllerOf" => Filter::ControllerOf(as_sel(1)?),
        "Bind" => Filter::Bind(as_placeholder(1)?),
        "Literal" => Filter::Literal(as_sel(1)?),
        "Operation" => Filter::Operation(as_mathfn(1)?, as_sel(2)?),
        "Update" => Filter::Update(Box::new(json_to_filter(&json[1])?)),
        "Empty" => Filter::Empty,
        _ => panic!("Unknown filter type {}", json[0]),
    };

    Ok(out)
}
