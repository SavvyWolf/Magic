// I don't want only some actions to be Default
#![allow(clippy::new_without_default)]

use abilities::{layers, ActivatedAbility, SEBoxClone, Speed, StaticEffect, TriggeredAbility};
use actions::{Action, ActionBoxClone, ActionEntry, ActionHookType, ExpectedUserInput, Usability};
use cards::mana::{Cost, CostEntry, Mana};
use cards::properties::CardType;
use cards::{Card, CardKind};
use filter::Filter::*;
use filter::{
    filter_to_json, json_to_filter, run_filter, run_filter_on, sstype, FilterList, Placeholder,
    PlaceholderData, Selectable, SelectionSet,
};
use game::Game;
use gamestate::{GameObject, ObjectType, PlayerId, RcGameObject, ZoneType};
use parse::{dump_cost, parse_cost, ParseError, TypedJson, TypedRegistry};
use utils::{could_pay_from_board, could_pay_from_pool};

use std::str::FromStr;

use super::get_actions_registry;
use super::DeleteAction;
use super::MoveToBattlefield;
use super::MoveToGraveyard;
use super::PlayLandAction;
use super::UntapAction;

use json::{object, JsonValue};
use magic_macros::{ActionBoxClone, SEBoxClone, TypedJson};

use std::any::Any;
use std::rc::Rc;

// =======
// Actions
// =======
#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PanicAction {}
impl PanicAction {
    pub fn new() -> Self {
        Self {}
    }
}
impl Action for PanicAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Panic (this is a bug)".to_owned()
    }

    fn perform(&self, _game: &mut Game, _this: Option<RcGameObject>) {
        panic!("Ran panic action");
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PanicOnEmptyStackAction {}
impl Action for PanicOnEmptyStackAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Panic on empty stack".to_owned()
    }

    fn perform(&self, _game: &mut Game, _this: Option<RcGameObject>) {
        panic!("Action stack is empty!");
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PassPriorityAction {}
impl Action for PassPriorityAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Pass priority".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Pop the top item off the action stack
        game.pop_action();
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct SetPriorityAction {
    player: PlayerId,
}
impl SetPriorityAction {
    pub fn new(player: PlayerId) -> Self {
        Self { player }
    }
}
impl Action for SetPriorityAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Set Priority".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        game.state.priority = self.player;
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct FinishActivatingManaAbilitiesAction {
    player: PlayerId,
}
impl FinishActivatingManaAbilitiesAction {
    pub fn new(player: PlayerId) -> Self {
        Self { player }
    }
}
impl Action for FinishActivatingManaAbilitiesAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Finish".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Pop the top item off the action stack
        game.pop_action();
    }

    fn get_usability(&self, game: &Game, _this: Option<&RcGameObject>) -> Usability {
        if could_pay_from_pool(
            &game.current_cost,
            game.players[self.player].mana_pool.clone(),
            game.players[self.player].life,
        ) {
            Usability::Usable
        } else {
            Usability::Unusable
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct AdvanceGameStateAction {}
impl Action for AdvanceGameStateAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Advance to next game state".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        game.advance();
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct HavePriorityAction {}
impl HavePriorityAction {
    pub fn new() -> HavePriorityAction {
        HavePriorityAction {}
    }
}
impl Action for HavePriorityAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Player has priority".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Repeat itself
        game.push_action(None, Box::new(HavePriorityAction {}));
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (game.get_priority(), ExpectedUserInput::PriorityChoice)
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct MayActivateManaAbilitiesAction {}
impl MayActivateManaAbilitiesAction {
    pub fn new() -> MayActivateManaAbilitiesAction {
        MayActivateManaAbilitiesAction {}
    }
}
impl Action for MayActivateManaAbilitiesAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Player may activate mana abilities".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Repeat itself
        game.push_action(None, Box::new(MayActivateManaAbilitiesAction {}));
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (
            game.get_priority(),
            ExpectedUserInput::ActivateManaAbilities,
        )
    }
}

// 106.4. [...] Each player’s mana pool empties at the end of each step and phase, and the player is said to lose this
// mana.
#[derive(Clone, ActionBoxClone)]
pub struct EmptyManaPoolsAction {
    player: PlayerId,
}
impl Action for EmptyManaPoolsAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Player {} empties their mana pool", self.player)
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        game.players[self.player].mana_pool.empty();
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(Box::new(Self::new(
            json["player"]
                .as_usize()
                .ok_or(ParseError::WrongType("player"))?,
        )))
    }
}
impl TypedJson for EmptyManaPoolsAction {
    fn type_name(&self) -> &'static str {
        "EmptyManaPoolsAction"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "player" => self.player
        }
    }
}
impl EmptyManaPoolsAction {
    pub fn new(player: PlayerId) -> EmptyManaPoolsAction {
        EmptyManaPoolsAction { player }
    }
}

#[derive(Clone, ActionBoxClone)]
pub struct UseActivatedAbilityAction {
    offset: usize,
}
impl UseActivatedAbilityAction {
    pub fn get_entry(&self, _game: &Game, this: Option<&RcGameObject>) -> Option<ActivatedAbility> {
        let o = this.unwrap().borrow();
        Some(o.activated_abilities.get(self.offset)?.clone())
    }
}
impl Action for UseActivatedAbilityAction {
    fn description(&self, game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let e = self.get_entry(game, this).unwrap();
        let desc = format!(
            "{}: {}",
            e.cost_description(game, this),
            e.action.description(game, this, false)
        );
        desc
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let entry = self.get_entry(game, this.as_ref()).unwrap();
        if entry.speed == Speed::ManaAbility {
            let action = entry.action.box_clone();
            game.push_action(Some(this.clone().unwrap()), action);
        } else {
            game.push_action(
                Some(this.clone().unwrap()),
                Box::new(PutActivatedAbilityAction::new(entry.clone())),
            );
        }

        for c in self.get_entry(game, this.as_ref()).clone().unwrap().cost {
            let action = c;
            game.push_action(Some(this.clone().unwrap()), action);
        }
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(Box::new(Self::new(
            json["offset"]
                .as_usize()
                .ok_or(ParseError::WrongType("offset"))?,
        )))
    }

    fn could_produce_mana(&self, game: &Game, this: Option<&RcGameObject>) -> Vec<Vec<Mana>> {
        let mut out: Vec<Vec<Mana>> = Default::default();
        for a in vec![self.get_entry(game, this).unwrap().action] {
            let mut mana = a.could_produce_mana(game, this);
            if !mana.is_empty() {
                out.append(&mut mana);
            }
        }

        out
    }

    fn get_usability(&self, game: &Game, this: Option<&RcGameObject>) -> Usability {
        self.get_entry(game, this)
            .unwrap()
            .get_usability(game, this)
    }
}
impl TypedJson for UseActivatedAbilityAction {
    fn type_name(&self) -> &'static str {
        "UseActivatedAbilityAction"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "offset" => self.offset,
        }
    }
}
impl UseActivatedAbilityAction {
    pub fn new(offset: usize) -> UseActivatedAbilityAction {
        UseActivatedAbilityAction { offset }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct StartCastSpellAction {}
impl StartCastSpellAction {
    pub fn new() -> StartCastSpellAction {
        StartCastSpellAction {}
    }
}
impl Action for StartCastSpellAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let c = this.unwrap();
        format!("Cast {}", c.borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let spell = this.unwrap();

        // Remove the card
        let old_zone = spell.borrow().zone.unwrap();
        let old_id = spell.borrow().id;
        let spell = game
            .find_zone_mut(old_zone)
            .remove_id(old_id)
            .unwrap()
            .borrow()
            .transform(ObjectType::Stack);

        // Put the spell on the stack
        let spell = game.stack.push(spell).clone();

        // Pay the cost
        game.current_cost = spell.borrow().card.cost.clone();
        game.push_action(None, Box::new(PayManaCostAction::new()));
        game.push_action(None, Box::new(MayActivateManaAbilitiesAction::new()));

        // Perform cast choices (target, mana resolution, etc.)
        for cc in spell.borrow().card.cast_choices.iter().rev() {
            game.push_action(Some(spell.clone()), cc.box_clone());
        }
    }

    fn get_usability(&self, game: &Game, this: Option<&RcGameObject>) -> Usability {
        if could_pay_from_board(
            &this.unwrap().borrow().card.cost,
            &game,
            this.unwrap().borrow().controller,
        ) {
            Usability::Usable
        } else {
            Usability::Unlikely
        }
    }
}

#[derive(Clone, ActionBoxClone)]
pub struct ManaCostAction {
    cost: Cost,
}
impl ManaCostAction {
    pub fn new(cost: Cost) -> Self {
        Self { cost }
    }
}
impl Action for ManaCostAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let mut c = String::new();
        for e in &self.cost {
            c.push_str(format!("{}", e).as_str());
        }
        c
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        game.current_cost = self.cost.clone();
        game.push_action(None, Box::new(PayManaCostAction::new()));
        game.push_action(None, Box::new(MayActivateManaAbilitiesAction::new()));
    }

    fn get_usability(&self, game: &Game, this: Option<&RcGameObject>) -> Usability {
        if could_pay_from_board(&self.cost, &game, this.unwrap().borrow().controller) {
            Usability::Usable
        } else {
            Usability::Unlikely
        }
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(Box::new(Self::new(parse_cost(
            json["cost"].as_str().ok_or(ParseError::WrongType("cost"))?,
        )?)))
    }
}
impl TypedJson for ManaCostAction {
    fn type_name(&self) -> &'static str {
        "ManaCostAction"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "cost" => dump_cost(&self.cost),
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PutTriggeredAbilityAction {}
impl PutTriggeredAbilityAction {
    pub fn new() -> Self {
        Self {}
    }
}
impl Action for PutTriggeredAbilityAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let c = this.unwrap();
        c.borrow().sig()
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let ability = this.unwrap();

        // Remove the card
        let old_zone = ability.borrow().zone.unwrap();
        let old_id = ability.borrow().id;
        let ability = game
            .find_zone_mut(old_zone)
            .remove_id(old_id)
            .unwrap()
            .borrow()
            .transform(ObjectType::Stack);

        // Put the spell on the stack
        game.stack.push(ability);

        // Pay the cost (None yet)
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PutActivatedAbilityAction {
    ability: ActivatedAbility,
}
impl PutActivatedAbilityAction {
    pub fn new(ability: ActivatedAbility) -> Self {
        Self { ability }
    }
}
impl Action for PutActivatedAbilityAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let c = this.unwrap();
        c.borrow().sig()
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let this = this.unwrap();
        let this = this.borrow();

        let mut card: Card = Default::default();
        card.resolution_effects = vec![self.ability.action.box_clone()];
        card.kind = CardKind::Ability;
        card.name = this.sig();
        game.find_zone_mut(ZoneType::Stack)
            .insert_from_card(Rc::new(card), this.controller);
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PayManaCostAction {}
impl PayManaCostAction {
    pub fn new() -> PayManaCostAction {
        PayManaCostAction {}
    }
}
impl Action for PayManaCostAction {
    fn description(&self, game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let mut c = String::new();
        for e in &game.current_cost {
            c.push_str(format!("{}", e).as_str());
        }
        format!("Pay {}", c)
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        if !game.current_cost.is_empty() {
            game.push_action(None, Box::new(PayManaCostAction::new()));
        }
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (game.get_priority(), ExpectedUserInput::MakeManaPayment)
    }
}

#[derive(Clone, ActionBoxClone)]
pub struct PayUnitOfManaCostAction {
    pool: Mana,
    player: PlayerId,
    cost: usize,
}
impl PayUnitOfManaCostAction {
    pub fn new(pool: Mana, player: PlayerId, cost: usize) -> PayUnitOfManaCostAction {
        PayUnitOfManaCostAction { pool, player, cost }
    }

    pub fn get_cost(&self, game: &Game) -> CostEntry {
        game.current_cost[self.cost]
    }
}
impl Action for PayUnitOfManaCostAction {
    fn description(&self, game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Pay {} for {}", self.pool, self.get_cost(game))
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        game.current_cost.remove(self.cost);
        // TODO: Generic mana
        game.players[self.player].mana_pool.remove(self.pool);
    }

    /*    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(Box::new(Self::new(
            /*json["pool"]
                .as_usize()
                .ok_or(ParseError::WrongType("pool"))?,*/
            json["player"]
                .as_usize()
                .ok_or(ParseError::WrongType("player"))?,
            json["cost"]
                .as_usize()
                .ok_or(ParseError::WrongType("cost"))?,
        )))
    }*/
}
impl TypedJson for PayUnitOfManaCostAction {
    fn type_name(&self) -> &'static str {
        "PayUnitOfManaCostAction"
    }

    /*    fn get_json(&self) -> JsonValue {
        object! {
            "pool" => self.pool,
            "player" => self.player,
            "cost" => self.cost,
        }
    }*/
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct ResolveStackObject {}
impl Action for ResolveStackObject {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let s = this.unwrap();
        format!("Resolve {}", s.borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let s = this.unwrap();

        if s.borrow().is_type(CardType::Instant) || s.borrow().is_type(CardType::Sorcery) {
            let effects = &s.borrow().card.resolution_effects;
            let mut actions = effects.clone();

            game.push_action(Some(s.clone()), Box::from(MoveToGraveyard::new(false)));
            for action in actions.drain(..) {
                game.push_action(Some(s.clone()), action);
            }
        } else if s.borrow().card.kind == CardKind::Ability {
            let effects = &s.borrow().card.resolution_effects;
            let mut actions = effects.clone();

            // Remove the ability
            game.push_action(Some(s.clone()), Box::from(DeleteAction::new()));
            for action in actions.drain(..) {
                game.push_action(Some(s.clone()), action);
            }
        } else {
            // Permanent types
            game.push_action(Some(s.clone()), Box::from(MoveToBattlefield::new()));
        }
    }
}
impl ResolveStackObject {
    pub fn new() -> ResolveStackObject {
        ResolveStackObject {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct UntapStepAction {
    object: FilterList,
}
impl UntapStepAction {
    pub fn new(player: PlayerId) -> UntapStepAction {
        UntapStepAction {
            object: vec![
                InZone(ZoneType::Battlefield),
                Controller(Selectable::Player(player)),
                Tapped,
            ],
        }
    }

    fn get_objects(
        &self,
        game: &Game,
        this: Option<&RcGameObject>,
    ) -> SelectionSet<sstype::Resolved> {
        run_filter(&self.object, game, this, Default::default()).0
    }
}
impl Action for UntapStepAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Perform untap step actions".to_owned()
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let tapped = self.get_objects(game, this.as_ref());
        for t in tapped.iter() {
            game.push_action(
                Some(t.unwrap_gameobject().clone()),
                Box::new(UntapAction::new()),
            );
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct SBAAction {}
impl SBAAction {
    pub fn new() -> Self {
        Self {}
    }
}
impl Action for SBAAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Perform state based actions".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        let mut actions: Vec<(Option<RcGameObject>, Box<dyn Action>)> = vec![];

        // 704.5d If a token is in a zone other than the battlefield, it ceases to exist.
        let mut remove = vec![];
        for z in game.all_zones_mut() {
            if !z.get_type().is_battlefield() {
                for p in z.iter() {
                    if p.borrow().card.kind == CardKind::Token {
                        remove.push(p.clone());
                    }
                }
            }
        }
        for r in remove {
            actions.push((Some(r), Box::from(DeleteAction::new())));
        }

        // 704.5f If a creature has toughness 0 or less, it’s put into its owner’s graveyard. Regeneration can’t replace
        // this event.
        let dead: Vec<RcGameObject> = game
            .find_zone(ZoneType::Battlefield)
            .iter()
            .filter(|x| x.borrow().toughness == 0 && x.borrow().is_type(CardType::Creature))
            .cloned()
            .collect();
        for d in dead {
            actions.push((Some(d.clone()), Box::from(MoveToGraveyard::new(false))));
        }

        // 704.5g If a creature has toughness greater than 0, and the total damage marked on it is greater than or equal
        // to its toughness, that creature has been dealt lethal damage and is destroyed. Regeneration can replace this
        // event.
        let dead: Vec<RcGameObject> = game
            .find_zone(ZoneType::Battlefield)
            .iter()
            .filter(|x| {
                x.borrow().marked_damage >= x.borrow().toughness
                    && x.borrow().is_type(CardType::Creature)
            })
            .cloned()
            .collect();
        for d in dead {
            actions.push((Some(d.clone()), Box::from(MoveToGraveyard::new(true))));
        }

        // 704.5i If a planeswalker has loyalty 0, it’s put into its owner’s graveyard.
        let dead: Vec<RcGameObject> = game
            .find_zone(ZoneType::Battlefield)
            .iter()
            .filter(|x| x.borrow().loyalty == 0 && x.borrow().is_type(CardType::Planeswalker))
            .cloned()
            .collect();
        for d in dead {
            actions.push((Some(d.clone()), Box::from(MoveToGraveyard::new(false))));
        }

        if !actions.is_empty() {
            game.push_action(None, Box::new(SBAAction::new()));
        }

        for a in actions {
            game.push_action(a.0, a.1);
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct TriggerTriggeredAbilityAction {
    ability: TriggeredAbility,
}
impl TriggerTriggeredAbilityAction {
    pub fn new(ability: TriggeredAbility) -> Self {
        Self { ability }
    }
}
impl Action for TriggerTriggeredAbilityAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let c = this.unwrap();
        format!("Trigger triggered ability of {}", c.borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let this = this.unwrap();
        let this = this.borrow();
        let mut card: Card = Default::default();
        card.resolution_effects = self.ability.payload.clone();
        card.kind = CardKind::Ability;
        card.name = this.sig();
        game.find_zone_mut(ZoneType::TriggeredWaiting)
            .insert_from_card(Rc::new(card), this.controller);
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct MaybePutTriggeredAbilityAction {
    player: PlayerId,
}
impl MaybePutTriggeredAbilityAction {
    pub fn new(player: PlayerId) -> Self {
        Self { player }
    }

    fn needs_done(&self, game: &Game) -> bool {
        game.find_zone(ZoneType::TriggeredWaiting)
            .iter()
            .any(|x| x.borrow().controller == self.player)
    }
}
impl Action for MaybePutTriggeredAbilityAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!(
            "Player {} puts triggered abilities onto the stack",
            self.player
        )
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        if self.needs_done(game) {
            game.push_action(this, self.box_clone());
        }
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        if self.needs_done(game) {
            (self.player, ExpectedUserInput::PutTriggeredAbilityOnStack)
        } else {
            (0, ExpectedUserInput::None)
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct SelectTargetAction {
    dest: Placeholder,
    options: FilterList,
    min: FilterList,
    max: FilterList,
}
impl SelectTargetAction {
    pub fn new(dest: Placeholder, options: FilterList, min: FilterList, max: FilterList) -> Self {
        Self {
            dest,
            options,
            min,
            max,
        }
    }

    pub fn get_options(
        &self,
        game: &Game,
        this: Option<&RcGameObject>,
    ) -> SelectionSet<sstype::Resolved> {
        run_filter(&self.options, game, this, Default::default()).0
    }
    pub fn get_min(&self, game: &Game, this: Option<&RcGameObject>) -> usize {
        run_filter(&self.min, game, this, Default::default())
            .0
            .only()
            .unwrap_value() as usize
    }
    pub fn get_max(&self, game: &Game, this: Option<&RcGameObject>) -> usize {
        run_filter(&self.max, game, this, Default::default())
            .0
            .only()
            .unwrap_value() as usize
    }
}
impl Action for SelectTargetAction {
    fn description(&self, game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("TBD")
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        dbg!();
        game.push_action(this, self.box_clone());
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (game.get_priority(), ExpectedUserInput::SelectTarget)
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct ChooseTargetAction {
    dest: Placeholder,
    target: Selectable,
}
impl ChooseTargetAction {
    pub fn new(dest: Placeholder, target: Selectable) -> Self {
        assert!(target.is_resolved());
        Self { dest, target }
    }
}
impl Action for ChooseTargetAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        match &self.target {
            Selectable::GameObject(go) => go.borrow().sig(),
            Selectable::Player(p) => format!("Player {}", p),
            Selectable::Value(v) => format!("{}", v),
            _ => panic!("???"),
        }
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        let this = this.unwrap();
        let mut this = this.borrow_mut();

        match &self.target {
            Selectable::GameObject(go) => {
                let go = go;
                assert!(!this.stored_data.contains_key(&self.dest)); // Not yet supported
                this.stored_data
                    .insert(self.dest, SelectionSet::new_gameobject(go.clone()));
            }
            _ => panic!("Non-go targets not yet supported"),
        }
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (game.get_priority(), ExpectedUserInput::SelectTarget)
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct FinishChoosingTargetsAction {}
impl FinishChoosingTargetsAction {
    pub fn new() -> Self {
        Self {}
    }
}
impl Action for FinishChoosingTargetsAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Finish choosing targets".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        game.pop_action();
    }
}

pub fn load_engine_actions(registry: &mut TypedRegistry<dyn Action>) {
    registry.push(Box::from(ManaCostAction::new(vec![])));
}

// ==============
// Static Effects
// ==============
#[derive(Clone, TypedJson, SEBoxClone)]
pub struct PriorityCanBePassed {}
impl PriorityCanBePassed {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for PriorityCanBePassed {
    fn get_actions(
        &self,
        _game: &Game,
        source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        if expected == ExpectedUserInput::PriorityChoice {
            vec![ActionEntry::new(
                Box::new(PassPriorityAction {}),
                Some(source.clone()),
            )]
        } else if expected == ExpectedUserInput::ActivateManaAbilities {
            vec![ActionEntry::new(
                Box::new(FinishActivatingManaAbilitiesAction::new(player)),
                Some(source.clone()),
            )]
        } else if expected == ExpectedUserInput::DeclareAttackers {
            vec![ActionEntry::new(
                Box::new(super::FinishDeclaringAttackersAction {}),
                Some(source.clone()),
            )]
        } else if expected == ExpectedUserInput::DeclareBlockers {
            vec![ActionEntry::new(
                Box::new(super::FinishDeclaringBlockersAction {}),
                Some(source.clone()),
            )]
        } else {
            vec![]
        }
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct LandsMayBePlayed {}
impl LandsMayBePlayed {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for LandsMayBePlayed {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        if expected != ExpectedUserInput::PriorityChoice {
            return vec![];
        }
        let mut out = vec![];
        for c in game.hands[player].iter() {
            let cb = c.borrow();
            if cb.is_type(CardType::Land)
                && game.can_cast_sorceries(cb.controller)
                && game.players[cb.controller].land_drops_left > 0
            {
                out.push(ActionEntry::new(
                    Box::new(PlayLandAction::new()),
                    Some(c.clone()),
                ));
            }
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct SpellsMayBeCast {}
impl SpellsMayBeCast {
    pub fn new() -> Self {
        Self {}
    }
}
impl SpellsMayBeCast {
    fn can_be_cast(types: &[CardType], sorc_speed: bool) -> bool {
        if sorc_speed {
            for t in types {
                if match t {
                    CardType::Artifact => true,
                    CardType::Creature => true,
                    CardType::Enchantment => true,
                    CardType::Instant => true,
                    CardType::Planeswalker => true,
                    CardType::Sorcery => true,
                    _ => false,
                } {
                    return true;
                }
            }
            false
        } else {
            types.contains(&CardType::Instant)
        }
    }
}
impl StaticEffect for SpellsMayBeCast {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        if expected != ExpectedUserInput::PriorityChoice {
            return vec![];
        }
        let mut out = vec![];
        for c in game.hands[player].iter() {
            let cb = c.borrow();
            let sorc_speed = game.can_cast_sorceries(cb.controller);
            if game.can_cast_instants(cb.controller)
                && SpellsMayBeCast::can_be_cast(&cb.types, sorc_speed)
            {
                out.push(ActionEntry::new(
                    Box::new(StartCastSpellAction::new()),
                    Some(c.clone()),
                ));
            }
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct TriggeredAbilitiesMayBePut {}
impl TriggeredAbilitiesMayBePut {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for TriggeredAbilitiesMayBePut {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        if expected != ExpectedUserInput::PutTriggeredAbilityOnStack {
            return vec![];
        }
        let mut out = vec![];
        for c in game
            .find_zone(ZoneType::TriggeredWaiting)
            .iter()
            .filter(|x| x.borrow().controller == player)
        {
            out.push(ActionEntry::new(
                Box::new(PutTriggeredAbilityAction::new()),
                Some(c.clone()),
            ));
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct ActivatedAbilitiesMayBeActivated {}
impl ActivatedAbilitiesMayBeActivated {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for ActivatedAbilitiesMayBeActivated {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        let mut out = vec![];
        if expected != ExpectedUserInput::PriorityChoice
            && expected != ExpectedUserInput::ActivateManaAbilities
        {
            return vec![];
        }

        let can_cast = |x: &ActivatedAbility, o: &GameObject| -> bool {
            match x.speed {
                Speed::ManaAbility => true,
                Speed::Instant => expected != ExpectedUserInput::ActivateManaAbilities,
                Speed::Sorcery => {
                    game.can_cast_sorceries(o.controller)
                        && expected != ExpectedUserInput::ActivateManaAbilities
                }
            }
        };

        for z in game.all_zones() {
            for o in z.iter() {
                let ob = o.borrow();
                for (i, aa) in ob.activated_abilities.iter().enumerate() {
                    if can_cast(aa, &ob)
                        && aa.get_usability(game, Some(o)) != Usability::Unusable
                        && ob.controller == player
                    {
                        out.push(ActionEntry::new(
                            Box::new(UseActivatedAbilityAction::new(i)),
                            Some(o.clone()),
                        ));
                    }
                }
            }
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct ManaCostsMayBePaid {}
impl ManaCostsMayBePaid {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for ManaCostsMayBePaid {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        let mut out = vec![];
        if expected != ExpectedUserInput::MakeManaPayment {
            return vec![];
        }

        for mana in game.players[player].mana_pool.types() {
            for (cost_i, cost) in game.current_cost.iter().enumerate() {
                if mana.could_pay(cost) {
                    out.push(ActionEntry::new(
                        Box::new(PayUnitOfManaCostAction::new(mana, player, cost_i)),
                        None,
                    ));
                }
            }
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct TargetsMayBeChosen {}
impl TargetsMayBeChosen {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for TargetsMayBeChosen {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        _player: PlayerId,
    ) -> Vec<ActionEntry> {
        let mut out = vec![];
        if expected != ExpectedUserInput::SelectTarget {
            return vec![];
        }

        let top = game
            .peek_action()
            .action
            .as_any()
            .downcast_ref::<SelectTargetAction>()
            .unwrap();
        let top_obj = game.peek_action().object.as_ref();
        let top_obj_borrow = top_obj.unwrap().borrow();

        let min = top.get_min(game, top_obj);
        let max = top.get_max(game, top_obj);

        let default: SelectionSet<sstype::Resolved> = Default::default();
        let set = top_obj_borrow.stored_data.get(&top.dest);
        let have = match set {
            Some(x) => x,
            None => &default,
        };

        dbg!(max);
        dbg!(min);
        dbg!(have.len());

        if have.len() < max {
            for o in top.get_options(game, top_obj).iter() {
                if !have.contains(o) {
                    out.push(ActionEntry::new(
                        Box::new(ChooseTargetAction::new(top.dest, o.clone())),
                        top_obj.cloned(),
                    ));
                }
            }
        }

        if have.len() >= min {
            out.push(ActionEntry::new(
                Box::new(FinishChoosingTargetsAction::new()),
                top_obj.cloned(),
            ));
        }

        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, SEBoxClone)]
pub struct ReplacementEffect {
    pub watch: FilterList,
    pub watch_type: ActionHookType,
    pub keep_original: bool,
    pub replace_with: Vec<Box<dyn Action>>,
}
impl ReplacementEffect {
    pub fn new(
        watch: FilterList,
        watch_type: ActionHookType,
        keep_original: bool,
        replace_with: Vec<Box<dyn Action>>,
    ) -> ReplacementEffect {
        ReplacementEffect {
            watch,
            watch_type,
            keep_original,
            replace_with,
        }
    }

    pub fn does_match(
        &self,
        game: &Game,
        source: Option<&RcGameObject>,
        set: SelectionSet<sstype::Resolved>,
        data: PlaceholderData,
    ) -> Option<PlaceholderData> {
        let (objects, data) = run_filter_on(&self.watch, game, source, set, data);

        if objects.is_empty() {
            None
        } else {
            Some(data)
        }
    }
}
impl StaticEffect for ReplacementEffect {
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
    fn as_replacement_effect(&self) -> Option<&ReplacementEffect> {
        Some(self)
    }
    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn StaticEffect>, ParseError> {
        let areg = get_actions_registry();
        let mut actions = vec![];
        for a in json["replace_with"].members() {
            actions.push(areg.load_action(a)?);
        }
        Ok(Box::new(Self::new(
            json_to_filter(&json["watch"])?,
            ActionHookType::from_str(
                json["watch_type"]
                    .as_str()
                    .ok_or(ParseError::WrongType("watch_type"))?,
            )?,
            json["keep_original"]
                .as_bool()
                .ok_or(ParseError::WrongType("keep_original"))?,
            actions,
        )))
    }
}
impl TypedJson for ReplacementEffect {
    fn type_name(&self) -> &'static str {
        "ReplacementEffect"
    }

    fn get_json(&self) -> JsonValue {
        let mut alist = JsonValue::new_array();
        for a in &self.replace_with {
            alist.push(a.to_json()).unwrap();
        }
        object! {
            "watch" => filter_to_json(&self.watch),
            "watch_type" => format!("{}", &self.watch_type),
            "keep_original" => self.keep_original,
            "replace_with" => alist
        }
    }
}

#[derive(Clone, SEBoxClone)]
pub struct AddActivatedAbility {
    onto: FilterList,
    ability: ActivatedAbility,
}
impl AddActivatedAbility {
    pub fn new(onto: FilterList, ability: ActivatedAbility) -> AddActivatedAbility {
        AddActivatedAbility { onto, ability }
    }

    fn get_onto(
        &self,
        game: &Game,
        source: Option<&RcGameObject>,
    ) -> SelectionSet<sstype::Resolved> {
        run_filter(&self.onto, game, source, Default::default()).0
    }
}
impl StaticEffect for AddActivatedAbility {
    fn modify_board(&self, game: &mut Game, source: &RcGameObject) {
        let objects = self.get_onto(game, Some(source));
        for fable in objects.iter() {
            let loc = fable.unwrap_gameobject();
            loc.borrow_mut()
                .activated_abilities
                .push(self.ability.clone());
        }
    }
    fn layer(&self) -> layers::Layer {
        layers::ABILITY_ADDING_EFFECTS
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn StaticEffect>, ParseError> {
        Ok(Box::new(Self::new(
            json_to_filter(&json["onto"])?,
            ActivatedAbility::from_json(&json["ability"])?,
        )))
    }
}
impl TypedJson for AddActivatedAbility {
    fn type_name(&self) -> &'static str {
        "AddActivatedAbility"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "onto" => filter_to_json(&self.onto),
            "ability" => self.ability.to_json(),
        }
    }
}

pub fn load_engine_static_effects(registry: &mut TypedRegistry<dyn StaticEffect>) {
    registry.push(Box::from(PriorityCanBePassed::new()));
    registry.push(Box::from(LandsMayBePlayed::new()));
    registry.push(Box::from(SpellsMayBeCast::new()));
    registry.push(Box::from(TriggeredAbilitiesMayBePut::new()));
    registry.push(Box::from(ActivatedAbilitiesMayBeActivated::new()));
    registry.push(Box::from(ManaCostsMayBePaid::new()));
    registry.push(Box::from(TargetsMayBeChosen::new()));
    registry.push(Box::from(ReplacementEffect::new(
        vec![],
        ActionHookType::Draw,
        true,
        vec![],
    )));
    registry.push(Box::from(AddActivatedAbility::new(
        vec![],
        ActivatedAbility::new(vec![], vec![], Box::new(PanicAction::new()), Speed::Instant),
    )));
}
