// I don't want only some actions to be Default
#![allow(clippy::new_without_default)]

use abilities::{layers, ActivatedAbility, SEBoxClone, Speed, StaticEffect};
use actions::{Action, ActionBoxClone, ActionHook, ActionHookType, Usability};
use cards::mana::Colour::*;
use cards::mana::Cost;
use cards::mana::Mana;
use cards::properties::CardSubtype;
use cards::Card;
use cards::CardKind;
use filter::Filter::*;
use filter::Placeholder::*;
use filter::{
    filter_to_json, json_to_filter, run_filter, run_filter_one, sstype, Filter, FilterList,
    PlaceholderData, Selectable, SelectionSet,
};
use game::Game;
use gamestate::Life;
use gamestate::ObjectType;
use gamestate::RcGameObject;
use gamestate::ZoneType;
use parse::savmtg::{card_to_json, json_to_card};
use parse::{dump_cost, parse_cost};
use parse::{ParseError, TypedJson, TypedRegistry};
use utils::put_into_play;

use super::ManaCostAction;
use super::ReplacementEffect;

use json::{object, JsonValue};
use magic_macros::{ActionBoxClone, SEBoxClone, TypedJson};

use std::any::Any;
use std::rc::Rc;

fn apply_template(vec: &mut FilterList, data: &PlaceholderData) {
    // Not sure how to do this correctly...
    #[allow(clippy::needless_range_loop)]
    for i in 0..vec.len() {
        vec[i] = vec[i].clone().apply_template(data);
    }
}

enum MultiOption {
    None,
    One(SelectionSet<sstype::Resolved>),
    Many(SelectionSet<sstype::Resolved>),
}

fn multifilter(filters: &[Filter], game: &Game, this: Option<&RcGameObject>) -> MultiOption {
    let f = run_filter(filters, game, this, Default::default()).0;
    match f.len() {
        0 => MultiOption::None,
        1 => MultiOption::One(f),
        _ => MultiOption::Many(f),
    }
}

// =======
// Actions
// =======
#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PlayLandAction {}
impl Action for PlayLandAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Play {} as a land", this.unwrap().borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let card = this.unwrap();
        let c = card.borrow().controller;
        game.players[c].land_drops_left -= 1;
        put_into_play(game, &card);
    }
}
impl PlayLandAction {
    pub fn new() -> PlayLandAction {
        PlayLandAction {}
    }
}

#[derive(Clone, ActionBoxClone)]
pub struct DrawCardAction {
    player: FilterList,
}
impl Action for DrawCardAction {
    fn description(&self, game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        match self.get_player(game, this) {
            MultiOption::None => "Unknown player draws a card".to_string(),
            MultiOption::One(player) => {
                let player = player.only().unwrap_player();
                format!("Player {} draws a card", player)
            }
            MultiOption::Many(_) => "Multiple players draw a card".to_string(),
        }
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        match self.get_player(game, this.as_ref()) {
            MultiOption::None => (),
            MultiOption::One(player) => {
                let player = player.only().unwrap_player();
                game.draw(player, 1);
            }
            MultiOption::Many(players) => {
                for entry in players.iter() {
                    // TODO: APNAP order
                    let player = entry.unwrap_player();
                    game.push_action(
                        this.clone(),
                        Box::new(DrawCardAction::new(vec![IsAbsolutePlayer(player)])),
                    )
                }
            }
        }
    }

    fn get_hooks(&self, game: &Game, this: Option<&RcGameObject>) -> Vec<ActionHook> {
        match self.get_player(game, this) {
            MultiOption::One(player) => {
                let player = player.only().unwrap_player();

                let mut data: PlaceholderData = Default::default();
                data.insert(PPlayer, SelectionSet::new_player(player));

                vec![ActionHook::new_auto_collect(ActionHookType::Draw, data)]
            }
            _ => vec![], // No/many player = no hook
        }
    }

    fn apply_template(&mut self, data: &PlaceholderData) {
        apply_template(&mut self.player, data);
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(Box::new(Self::new(json_to_filter(&json["player"])?)))
    }
}
impl TypedJson for DrawCardAction {
    fn type_name(&self) -> &'static str {
        "DrawCardAction"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "player" => filter_to_json(&self.player),
        }
    }
}
impl DrawCardAction {
    pub fn new(player: FilterList) -> DrawCardAction {
        DrawCardAction { player }
    }

    fn get_player(&self, game: &Game, this: Option<&RcGameObject>) -> MultiOption {
        multifilter(&self.player, game, this)
    }
}

#[derive(Clone, ActionBoxClone)]
pub struct GainLifeAction {
    player: FilterList,
    life: FilterList,
}
impl Action for GainLifeAction {
    fn description(&self, game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let life = self.get_life(game, this);
        match self.get_player(game, this) {
            MultiOption::None => format!("Unknown player gains {} life", life),
            MultiOption::One(player) => {
                let player = player.only().unwrap_player();
                format!("Player {} gains {} life", player, life)
            }
            MultiOption::Many(_) => format!("Multiple players gain {} life", life),
        }
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let life = self.get_life(game, this.as_ref());
        match self.get_player(game, this.as_ref()) {
            MultiOption::None => (),
            MultiOption::One(player) => {
                let player = player.only().unwrap_player();
                game.players[player].life += life;
            }
            MultiOption::Many(players) => {
                for entry in players.iter() {
                    // TODO: APNAP order
                    let player = entry.unwrap_player();
                    game.push_action(
                        this.clone(),
                        Box::new(GainLifeAction::new(
                            vec![IsAbsolutePlayer(player)],
                            vec![Literal(Selectable::Value(life))],
                        )),
                    )
                }
            }
        }
    }

    fn apply_template(&mut self, data: &PlaceholderData) {
        apply_template(&mut self.player, data);
        apply_template(&mut self.life, data);
    }

    fn get_hooks(&self, game: &Game, this: Option<&RcGameObject>) -> Vec<ActionHook> {
        match self.get_player(game, this) {
            MultiOption::One(player) => {
                let life = self.get_life(game, this);

                let mut data: PlaceholderData = Default::default();
                data.insert(PPlayer, player);
                data.insert(PLife, SelectionSet::new_value(life as i32));

                vec![ActionHook::new_auto_collect(ActionHookType::GainLife, data)]
            }
            _ => vec![], // No/many player = no hook
        }
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(Box::new(Self::new(
            json_to_filter(&json["player"])?,
            json_to_filter(&json["life"])?,
        )))
    }
}
impl TypedJson for GainLifeAction {
    fn type_name(&self) -> &'static str {
        "GainLifeAction"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "player" => filter_to_json(&self.player),
            "life" => filter_to_json(&self.life),
        }
    }
}
impl GainLifeAction {
    pub fn new(player: FilterList, life: FilterList) -> GainLifeAction {
        GainLifeAction { player, life }
    }

    fn get_player(&self, game: &Game, this: Option<&RcGameObject>) -> MultiOption {
        multifilter(&self.player, game, this)
    }

    fn get_life(&self, game: &Game, this: Option<&RcGameObject>) -> Life {
        run_filter_one(&self.life, game, this, Default::default())
            .0
            .unwrap_value() as Life
    }
}

#[derive(Clone, ActionBoxClone)]
pub struct MoveToGraveyard {
    regenerateable: bool,
}
impl Action for MoveToGraveyard {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let s = this.unwrap();
        format!("Move {} to the graveyard", s.borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let loc = this.unwrap();
        let zone = loc.borrow().zone;
        if zone.is_none() {
            // If it isn't in any zone, return none
            return;
        }
        let zone = zone.unwrap();
        let id = loc.borrow().id;
        let old = game.find_zone_mut(zone).remove_id(id).unwrap();
        let new = old.borrow().transform(ObjectType::Card);
        let owner = new.borrow().owner;
        game.graveyards[owner].push(new);
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(Box::new(Self::new(
            json["regenerateable"]
                .as_bool()
                .ok_or(ParseError::InvalidValue("MoveToGraveyard::regenerateable"))?,
        )))
    }
}
impl MoveToGraveyard {
    pub fn new(regenerateable: bool) -> MoveToGraveyard {
        MoveToGraveyard { regenerateable }
    }
}
impl TypedJson for MoveToGraveyard {
    fn type_name(&self) -> &'static str {
        "MoveToGraveyard"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "regenerateable" => JsonValue::Boolean(self.regenerateable),
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeleteAction {}
impl Action for DeleteAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let s = this.unwrap();
        format!("Delete {}", s.borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let loc = this.unwrap();
        let zone = loc.borrow().zone.unwrap();
        let id = loc.borrow().id;
        game.find_zone_mut(zone).remove_id(id); // Drop it
    }
}
impl DeleteAction {
    pub fn new() -> Self {
        Self {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct MoveToBattlefield {}
impl Action for MoveToBattlefield {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let s = this.unwrap();
        format!("Move {} to the battlefield", s.borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let loc = this.unwrap();
        let zone = loc.borrow().zone.unwrap();
        let id = loc.borrow().id;
        let old = game.find_zone_mut(zone).remove_id(id).unwrap();
        let new = old.borrow().transform(ObjectType::Permanent);
        game.battlefield.push(new);
    }
}
impl MoveToBattlefield {
    pub fn new() -> Self {
        Self {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct MoveToExile {}
impl Action for MoveToExile {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let s = this.unwrap();
        format!("Exile {}", s.borrow().sig())
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let loc = this.unwrap();
        let zone = loc.borrow().zone.unwrap();
        let id = loc.borrow().id;
        let old = game.find_zone_mut(zone).remove_id(id).unwrap();
        let new = old.borrow().transform(ObjectType::Card);
        game.find_zone_mut(ZoneType::Exile).push(new);
    }
}
impl MoveToExile {
    pub fn new() -> Self {
        Self {}
    }
}

// 106.4. When an effect instructs a player to add mana, that mana goes into a player’s mana pool. From there, it can be
// used to pay costs immediately, or it can stay in the player’s mana pool as unspent mana. [...]
#[derive(Clone, ActionBoxClone)]
pub struct AddManaAction {
    mana: Vec<Mana>,
    player: FilterList,
}
impl AddManaAction {
    pub fn new(mana: Vec<Mana>, player: FilterList) -> AddManaAction {
        AddManaAction { mana, player }
    }

    fn get_player(&self, game: &Game, this: Option<&RcGameObject>) -> MultiOption {
        multifilter(&self.player, game, this)
    }
}
impl Action for AddManaAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let mut str = String::from("Add ");
        for m in self.mana.clone() {
            str += &m.description();
        }
        str
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        match self.get_player(game, this.as_ref()) {
            MultiOption::None => (),
            MultiOption::One(player) => {
                let player = player.only().unwrap_player();
                game.players[player].mana_pool.extend(&self.mana);
            }
            MultiOption::Many(players) => {
                for entry in players.iter() {
                    // TODO: APNAP order
                    let player = entry.unwrap_player();
                    game.push_action(
                        this.clone(),
                        Box::new(AddManaAction::new(
                            self.mana.clone(),
                            vec![IsAbsolutePlayer(player)],
                        )),
                    )
                }
            }
        }
    }

    fn apply_template(&mut self, data: &PlaceholderData) {
        self.player = self
            .player
            .iter_mut()
            .map(|x| x.clone().apply_template(data))
            .collect();
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        let player = json_to_filter(&json["player"])?;
        let mut mana = vec![];
        for m in json["mana"].members() {
            mana.push(Mana::from_json(m)?);
        }

        Ok(Box::new(Self::new(mana, player)))
    }

    fn could_produce_mana(&self, _game: &Game, _this: Option<&RcGameObject>) -> Vec<Vec<Mana>> {
        vec![self.mana.clone()]
    }
}
impl TypedJson for AddManaAction {
    fn type_name(&self) -> &'static str {
        "AddManaAction"
    }

    fn get_json(&self) -> JsonValue {
        let mut mana = JsonValue::new_array();
        for m in &self.mana {
            mana.push(m.to_json()).unwrap();
        }
        object! {
            "player" => filter_to_json(&self.player),
            "mana" => mana,
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct TapObjectSymbolAction {
    object: FilterList,
}
impl TapObjectSymbolAction {
    pub fn new() -> TapObjectSymbolAction {
        TapObjectSymbolAction {
            object: vec![
                InZone(ZoneType::Battlefield),
                Is(Selectable::Placeholder(This)),
                Untapped,
            ],
        }
    }

    fn get_object(
        &self,
        game: &Game,
        this: Option<&RcGameObject>,
    ) -> SelectionSet<sstype::Resolved> {
        run_filter(&self.object, game, this, Default::default()).0
    }
}
impl Action for TapObjectSymbolAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "{T}".to_owned()
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let tap = self.get_object(game, this.as_ref()).clone();
        for ti in tap.iter() {
            ti.unwrap_gameobject().borrow_mut().tapped = true;
        }
    }

    fn get_usability(&self, game: &Game, this: Option<&RcGameObject>) -> Usability {
        let tap = self.get_object(game, this);
        if !tap.is_empty() {
            Usability::Usable
        } else {
            Usability::Unusable
        }
    }
}

#[derive(Clone, ActionBoxClone)]
pub struct CreateTokenAction {
    count: FilterList,
    token: Card,
}
impl CreateTokenAction {
    pub fn new(count: FilterList, token: Card) -> Self {
        Self { count, token }
    }

    fn get_count(&self, game: &Game, this: Option<&RcGameObject>) -> i32 {
        run_filter(&self.count, game, this, Default::default())
            .0
            .only()
            .unwrap_value()
    }
}
impl Action for CreateTokenAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Create {}", self.token.name)
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let count = self.get_count(game, this.as_ref());
        for _ in 0..count {
            game.find_zone_mut(ZoneType::Battlefield).insert_from_card(
                Rc::new(self.token.clone()),
                this.as_ref().unwrap().borrow().controller,
            );
        }
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        let count = json_to_filter(&json["count"])?;
        let token = json_to_card(&json["token"])?;

        Ok(Box::new(Self::new(count, token)))
    }
}
impl TypedJson for CreateTokenAction {
    fn type_name(&self) -> &'static str {
        "CreateTokenAction"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "count" => filter_to_json(&self.count),
            "token" => card_to_json(&self.token).unwrap(),
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct UntapAction {}
impl UntapAction {
    pub fn new() -> UntapAction {
        UntapAction {}
    }
}
impl Action for UntapAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Untap {}", this.unwrap().borrow().sig())
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        this.unwrap().borrow_mut().tapped = false;
    }
}

pub fn load_common_actions(registry: &mut TypedRegistry<dyn Action>) {
    registry.push(Box::from(PlayLandAction::new()));
    registry.push(Box::from(DrawCardAction::new(vec![])));
    registry.push(Box::from(GainLifeAction::new(vec![], vec![])));
    registry.push(Box::from(MoveToGraveyard::new(true)));
    registry.push(Box::from(MoveToBattlefield::new()));
    registry.push(Box::from(MoveToExile::new()));
    registry.push(Box::from(AddManaAction::new(vec![], vec![])));
    registry.push(Box::from(TapObjectSymbolAction::new()));
    registry.push(Box::from(UntapAction::new()));
    registry.push(Box::from(CreateTokenAction::new(
        vec![],
        Default::default(),
    )));
}

// ==============
// Static Effects
// ==============
#[derive(Clone, SEBoxClone)]
pub struct IncreasePT {
    onto: FilterList,
    power: FilterList,
    toughness: FilterList,
}
impl IncreasePT {
    pub fn new(onto: FilterList, power: FilterList, toughness: FilterList) -> Self {
        Self {
            onto,
            power,
            toughness,
        }
    }

    fn get_onto(
        &self,
        game: &Game,
        source: Option<&RcGameObject>,
    ) -> SelectionSet<sstype::Resolved> {
        run_filter(&self.onto, game, source, Default::default()).0
    }

    fn get_power(&self, game: &Game, source: Option<&RcGameObject>) -> i32 {
        run_filter_one(&self.power, game, source, Default::default())
            .0
            .unwrap_value()
    }

    fn get_toughness(&self, game: &Game, source: Option<&RcGameObject>) -> i32 {
        run_filter_one(&self.toughness, game, source, Default::default())
            .0
            .unwrap_value()
    }
}
impl StaticEffect for IncreasePT {
    fn modify_board(&self, game: &mut Game, source: &RcGameObject) {
        let objects = self.get_onto(game, Some(source));
        let power = self.get_power(game, Some(source));
        let toughness = self.get_toughness(game, Some(source));
        for fable in objects.iter() {
            let loc = fable.unwrap_gameobject();
            let mut obj = loc.borrow_mut();
            obj.power += power;
            obj.toughness += toughness;
        }
    }
    fn layer(&self) -> layers::Layer {
        layers::POWER_TOUGHNESS_CHANGING_EFFECTS
    }

    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn StaticEffect>, ParseError> {
        Ok(Box::new(Self::new(
            json_to_filter(&json["onto"])?,
            json_to_filter(&json["power"])?,
            json_to_filter(&json["toughness"])?,
        )))
    }
}
impl TypedJson for IncreasePT {
    fn type_name(&self) -> &'static str {
        "IncreasePT"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "onto" => filter_to_json(&self.onto),
            "power" => filter_to_json(&self.power),
            "toughness" => filter_to_json(&self.power),
        }
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct Lifelink {
    pub underlying: ReplacementEffect,
}
impl Lifelink {
    pub fn new() -> Self {
        Self {
            underlying: ReplacementEffect::new(
                vec![
                    Is(Selectable::Placeholder(PAttacker)),
                    Is(Selectable::Placeholder(This)),
                ],
                ActionHookType::DealDamage,
                true,
                vec![Box::new(GainLifeAction::new(
                    vec![ControllerOf(Selectable::Template(PAttacker))],
                    vec![Literal(Selectable::Template(PDamage))],
                ))],
            ),
        }
    }
}
impl StaticEffect for Lifelink {
    fn layer(&self) -> layers::Layer {
        self.underlying.layer()
    }
    fn as_replacement_effect(&self) -> Option<&ReplacementEffect> {
        Some(&self.underlying)
    }
}

// 702.127a Embalm is an activated ability that functions while the card with embalm is in a graveyard. “Embalm [cost]”
// means “[Cost], Exile this card from your graveyard: Create a token that’s a copy of this card, except it’s white, it
// has no mana cost, and it’s a Zombie in addition to its other types. Activate this ability only any time you could
// cast a sorcery.”
// TODO: This isn't the best way of doing this, maybe they should have different types or be a type of static ability
#[derive(Clone, SEBoxClone)]
pub struct Embalm {
    cost: Cost,
}
impl Embalm {
    pub fn new(cost: Cost) -> Self {
        Self { cost: cost.clone() }
    }
}
impl StaticEffect for Embalm {
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
    fn modify_board(&self, _game: &mut Game, source: &RcGameObject) {
        let mut new_card = (*source.borrow().card).clone();
        new_card.subtypes.push(CardSubtype::Zombie);
        new_card.cost = vec![];
        new_card.colours = vec![White];
        new_card.kind = CardKind::Token;

        let ability = ActivatedAbility::new(
            vec![
                Update(Box::new(vec![
                    ControllerOf(Selectable::Placeholder(This)),
                    Bind(A),
                ])),
                Is(Selectable::Placeholder(This)),
                InGraveyard(Selectable::Placeholder(A)),
            ],
            vec![
                Box::new(MoveToExile::new()),
                Box::new(ManaCostAction::new(self.cost.clone())),
            ],
            Box::new(CreateTokenAction::new(
                vec![Literal(Selectable::Value(1))],
                new_card,
            )),
            Speed::Sorcery,
        );

        source.borrow_mut().activated_abilities.push(ability);
    }
    fn load_json(&self, json: &JsonValue) -> Result<Box<dyn StaticEffect>, ParseError> {
        let cost = parse_cost(json["cost"].as_str().ok_or(ParseError::WrongType("cost"))?)?;
        Ok(Box::new(Self::new(cost)))
    }
}
impl TypedJson for Embalm {
    fn type_name(&self) -> &'static str {
        "Embalm"
    }

    fn get_json(&self) -> JsonValue {
        object! {
            "cost" => dump_cost(&self.cost),
        }
    }
}

pub fn load_common_static_effects(registry: &mut TypedRegistry<dyn StaticEffect>) {
    registry.push(Box::from(IncreasePT::new(vec![], vec![], vec![])));
    registry.push(Box::from(Lifelink::new()));
    registry.push(Box::from(Embalm::new(vec![])));
}
