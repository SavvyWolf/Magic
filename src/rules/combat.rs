// I don't want only some actions to be Default
#![allow(clippy::new_without_default)]

use abilities::{layers, SEBoxClone, StaticEffect};
use actions::{Action, ActionBoxClone, ActionEntry, ActionHook, ActionHookType, ExpectedUserInput};
use filter::Placeholder::*;
use filter::PlaceholderData;
use filter::Selectable;
use filter::SelectionSet;
use game::Game;
use gamestate::{AttackTarget, ObjectId, PlayerId, RcGameObject, ZoneType};
use magic_macros::{ActionBoxClone, SEBoxClone, TypedJson};
use parse::{TypedJson, TypedRegistry};
use utils::{get_attackers, get_blockers};

use std::any::Any;

// =======
// Actions
// =======

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct FinishDeclaringAttackersAction {}
impl Action for FinishDeclaringAttackersAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Finish declaring attackers".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Pop the top item off the action stack
        game.pop_action();

        // Have every creature attack
        let attackers: Vec<RcGameObject> = game
            .battlefield
            .iter()
            .filter(|o| {
                let ob = o.borrow();
                ob.attack_pending != ob.attacking
            })
            .cloned()
            .collect();

        for o in attackers {
            game.push_action(Some(o.clone()), Box::new(CommitAttackAction::new()));
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct FinishDeclaringBlockersAction {}
impl Action for FinishDeclaringBlockersAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Finish declaring blockers".to_owned()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Pop the top item off the action stack
        game.pop_action();

        // Have every creature block
        let blockers: Vec<RcGameObject> = game
            .battlefield
            .iter()
            .filter(|o| {
                let ob = o.borrow();
                ob.block_pending != ob.blocking
            })
            .cloned()
            .collect();

        for o in blockers {
            game.push_action(Some(o.clone()), Box::new(CommitBlockAction::new()));
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeclareAttackersAction {
    player: PlayerId,
}
impl DeclareAttackersAction {
    pub fn new(player: PlayerId) -> Self {
        DeclareAttackersAction { player }
    }
}
impl Action for DeclareAttackersAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Player {} declares attackers", self.player)
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Repeat itself
        game.push_action(None, Box::new(DeclareAttackersAction::new(self.player)));
    }

    fn get_expected_user_input(
        &self,
        _game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (self.player, ExpectedUserInput::DeclareAttackers)
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeclareBlockersAction {
    player: PlayerId,
}
impl DeclareBlockersAction {
    pub fn new(player: PlayerId) -> Self {
        Self { player }
    }
}
impl Action for DeclareBlockersAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Player {} declares blockers", self.player)
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Repeat itself
        game.push_action(None, Box::new(DeclareBlockersAction::new(self.player)));
    }

    fn get_expected_user_input(
        &self,
        _game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (self.player, ExpectedUserInput::DeclareBlockers)
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeclareAttackAction {
    target: AttackTarget,
}
impl Action for DeclareAttackAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let target_str = match self.target {
            AttackTarget::Player(p) => format!("Player {}", p),
            _ => "???".to_owned(),
        };
        format!("{} -> {}", this.unwrap().borrow().sig(), target_str)
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        this.unwrap().borrow_mut().attack_pending = self.target;
    }
}
impl DeclareAttackAction {
    pub fn new(target: AttackTarget) -> Self {
        DeclareAttackAction { target }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeclareBlockAction {
    target: ObjectId,
}
impl DeclareBlockAction {
    pub fn new(target: ObjectId) -> Self {
        Self { target }
    }

    fn get_target<'a>(&self, game: &'a Game) -> Option<&'a RcGameObject> {
        game.battlefield.get_id(self.target)
    }
}
impl Action for DeclareBlockAction {
    fn description(&self, game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let target = self
            .get_target(game)
            .expect("DeclareBlockAction with no target")
            .borrow();
        format!("{} -> {}", target.sig(), this.unwrap().borrow().sig())
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        this.unwrap().borrow_mut().block_pending = Some(self.target);
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct WithdrawAttackAction {}
impl Action for WithdrawAttackAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Withdraw {}", this.unwrap().borrow().sig())
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        this.unwrap().borrow_mut().attack_pending = AttackTarget::None;
    }
}
impl WithdrawAttackAction {
    pub fn new() -> Self {
        WithdrawAttackAction {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct WithdrawBlockAction {}
impl Action for WithdrawBlockAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Withdraw {}", this.unwrap().borrow().sig())
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        this.unwrap().borrow_mut().block_pending = None;
    }
}
impl WithdrawBlockAction {
    pub fn new() -> Self {
        Self {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct CommitAttackAction {}
impl Action for CommitAttackAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Commit {}'s attack", this.unwrap().borrow().sig())
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        let mut this = this.as_ref().unwrap().borrow_mut();
        let target = this.attack_pending;
        this.attacking = target;
        this.attack_pending = AttackTarget::None;
        this.tapped = true;
    }
}
impl CommitAttackAction {
    pub fn new() -> Self {
        Self {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct CommitBlockAction {}
impl Action for CommitBlockAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!("Commit {}'s block", this.unwrap().borrow().sig())
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        let mut this = this.as_ref().unwrap().borrow_mut();
        let target = this.block_pending;
        this.blocking = target;
        this.block_pending = None;
    }
}
impl CommitBlockAction {
    pub fn new() -> Self {
        Self {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeclareSingleAttackerDamageOrderAction {}
impl Action for DeclareSingleAttackerDamageOrderAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!(
            "Selecting {}'s damage assignment",
            this.unwrap().borrow().sig()
        )
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        game.push_action(this, self.box_clone());
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (
            game.get_priority(),
            ExpectedUserInput::DeclareAttackerAssignmentOrder,
        )
    }
}
impl DeclareSingleAttackerDamageOrderAction {
    pub fn new() -> Self {
        Self {}
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeclareAttackerDamageOrderAction {
    player: PlayerId,
}
impl Action for DeclareAttackerDamageOrderAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!(
            "Considering Player {}'s attacker damage assignment order",
            self.player
        )
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        // Find all the attackers
        let attackers = get_attackers(game);

        for a in attackers {
            if !get_blockers(game, &a).is_empty() {
                game.push_action(
                    Some(a),
                    Box::new(DeclareSingleAttackerDamageOrderAction::new()),
                );
            }
        }
    }
}
impl DeclareAttackerDamageOrderAction {
    pub fn new(player: PlayerId) -> Self {
        Self { player }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PrependAttackerDamageOrderAction {
    blocker: ObjectId,
}
impl Action for PrependAttackerDamageOrderAction {
    fn description(&self, game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        let obj = game.battlefield.get_id(self.blocker);
        match obj {
            None => "(Not present)".to_string(),
            Some(x) => x.borrow().sig(),
        }
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        let this = this.unwrap();
        this.borrow_mut().attack_assignment_order.push(self.blocker);

        // If all the other attackers are done, just pop the repeating action from the stack
        let mut blockers = get_blockers(game, &this.clone());
        let order = &this.borrow().attack_assignment_order;

        blockers.retain(|x| !order.contains(&x.borrow().id));

        if blockers.is_empty() {
            game.pop_action();
        }
    }
}
impl PrependAttackerDamageOrderAction {
    pub fn new(blocker: ObjectId) -> Self {
        Self { blocker }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DeclareDamageAction {
    use_rest: bool,
    offset: usize,
    result: Option<i32>,
}
impl DeclareDamageAction {
    pub fn new(use_rest: bool, offset: usize) -> Self {
        Self {
            use_rest,
            offset,
            result: None,
        }
    }

    pub fn target<'a>(&self, game: &'a Game, this: &RcGameObject) -> &'a RcGameObject {
        game.find_zone(ZoneType::Battlefield)
            .get_id(this.borrow().attack_assignment_order[self.offset])
            .unwrap()
    }
}
impl Action for DeclareDamageAction {
    fn description(&self, game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!(
            "Assign damage to {}",
            self.target(game, this.unwrap()).borrow().sig()
        )
    }

    fn perform(&self, _game: &mut Game, this: Option<RcGameObject>) {
        let this = this.unwrap();
        let mut this_mut = this.borrow_mut();

        let damage = if self.use_rest {
            this_mut.attack_damage_remaining
        } else {
            self.result.unwrap()
        };

        this_mut.attack_damage_remaining -= damage;
        if this_mut.attack_damage_values.len() <= self.offset {
            this_mut.attack_damage_values.resize(self.offset + 1, 0);
        }
        this_mut.attack_damage_values[self.offset] = damage;
    }

    fn get_expected_user_input(
        &self,
        game: &Game,
        this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        if self.result.is_some() {
            return (0, ExpectedUserInput::None);
        }

        // If we've to use the rest of the damage, don't prompt the user
        if self.use_rest {
            return (0, ExpectedUserInput::None);
        }

        let remaining = this.unwrap().borrow().attack_damage_remaining;
        let toughness = self.target(game, this.unwrap()).borrow().toughness;

        if remaining <= toughness {
            (
                game.get_ap(),
                ExpectedUserInput::DeclareAttackerAssignment(remaining, remaining + 1),
            )
        } else {
            (
                game.get_ap(),
                ExpectedUserInput::DeclareAttackerAssignment(toughness, remaining + 1),
            )
        }
    }

    fn use_value(&mut self, value: i32) {
        self.result = Some(value);
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct PerformAllDamageAction {}
impl PerformAllDamageAction {
    pub fn new() -> Self {
        Self {}
    }
}
impl Action for PerformAllDamageAction {
    fn description(&self, _game: &Game, _this: Option<&RcGameObject>, _is_cost: bool) -> String {
        "Perform damage".to_string()
    }

    fn perform(&self, game: &mut Game, _this: Option<RcGameObject>) {
        for a in get_attackers(game) {
            let blockers = get_blockers(game, &a);
            let have_blockers = !blockers.is_empty();
            for b in blockers {
                let ab = a.borrow();
                let attacker_ind = ab
                    .attack_assignment_order
                    .iter()
                    .position(|x| x == &b.borrow().id)
                    .unwrap();
                let attacker_dmg = ab.attack_damage_values[attacker_ind];
                game.push_action(
                    Some(a.clone()),
                    Box::new(DealDamageToAction::new(
                        Selectable::GameObject(b.clone()),
                        Selectable::Value(attacker_dmg),
                    )),
                );
                // TODO Blocker damage assignment order
                game.push_action(
                    Some(b.clone()),
                    Box::new(DealDamageToAction::new(
                        Selectable::GameObject(a.clone()),
                        Selectable::Value(b.borrow().power),
                    )),
                );
            }

            // Player (or trample)
            if !have_blockers {
                game.push_action(
                    Some(a.clone()),
                    Box::new(DealDamageToAttackTargetAction::new(
                        a.borrow().attack_damage_remaining,
                    )),
                );
            }
        }
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DealDamageToAction {
    object: Selectable,
    damage: Selectable,
}
impl DealDamageToAction {
    pub fn new(object: Selectable, damage: Selectable) -> Self {
        Self { object, damage }
    }

    pub fn target(&self, this: Option<&RcGameObject>, _game: &Game) -> RcGameObject {
        self.object
            .resolve(&this.unwrap().borrow().stored_data)
            .only()
            .unwrap_gameobject()
            .clone()
    }
    pub fn damage(&self, this: Option<&RcGameObject>, _game: &Game) -> i32 {
        self.damage
            .resolve(&this.unwrap().borrow().stored_data)
            .only()
            .unwrap_value()
    }
}
impl Action for DealDamageToAction {
    fn description(&self, game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        format!(
            "{} deals {} damage to {}",
            this.unwrap().borrow().sig(),
            self.damage(this, game),
            self.target(this, game).borrow().sig()
        )
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        self.target(this.as_ref(), game).borrow_mut().marked_damage +=
            self.damage(this.as_ref(), game);
    }

    fn get_hooks(&self, game: &Game, this: Option<&RcGameObject>) -> Vec<ActionHook> {
        let mut data: PlaceholderData = Default::default();
        data.insert(
            PDefender,
            SelectionSet::new_gameobject(this.unwrap().clone()),
        );
        data.insert(
            PAttacker,
            SelectionSet::new_gameobject(self.target(this, game).clone()),
        );
        data.insert(PDamage, SelectionSet::new_value(self.damage(this, game)));

        vec![ActionHook::new_auto_collect(
            ActionHookType::DealDamage,
            data,
        )]
    }
}

#[derive(Clone, TypedJson, ActionBoxClone)]
pub struct DealDamageToAttackTargetAction {
    damage: i32,
}
impl DealDamageToAttackTargetAction {
    pub fn new(damage: i32) -> Self {
        Self { damage }
    }
}
impl Action for DealDamageToAttackTargetAction {
    fn description(&self, _game: &Game, this: Option<&RcGameObject>, _is_cost: bool) -> String {
        match this.unwrap().borrow().attacking {
            AttackTarget::None => format!(
                "{} deals {} damage to nothing(?)",
                this.unwrap().borrow().sig(),
                self.damage
            ),
            AttackTarget::Player(p) => format!(
                "{} deals {} damage to Player {}",
                this.unwrap().borrow().sig(),
                self.damage,
                p
            ),
        }
    }

    fn perform(&self, game: &mut Game, this: Option<RcGameObject>) {
        match this.unwrap().borrow().attacking {
            AttackTarget::None => (), // Can this happen?
            AttackTarget::Player(p) => {
                game.players[p].life -= self.damage;
            }
        };
    }

    fn get_hooks(&self, _game: &Game, this: Option<&RcGameObject>) -> Vec<ActionHook> {
        let mut data: PlaceholderData = Default::default();
        match this.unwrap().borrow().attacking {
            AttackTarget::None => (),
            AttackTarget::Player(p) => {
                data.insert(PDefender, SelectionSet::new_player(p));
            }
        };
        data.insert(
            PAttacker,
            SelectionSet::new_gameobject(this.unwrap().clone()),
        );
        data.insert(PDamage, SelectionSet::new_value(self.damage as i32));

        vec![ActionHook::new_auto_collect(
            ActionHookType::DealDamage,
            data,
        )]
    }
}

// ==============
// Static Effects
// ==============

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct AttackersMayBeDeclared {}
impl AttackersMayBeDeclared {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for AttackersMayBeDeclared {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        let mut out = vec![];
        if expected != ExpectedUserInput::DeclareAttackers {
            return vec![];
        }

        // Compute the list of targets
        let mut targets: Vec<AttackTarget> = vec![];
        for p in 0..game.num_players() {
            if p != player {
                targets.push(AttackTarget::Player(p));
            }
        }

        // And create the action list
        for o in game.battlefield.iter() {
            let ob = o.borrow();
            if ob.can_attack(player) {
                if ob.attack_pending != AttackTarget::None {
                    out.push(ActionEntry::new(
                        Box::new(WithdrawAttackAction::new()),
                        Some(o.clone()),
                    ));
                } else {
                    for t in &targets {
                        out.push(ActionEntry::new(
                            Box::new(DeclareAttackAction::new(*t)),
                            Some(o.clone()),
                        ));
                    }
                }
            }
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct BlockersMayBeDeclared {}
impl BlockersMayBeDeclared {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for BlockersMayBeDeclared {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        player: PlayerId,
    ) -> Vec<ActionEntry> {
        let mut out = vec![];
        if expected != ExpectedUserInput::DeclareBlockers {
            return vec![];
        }

        // Compute the list of attackers
        let attackers: Vec<ObjectId> = game
            .battlefield
            .iter()
            .filter_map(|x| {
                if x.borrow().attacking != AttackTarget::None {
                    Some(x.borrow().id)
                } else {
                    None
                }
            })
            .collect();

        // And create the action list
        for o in game.battlefield.iter() {
            let ob = o.borrow();
            if ob.block_pending != None {
                out.push(ActionEntry::new(
                    Box::new(WithdrawBlockAction::new()),
                    Some(o.clone()),
                ));
            } else {
                let ob = o.borrow();
                if ob.can_block(player) {
                    for t in &attackers {
                        out.push(ActionEntry::new(
                            Box::new(DeclareBlockAction::new(*t)),
                            Some(o.clone()),
                        ));
                    }
                }
            }
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

#[derive(Clone, TypedJson, SEBoxClone)]
pub struct AttackerDamageOrderMayBeDeclared {}
impl AttackerDamageOrderMayBeDeclared {
    pub fn new() -> Self {
        Self {}
    }
}
impl StaticEffect for AttackerDamageOrderMayBeDeclared {
    fn get_actions(
        &self,
        game: &Game,
        _source: &RcGameObject,
        expected: ExpectedUserInput,
        _player: PlayerId,
    ) -> Vec<ActionEntry> {
        let mut out = vec![];
        if expected != ExpectedUserInput::DeclareAttackerAssignmentOrder {
            return vec![];
        }

        // Compute the list of attackers
        let attacker = game.peek_action().object.as_ref().unwrap();
        let already_declared = &attacker.borrow().attack_assignment_order;

        let mut blockers = get_blockers(game, &attacker);
        blockers.retain(|x| !already_declared.contains(&x.borrow().id));

        for b in blockers {
            out.push(ActionEntry::new(
                Box::new(PrependAttackerDamageOrderAction::new(b.borrow().id)),
                Some(attacker.clone()),
            ));
        }
        out
    }
    fn layer(&self) -> layers::Layer {
        layers::ENGINE_COLLECT_ABILITIES
    }
}

pub fn load_combat_static_effects(registry: &mut TypedRegistry<dyn StaticEffect>) {
    registry.push(Box::from(AttackersMayBeDeclared::new()));
    registry.push(Box::from(BlockersMayBeDeclared::new()));
    registry.push(Box::from(AttackerDamageOrderMayBeDeclared::new()));
}
