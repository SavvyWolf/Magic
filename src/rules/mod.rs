mod combat;
mod common;
mod engine;

pub use self::combat::*;
pub use self::common::*;
pub use self::engine::*;

use abilities::StaticEffect;
use actions::Action;
use parse::TypedRegistry;

pub fn get_actions_registry() -> TypedRegistry<dyn Action> {
    let mut registry = Default::default();

    load_engine_actions(&mut registry);
    load_common_actions(&mut registry);

    registry
}

pub fn get_static_effects_registry() -> TypedRegistry<dyn StaticEffect> {
    let mut registry = Default::default();

    load_common_static_effects(&mut registry);
    load_engine_static_effects(&mut registry);
    load_combat_static_effects(&mut registry);

    registry
}
