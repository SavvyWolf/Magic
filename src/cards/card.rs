use super::mana::Colour;
use super::mana::Cost;
use super::properties::CardSubtype;
use super::properties::CardSupertype;
use super::properties::CardType;
use super::properties::DisplayColour;
use abilities::{ActivatedAbility, StaticEffect, TriggeredAbility};
use actions::Action;

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone)]
pub enum CardKind {
    /// A "normal" object backed by a "physical" card
    CardOrPermanent,
    /// Activated or Triggered ability
    Ability,
    /// Token on the battlefield
    Token,
}

#[derive(Clone)]
pub struct Card {
    pub name: String,
    pub types: Vec<CardType>,
    pub subtypes: Vec<CardSubtype>,
    pub supertypes: Vec<CardSupertype>,
    pub colours: Vec<Colour>,
    pub static_effects: Vec<Box<dyn StaticEffect>>,
    pub cast_choices: Vec<Box<dyn Action>>,
    pub resolution_effects: Vec<Box<dyn Action>>,
    pub triggered_abilities: Vec<TriggeredAbility>,
    pub activated_abilities: Vec<ActivatedAbility>,
    pub cost: Cost,
    pub power: i32,
    pub toughness: i32,
    pub loyalty: i32,
    pub rules_as_written: String,
    pub kind: CardKind,
}
impl Card {
    pub fn is_type(&self, card_type: CardType) -> bool {
        self.types.contains(&card_type)
    }
    pub fn is_subtype(&self, card_subtype: CardSubtype) -> bool {
        self.subtypes.contains(&card_subtype)
    }
    pub fn is_supertype(&self, card_supertype: CardSupertype) -> bool {
        self.supertypes.contains(&card_supertype)
    }

    pub fn display_colour(&self) -> DisplayColour {
        if self.colours.is_empty() {
            DisplayColour::Colorless
        } else if self.colours.len() > 1 {
            DisplayColour::Gold
        } else {
            match self.colours[0] {
                Colour::White => DisplayColour::White,
                Colour::Blue => DisplayColour::Blue,
                Colour::Black => DisplayColour::Black,
                Colour::Red => DisplayColour::Red,
                Colour::Green => DisplayColour::Green,
                Colour::Colorless => DisplayColour::Colorless,
            }
        }
    }
}
impl Default for Card {
    fn default() -> Card {
        Card {
            name: "".to_owned(),
            types: Default::default(),
            subtypes: Default::default(),
            supertypes: Default::default(),
            colours: vec![],
            static_effects: Default::default(),
            cast_choices: Default::default(),
            resolution_effects: Default::default(),
            triggered_abilities: Default::default(),
            activated_abilities: Default::default(),
            cost: vec![],
            power: 0,
            toughness: 0,
            loyalty: 0,
            rules_as_written: String::new(),
            kind: CardKind::CardOrPermanent,
        }
    }
}
impl PartialEq<Card> for Card {
    fn eq(&self, other: &Card) -> bool {
        self.name == other.name
    }
}
impl Eq for Card {}
