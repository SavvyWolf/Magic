mod card;
mod cardlist;
pub mod mana;
mod oracle;
pub mod properties;

pub use self::oracle::Oracle;

pub use self::card::Card;
pub use self::card::CardKind;

pub use self::cardlist::add_cards;

use game::Game;
use gamestate::ZoneType;

pub fn add_rules_card(game: &mut Game) {
    game.insert_by_name(ZoneType::Rules, "__Game Rules", 0);
}
