use parse::{parse_colour, ParseError};

use json::{object, JsonValue};
use std::fmt;

/// All of the five colours of magic, plus colorless
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Colour {
    White,
    Blue,
    Black,
    Red,
    Green,

    // Not real colours
    Colorless,
}

impl fmt::Display for Colour {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Colour::White => write!(f, "W"),
            Colour::Blue => write!(f, "U"),
            Colour::Black => write!(f, "B"),
            Colour::Red => write!(f, "R"),
            Colour::Green => write!(f, "G"),
            Colour::Colorless => write!(f, "C"),
        }
    }
}

/// A cost that may appear in a mana cost
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum CostEntry {
    /// {M}
    Basic(Colour),
    /// {M/N}
    Hybrid(Colour, Colour),
    /// {2/M}
    GenericHybrid(Colour),
    /// {M/P}
    Phyrexian(Colour),
    /// {1}, {2}, ...
    Generic(usize),
    /// {X}
    X,
    /// {S}
    Snow,
}
impl fmt::Display for CostEntry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CostEntry::Basic(c) => write!(f, "{{{}}}", c),
            CostEntry::Generic(n) => write!(f, "{{{}}}", n),
            CostEntry::Hybrid(a, b) => write!(f, "{{{}/{}}}", a, b),
            CostEntry::GenericHybrid(c) => write!(f, "{{2/{}}}", c),
            CostEntry::Phyrexian(c) => write!(f, "{{{}/P}}", c),
            CostEntry::X => write!(f, "{{X}}"),
            CostEntry::Snow => write!(f, "{{S}}"),
        }
    }
}

pub type Cost = Vec<CostEntry>;

/// A unit of Mana that can be stored in a mana pool
///
/// This also stores any restrictions on the mana's usage (e.g. Snow, creature spells only)
#[derive(Clone, Copy, Hash, PartialEq, Eq, Debug)]
pub struct Mana {
    colour: Colour,
    snow: bool,
}
impl Mana {
    /// Creates a new Mana object which has no restrictions
    pub fn new_unconditional(colour: Colour) -> Mana {
        Mana {
            colour,
            snow: false,
        }
    }
    /// Creates a new snow Mana object which has no restrictions
    pub fn new_snow(colour: Colour) -> Mana {
        Mana { colour, snow: true }
    }

    /// Returns a string representation of the Mana
    pub fn description(self) -> String {
        format!("{{{}}}", self.colour)
    }

    /// Given a single Cost, can this Mana pay it?
    ///
    /// Note this will always be true for X, Generic or GenericHybrid, even if they require multiple mana
    pub fn could_pay(self, cost: &CostEntry) -> bool {
        match cost {
            CostEntry::Basic(c) => self.colour == *c,
            CostEntry::Hybrid(a, b) => self.colour == *a || self.colour == *b,
            CostEntry::Generic(_) => true,
            CostEntry::GenericHybrid(_) => true,
            CostEntry::Phyrexian(_) => true,
            CostEntry::X => true,
            CostEntry::Snow => self.snow,
        }
    }

    pub fn to_json(self) -> JsonValue {
        object! {
            "colour" => JsonValue::String(format!("{}", self.colour)),
            "snow" => JsonValue::Boolean(self.snow)
        }
    }

    pub fn from_json(json: &JsonValue) -> Result<Mana, ParseError> {
        let snow = json["snow"]
            .as_bool()
            .ok_or(ParseError::WrongType("snow"))?;
        let colour = parse_colour(
            json["colour"]
                .as_str()
                .ok_or(ParseError::WrongType("colour"))?,
        )?;

        Ok(Mana { snow, colour })
    }
}
impl fmt::Display for Mana {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}
