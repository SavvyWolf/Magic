use super::properties::CardSubtype::*;
use super::properties::CardType::*;
use super::{Card, Oracle};
use cards::mana::Colour::*;
use cards::mana::CostEntry::*;
use filter::Filter::*;
use filter::Placeholder;
use filter::Placeholder::*;
use filter::Selectable;
use gamestate::ZoneType::*;
use rules::*;

/// Add sample selection of cards, mostly for testing
///
/// Currently incomplete
pub fn add_cards(oracle: &mut Oracle) {
    oracle.insert(Card {
        name: "Sacred Cat".to_owned(),
        cost: vec![Basic(White)],
        types: vec![Creature],
        subtypes: vec![Cat],
        colours: vec![White],
        power: 1,
        toughness: 1,
        static_effects: vec![
            Box::new(Lifelink::new()),
            Box::new(Embalm::new(vec![Basic(White)])),
        ],
        ..Default::default()
    });
    oracle.insert(Card {
        name: "Regal Caracal".to_owned(),
        cost: vec![Generic(3), Basic(White), Basic(White)],
        types: vec![Creature],
        subtypes: vec![Cat],
        colours: vec![White],
        power: 3,
        toughness: 3,
        static_effects: vec![Box::new(IncreasePT::new(
            vec![
                InZone(Battlefield),
                HasSubtype(Cat),
                Not(Box::new(Is(Selectable::Placeholder(This)))),
            ],
            vec![Literal(Selectable::Value(1))],
            vec![Literal(Selectable::Value(1))],
        ))],
        ..Default::default()
    });
    oracle.insert(Card {
        name: "Young Wolf".to_owned(),
        cost: vec![Basic(Green)],
        types: vec![Creature],
        colours: vec![Green],
        power: 1,
        toughness: 1,
        ..Default::default()
    });
    oracle.insert(Card {
        name: "Raptor Hatchling".to_owned(),
        cost: vec![Generic(1), Basic(Red)],
        types: vec![Creature],
        colours: vec![Red],
        power: 1,
        toughness: 1,
        ..Default::default()
    });
    oracle.insert(Card {
        name: "Territorial Hammerskull".to_owned(),
        cost: vec![Generic(2), Basic(White)],
        types: vec![Creature],
        colours: vec![White],
        power: 2,
        toughness: 3,
        ..Default::default()
    });
    oracle.insert(Card {
        name: "Revitalize".to_owned(),
        cost: vec![Generic(1), Basic(White)],
        types: vec![Instant],
        colours: vec![White],
        resolution_effects: vec![
            Box::new(DrawCardAction::new(vec![ControllerOf(
                Selectable::Placeholder(This),
            )])),
            Box::new(GainLifeAction::new(
                vec![ControllerOf(Selectable::Placeholder(This))],
                vec![Literal(Selectable::Value(3))],
            )),
        ],
        ..Default::default()
    });
    oracle.insert(Card {
        name: "Shock, but white".to_owned(),
        cost: vec![Basic(White)],
        types: vec![Instant],
        colours: vec![White],
        cast_choices: vec![Box::new(SelectTargetAction::new(
            Placeholder::A,
            vec![HasType(Creature), InZone(Battlefield)],
            vec![Literal(Selectable::Value(1))],
            vec![Literal(Selectable::Value(1))],
        ))],
        resolution_effects: vec![Box::new(DealDamageToAction::new(
            Selectable::Placeholder(A),
            Selectable::Value(2),
        ))],
        ..Default::default()
    });
}
