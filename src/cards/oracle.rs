use super::cardlist::add_cards;
use super::Card;
use parse::{savmtg, ParseError};

use std::fs::DirBuilder;
use std::ops::Deref;
use std::path::PathBuf;
use std::rc::Rc;

#[derive(Clone)]
pub struct Oracle {
    cards: Vec<Rc<Card>>,
    dir: PathBuf,
    pub silent: bool,
}

impl Oracle {
    pub fn new(dir: PathBuf) -> Oracle {
        Oracle {
            cards: Vec::new(),
            dir,
            silent: false,
        }
    }

    pub fn get_card_by_name(&mut self, name: &str) -> Option<Rc<Card>> {
        let result = self.cards.iter().find(|x| x.name == name);
        match result {
            None => {
                self.load(name);
                let result = self.cards.iter().find(|x| x.name == name);
                result.cloned()
            }
            Some(c) => Some(c.clone()),
        }
    }

    pub fn remove_card_by_name(&mut self, name: &str) {
        self.cards.retain(|x| x.name != name);
    }

    pub fn commit_card_by_name(&mut self, name: &str) {
        let card = self.get_card_by_name(name).unwrap().deref().clone();
        let path = self.card_file(name);

        // Create the folder if it doesn't exist
        if !path.parent().unwrap().exists() {
            let db = DirBuilder::new();
            db.create(path.parent().unwrap())
                .expect("Failed to create card directory");
        }

        savmtg::dump(path.to_str().unwrap(), &[card]).expect("Failed to commit card");
    }

    pub fn insert(&mut self, card: Card) {
        self.cards.push(Rc::new(card));
    }

    pub fn load_included(&mut self) {
        add_cards(self)
    }

    fn card_file(&self, card: &str) -> PathBuf {
        PathBuf::from(format!(
            "{}.json",
            self.dir
                .join(card[0..1].to_string())
                .join(card)
                .to_str()
                .unwrap()
        ))
    }

    fn load(&mut self, card: &str) {
        let path = self.card_file(card);

        match self.load_from_file(&path.to_str().unwrap()) {
            Ok(_) => (),
            Err(_) => {
                if !self.silent {
                    eprintln!(
                        "WARNING: Attempted to import {}, but it couldn't be loaded",
                        card
                    )
                }
            }
        }
    }

    pub fn get_list(&self) -> &Vec<Rc<Card>> {
        &self.cards
    }

    pub fn load_list(&mut self, cards: Vec<Card>) {
        for c in cards {
            self.cards.push(Rc::new(c));
        }
    }

    pub fn load_from_file(&mut self, file: &str) -> Result<(), ParseError> {
        self.load_list(savmtg::load(file)?);
        Ok(())
    }
}
