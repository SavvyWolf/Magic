use super::Action;
use super::ActionEntry;
use game::Game;
use gamestate::ObjectUid;
use gamestate::RcGameObject;
use rules::PanicOnEmptyStackAction;

use std::collections::HashSet;

#[derive(Clone)]
pub struct ActionStackEntry {
    pub object: Option<RcGameObject>,
    pub action: Box<dyn Action>,
    modified_by: HashSet<(ObjectUid, usize)>, // Uid, offset
}

impl ActionStackEntry {
    pub fn new(object: Option<RcGameObject>, action: Box<dyn Action>) -> ActionStackEntry {
        ActionStackEntry {
            object,
            action,
            modified_by: Default::default(),
        }
    }

    pub fn copy_modified_by(&mut self, from: &ActionStackEntry) {
        for v in &from.modified_by {
            self.modified_by.insert(*v);
        }
    }

    pub fn insert_modified_by(&mut self, object_id: ObjectUid, offset: usize) {
        self.modified_by.insert((object_id, offset));
    }

    pub fn is_modified_by(&self, object_id: ObjectUid, offset: usize) -> bool {
        self.modified_by.contains(&(object_id, offset))
    }

    pub fn as_action_entry(&self) -> ActionEntry {
        ActionEntry::new(self.action.clone(), self.object.clone())
    }
}

/// The ActionStack maintains a list of actions that are going to be performed
///
/// The engine runs by pushing and popping Actions from this stack. The Game has one of these, and it's expected that
/// Actions add or remove other Actions from this stack.
///
/// Each entry on the stack consists of an Action, and the GameObject it is attached to.
///
/// ## Panics
///
/// The stack starts with an Action to panic! as its first entry, if the stack runs out of Actions, this is performed.
#[derive(Clone)]
pub struct ActionStack {
    actions: Vec<ActionStackEntry>,
}
impl ActionStack {
    pub fn new() -> ActionStack {
        ActionStack {
            actions: vec![ActionStackEntry::new(
                None,
                Box::new(PanicOnEmptyStackAction {}),
            )],
        }
    }

    /// Push a new (GameObject, Action) pair onto the stack
    pub fn push(&mut self, object: Option<RcGameObject>, action: Box<dyn Action>) {
        self.actions.push(ActionStackEntry::new(object, action));
    }

    /// Push a raw ActionStackEntry onto the stack
    pub fn push_entry(&mut self, entry: ActionStackEntry) {
        self.actions.push(entry);
    }

    /// Pop the (GameObject, Action) from the top of the stack, and return it
    pub fn pop(&mut self) -> ActionStackEntry {
        self.actions.pop().expect("Action stack is empty")
    }

    /// Return a reference to the top of the stack, but don't pop it
    pub fn top(&self) -> &ActionStackEntry {
        self.actions.last().expect("Action stack is empty")
    }

    /// Return a mutable reference to the top of the stack, but don't pop it
    pub fn top_mut(&mut self) -> &mut ActionStackEntry {
        self.actions.last_mut().expect("Action stack is empty")
    }

    /// Pretty print the ActionStack
    ///
    /// This is mostly for debugging.
    pub fn print(&self, game: &Game) {
        print!("[");
        let mut first = true;
        for entry in self.actions.iter() {
            if !first {
                print!(", ");
            } else {
                first = false;
            }
            print!(
                "{}",
                entry.action.description(game, entry.object.as_ref(), false)
            );
        }
        println!("]");
    }
}
impl Default for ActionStack {
    fn default() -> Self {
        ActionStack::new()
    }
}
