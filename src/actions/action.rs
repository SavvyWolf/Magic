use cards::mana::Mana;
use filter::{sstype, PlaceholderData, SelectionSet};
use game::Game;
use gamestate::{PlayerId, RcGameObject};
use json::JsonValue;
use parse::{ParseError, TypedJson};

use std::any;

use strum_macros::{Display, EnumString};

/// Actions may request the user make a choice, if they do, they return one of these enums
///
/// These enums represent what is asked of the user.
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum ExpectedUserInput {
    /// The user is not required to make any choice
    None,
    /// The user has priority and may do things like casting a spell, activating an ability, etc.
    PriorityChoice,
    /// The user has the ability to activate mana abilities
    ActivateManaAbilities,
    /// The user will take mana out of their mana pool to pay for a mana cost
    MakeManaPayment,
    /// The user will declare attackers
    DeclareAttackers,
    /// The user will declare blockers
    DeclareBlockers,
    /// The user is deciding on the attacker's damage assignment order
    DeclareAttackerAssignmentOrder,
    /// The user is deciding on the attacker's damage assignment (min, max)
    DeclareAttackerAssignment(i32, i32),
    /// The user should put a triggered ability onto the stack
    PutTriggeredAbilityOnStack,
    /// The user should select a target
    SelectTarget,
}

impl ExpectedUserInput {
    pub fn get_input_kind(self) -> UserInputKind {
        match self {
            ExpectedUserInput::None => UserInputKind::None,
            ExpectedUserInput::PriorityChoice => UserInputKind::ActionChoice,
            ExpectedUserInput::ActivateManaAbilities => UserInputKind::ActionChoice,
            ExpectedUserInput::MakeManaPayment => UserInputKind::ActionChoice,
            ExpectedUserInput::DeclareAttackers => UserInputKind::ActionChoice,
            ExpectedUserInput::DeclareBlockers => UserInputKind::ActionChoice,
            ExpectedUserInput::DeclareAttackerAssignmentOrder => UserInputKind::ActionChoice,
            ExpectedUserInput::DeclareAttackerAssignment(a, b) => UserInputKind::Range(a, b),
            ExpectedUserInput::PutTriggeredAbilityOnStack => UserInputKind::ActionChoice,
            ExpectedUserInput::SelectTarget => UserInputKind::ActionChoice,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum UserInputKind {
    None,
    ActionChoice,
    Range(i32, i32),
}

#[derive(EnumString, Display, Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum ActionHookType {
    Draw,
    GainLife,
    DealDamage,
}

/// Actions have a "Usability", which provides a hint to the viewer whether an action can be performed or not
///
/// "Performed" has various meanings depending on the action, but this enum should be used as a hint to the frontend
/// to whether an action should be visible or not.
///
/// For example, if a spell can be cast by a known method, such as using the mana in the pool, the cast spell action
/// should be `Usable`. However, if it is not clear whether a spell could be cast, we still want to let the user attempt
/// to cast it. In this case `Unlikely` should be returned, and the frontend should hide the option until the user
/// explictly requests it.
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Usability {
    /// The action is usable in all conditions
    Usable,
    /// The action is usable only in certain conditions that the game engine is not aware of
    Unlikely,
    /// The action cannot be used
    Unusable,
}

#[derive(Clone)]
pub struct ActionHook {
    pub hook_type: ActionHookType,
    pub args: SelectionSet<sstype::Resolved>,
    pub data: PlaceholderData,
}
impl ActionHook {
    pub fn new(
        hook_type: ActionHookType,
        args: SelectionSet<sstype::Resolved>,
        data: PlaceholderData,
    ) -> ActionHook {
        ActionHook {
            hook_type,
            args,
            data,
        }
    }

    pub fn new_auto_collect(hook_type: ActionHookType, data: PlaceholderData) -> ActionHook {
        let mut args: SelectionSet<sstype::Resolved> = Default::default();
        for (_key, val) in data.iter() {
            for sel in val.iter() {
                args.insert(sel.clone());
            }
        }
        ActionHook {
            hook_type,
            args,
            data,
        }
    }
}

/// An action entry is a struct containing an Action and the GameObject the action is performed on
///
/// The GameObject may be None, in which case the action has no associated object.
pub struct ActionEntry {
    pub action: Box<dyn Action>,
    pub object: Option<RcGameObject>,
}
impl ActionEntry {
    pub fn new(action: Box<dyn Action>, object: Option<RcGameObject>) -> ActionEntry {
        ActionEntry { action, object }
    }
}
impl Clone for ActionEntry {
    fn clone(&self) -> ActionEntry {
        ActionEntry {
            action: (*self.action).box_clone(),
            object: match &self.object {
                None => None,
                Some(x) => Some(x.clone()),
            },
        }
    }
}

pub trait ActionBoxClone {
    /// Return a Box containing a clone of this Action.
    fn box_clone(&self) -> Box<dyn Action>;

    fn as_any(&self) -> &dyn any::Any;
}

/// An Action is the smallest "game action" that may be performed
///
/// Everything that is done in the game is done through Actions. This includes card effects such as drawing cards and
/// also things like paying mana. Actions typically are added to the ActionStack, and resolve in terms of that stack.
///
/// Actions may also request input from the user (if get_expected_user_input returns anything but None). The Action
/// should assume that the requested input is placed on the action stack on top of it.
///
/// Actions are not linked to the objects they are sourced from. Most methods take a Game and GameObject instance which
/// represent the full game and their source, respectively. The GameObject may be null if they effect was created by
/// the engine.
pub trait Action: TypedJson + ActionBoxClone + any::Any {
    /// Returns a user-facing string description of this Action
    ///
    /// When the user is given a number of options, this string may be displayed to them.
    fn description(&self, game: &Game, this: Option<&RcGameObject>, is_cost: bool) -> String;
    /// The Action should update the game object to perform the requested Action
    ///
    /// The action stack may be modified, and this will already have been popped from the stack.
    fn perform(&self, game: &mut Game, this: Option<RcGameObject>);
    /// Return the type of Action that the provided player should provide in response to this Action
    ///
    /// By default, this returns (0, None), meaning no user input is expected. The player returned is the player that
    /// should take the action.
    fn get_expected_user_input(
        &self,
        _game: &Game,
        _this: Option<&RcGameObject>,
    ) -> (PlayerId, ExpectedUserInput) {
        (0, ExpectedUserInput::None)
    }

    /// Returns a list of action hooks which can be used by replacement effects
    ///
    /// These hooks can be used to identify the action without using its type. An action may provide multiple hooks with
    /// different or the same type, depending on the action.
    fn get_hooks(&self, _game: &Game, _this: Option<&RcGameObject>) -> Vec<ActionHook> {
        vec![]
    }

    /// Implementors should call apply_template on an filter lists they own
    ///
    /// This updates references to templates to their actual values, and is used (among other things) for when an action
    /// is loaded from a replacement hook
    fn apply_template(&mut self, _data: &PlaceholderData) {}

    /// Load an action of this type from a JsonValue
    ///
    /// The "self" value here isn't meaningful, but is used to handle dynamic dispatch. The default implementation calls
    /// clone, so you don't need to implement this if you have no properties. The JsonValue provided will be the same
    /// as that output by `to_json`.
    fn load_json(&self, _json: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        Ok(self.box_clone())
    }

    fn get_usability(&self, _game: &Game, _this: Option<&RcGameObject>) -> Usability {
        Usability::Usable
    }

    fn could_produce_mana(&self, _game: &Game, _this: Option<&RcGameObject>) -> Vec<Vec<Mana>> {
        vec![]
    }

    fn use_value(&mut self, _value: i32) {
        panic!("Action given a value but was not expecting one")
    }
}

impl Clone for Box<dyn Action> {
    fn clone(&self) -> Box<dyn Action> {
        self.box_clone()
    }
}
