use abilities::Speed;
use actions::ActionEntry;
use actions::ExpectedUserInput;
use actions::ExpectedUserInput::*;
use actions::Usability;
use game::Game;
use rules::*;

#[derive(Default, Clone)]
pub struct AutoPasser {}

fn find_if_one<'a, T: 'static>(options: &'a [ActionEntry], game: &Game) -> Option<&'a ActionEntry> {
    // Only allow usable ones
    let filter = options
        .iter()
        .filter(|x| x.action.get_usability(game, x.object.as_ref()) == Usability::Usable);
    let filter = filter.filter(|x| {
        if let Some(aa) = x
            .action
            .as_any()
            .downcast_ref::<UseActivatedAbilityAction>()
        {
            if let Some(entry) = aa.get_entry(game, x.object.as_ref()) {
                return entry.speed != Speed::ManaAbility;
            }
        }
        true
    });
    if filter.count() != 1 {
        return Option::None;
    }
    for o in options {
        if o.action.as_any().is::<T>() {
            return Some(o);
        }
    }
    panic!("Couldn't pass");
}

impl AutoPasser {
    pub fn request_action_choice(
        &mut self,
        game: &Game,
        expected: ExpectedUserInput,
        options: Vec<ActionEntry>,
    ) -> Option<ActionEntry> {
        match expected {
            PriorityChoice => {
                // No options, use the only one
                find_if_one::<PassPriorityAction>(&options, game)
            }
            ActivateManaAbilities => Option::None,
            MakeManaPayment => Option::None,
            DeclareAttackers => {
                // No legal attackers
                find_if_one::<FinishDeclaringAttackersAction>(&options, game)
            }
            DeclareBlockers => {
                // No legal blockers
                find_if_one::<FinishDeclaringBlockersAction>(&options, game)
            }
            DeclareAttackerAssignmentOrder => Option::None,
            PutTriggeredAbilityOnStack => {
                // Only one
                find_if_one::<PutTriggeredAbilityAction>(&options, game)
            }
            _ => panic!("Requested action from incorrect type"),
        }
        .cloned()
    }

    pub fn request_range(
        &mut self,
        _game: &Game,
        expected: ExpectedUserInput,
        _min: i32,
        _max: i32,
    ) -> Option<i32> {
        match expected {
            DeclareAttackerAssignment(_, _) => Option::None,
            _ => panic!("Requested range from non-range type"),
        }
    }
}
