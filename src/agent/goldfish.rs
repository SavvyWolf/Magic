use super::Agent;
use actions::ActionEntry;
use actions::ExpectedUserInput;
use actions::ExpectedUserInput::*;
use game::Game;
use rules::*;

pub struct GoldfishAgent {}

fn find<T: 'static>(options: &[ActionEntry]) -> &ActionEntry {
    for o in options {
        if o.action.as_any().is::<T>() {
            return o;
        }
    }
    panic!("Couldn't pass");
}

impl Agent for GoldfishAgent {
    fn request_action_choice(
        &mut self,
        _game: &Game,
        expected: ExpectedUserInput,
        options: Vec<ActionEntry>,
    ) -> ActionEntry {
        match expected {
            PriorityChoice => find::<PassPriorityAction>(&options),
            ActivateManaAbilities => find::<FinishActivatingManaAbilitiesAction>(&options),
            MakeManaPayment => &options[0],
            DeclareAttackers => find::<FinishDeclaringAttackersAction>(&options),
            DeclareBlockers => find::<FinishDeclaringBlockersAction>(&options),
            DeclareAttackerAssignmentOrder => &options[0],
            PutTriggeredAbilityOnStack => &options[0],
            _ => panic!("Requested action from incorrect type"),
        }
        .clone()
    }

    fn request_range(
        &mut self,
        _game: &Game,
        expected: ExpectedUserInput,
        min: i32,
        _max: i32,
    ) -> i32 {
        match expected {
            DeclareAttackerAssignment(_, _) => min,
            _ => panic!("Requested range from non-range type"),
        }
    }
}
