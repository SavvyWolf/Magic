use actions::ActionEntry;
use actions::ExpectedUserInput;
use game::Game;
use gamestate::PlayerId;

use std::collections::HashMap;

pub trait Agent {
    fn tell_game_action_performed(&mut self, _game: &Game, _action: ActionEntry) {}

    fn request_action_choice(
        &mut self,
        game: &Game,
        expected: ExpectedUserInput,
        options: Vec<ActionEntry>,
    ) -> ActionEntry;
    fn request_range(
        &mut self,
        game: &Game,
        expected: ExpectedUserInput,
        min: i32,
        max: i32,
    ) -> i32;
}

pub type AgentMap<'a> = HashMap<PlayerId, Box<dyn Agent>>;
