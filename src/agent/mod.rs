mod auto_passer;
mod base_agent;
mod goldfish;

pub use self::auto_passer::AutoPasser;
pub use self::base_agent::Agent;
pub use self::base_agent::AgentMap;
pub use self::goldfish::GoldfishAgent;
