use cards::mana::Colour;
use cards::mana::Colour::*;
use cards::mana::Cost;
use cards::mana::CostEntry;
use cards::mana::CostEntry::*;
use parse::ParseError;

extern crate json;

pub fn parse_cost(cost: &str) -> Result<Cost, ParseError> {
    let mut out = vec![];
    let mut start = 0;
    for (i, c) in cost.chars().enumerate() {
        // Start parsing entry
        if c == '{' {
            start = i + 1;
        }
        if c == '}' {
            out.push(parse_single_cost(&cost[start..i])?);
        }
    }
    Ok(out)
}

pub fn dump_cost(cost: &[CostEntry]) -> String {
    let mut out = String::new();

    for c in cost {
        out += &format!("{}", c);
    }

    out
}

pub fn parse_single_cost(cost: &str) -> Result<CostEntry, ParseError> {
    match cost {
        "W" => Ok(Basic(White)),
        "U" => Ok(Basic(Blue)),
        "B" => Ok(Basic(Black)),
        "R" => Ok(Basic(Red)),
        "G" => Ok(Basic(Green)),
        "C" => Ok(Basic(Colorless)),
        "X" => Ok(X),
        "S" => Ok(Snow),
        _ => {
            if cost.contains('/') {
                // Hybrid/Phyrexian mana
                let halves: Vec<&str> = cost.split('/').collect();
                if halves[0] == "2" {
                    Ok(GenericHybrid(parse_colour(halves[1])?))
                } else if halves[1] == "P" {
                    Ok(Phyrexian(parse_colour(halves[0])?))
                } else {
                    Ok(Hybrid(parse_colour(halves[0])?, parse_colour(halves[1])?))
                }
            } else {
                match cost.parse::<usize>() {
                    Ok(x) => Ok(Generic(x)),
                    _ => Err(ParseError::InvalidEnum),
                }
            }
        }
    }
}

pub fn parse_colour(colour: &str) -> Result<Colour, ParseError> {
    match colour {
        "W" => Ok(White),
        "U" => Ok(Blue),
        "B" => Ok(Black),
        "R" => Ok(Red),
        "G" => Ok(Green),
        _ => panic!("Unknown colour {}!", colour),
    }
}
