use json::Error as JsonError;
use std::fmt;
use std::io::Error as IoError;
use std::num::ParseIntError;
use strum::ParseError as StrumError;

#[derive(Debug)]
pub enum ParseError {
    TypeNotValid,
    TypeNotFound(String),
    JsonError(JsonError),
    WrongType(&'static str),
    InvalidValue(&'static str),
    InvalidEnum,
    IoError(IoError),
    WrongVersion,
    ParseIntError(ParseIntError),
}

impl From<JsonError> for ParseError {
    fn from(err: JsonError) -> Self {
        ParseError::JsonError(err)
    }
}
impl From<StrumError> for ParseError {
    fn from(_err: StrumError) -> Self {
        ParseError::InvalidEnum
    }
}
impl From<IoError> for ParseError {
    fn from(err: IoError) -> Self {
        ParseError::IoError(err)
    }
}
impl From<ParseIntError> for ParseError {
    fn from(err: ParseIntError) -> Self {
        ParseError::ParseIntError(err)
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::TypeNotValid => write!(f, "Type not valid"),
            Self::TypeNotFound(t) => write!(f, "Type not found: {}", t),
            Self::JsonError(j) => write!(f, "JsonError: {}", j),
            Self::WrongType(t) => write!(f, "WrongType: {}", t),
            Self::InvalidValue(v) => write!(f, "InvalidValue: {}", v),
            Self::InvalidEnum => write!(f, "InvalidEnum"),
            Self::IoError(e) => write!(f, "IoError: {}", e),
            Self::WrongVersion => write!(f, "WrongVersion"),
            Self::ParseIntError(e) => write!(f, "ParseIntError: {}", e),
        }
    }
}
