use abilities::StaticEffect;
use actions::Action;
use json::{object, JsonValue};

use super::ParseError;

pub trait TypedJson {
    fn to_json(&self) -> JsonValue {
        let mut obj = self.get_json();
        obj["_type"] = JsonValue::String(self.type_name().to_owned());
        obj
    }

    fn get_json(&self) -> JsonValue {
        object! {}
    }

    fn type_name(&self) -> &'static str;
}

pub struct TypedRegistry<T: ?Sized + TypedJson> {
    templates: Vec<Box<T>>,
}

impl<T: ?Sized + TypedJson> TypedRegistry<T> {
    pub fn push(&mut self, obj: Box<T>) {
        self.templates.push(obj);
    }
    fn find(&self, obj: &JsonValue) -> Result<&T, ParseError> {
        let t = obj["_type"].as_str();
        match t {
            Some(t) => {
                for temp in &self.templates {
                    if temp.type_name() == t {
                        return Ok(temp);
                    }
                }
                Err(ParseError::TypeNotFound(t.to_string()))
            }
            None => Err(ParseError::TypeNotValid),
        }
    }
}

impl<T: ?Sized + TypedJson> Default for TypedRegistry<T> {
    fn default() -> Self {
        Self { templates: vec![] }
    }
}

impl TypedRegistry<dyn Action> {
    pub fn load_action(&self, obj: &JsonValue) -> Result<Box<dyn Action>, ParseError> {
        let action = self.find(obj)?;
        action.load_json(obj)
    }
}

impl TypedRegistry<dyn StaticEffect> {
    pub fn load_staticeffect(&self, obj: &JsonValue) -> Result<Box<dyn StaticEffect>, ParseError> {
        let se = self.find(obj)?;
        se.load_json(obj)
    }
}
