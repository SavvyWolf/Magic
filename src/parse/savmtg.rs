use cards::Card;

use super::common::*;
use super::ParseError;

use std::fs;

extern crate json;
use self::json::{parse, JsonValue};

use abilities::{ActivatedAbility, StaticEffect, TriggeredAbility};
use actions::Action;
use cards::properties::{CardSubtype, CardSupertype, CardType};
use parse::TypedRegistry;
use rules::{get_actions_registry, get_static_effects_registry};

use std::str::FromStr;

pub const VERSION: &str = "0.1";

pub fn dump(file: &str, cards: &[Card]) -> Result<(), ParseError> {
    fs::write(file, dump_string(cards)?)?;
    Ok(())
}

pub fn dump_string(cards: &[Card]) -> Result<String, ParseError> {
    let mut data = json::JsonValue::new_object();

    data["version"] = VERSION.into();
    data["cards"] = json::JsonValue::new_array();

    for c in cards {
        data["cards"].push(dump_card(c)?)?;
    }

    //println!("{}", data.pretty(4));
    Ok(data.dump())
}

pub fn load(file: &str) -> Result<Vec<Card>, ParseError> {
    let contents = fs::read_to_string(file)?;
    load_str(&contents)
}

pub fn load_str(string: &str) -> Result<Vec<Card>, ParseError> {
    let contents = parse(string)?;
    let mut out = vec![];

    let areg = get_actions_registry();
    let sereg = get_static_effects_registry();

    if contents["version"]
        .as_str()
        .ok_or(ParseError::WrongVersion)?
        != VERSION
    {
        return Err(ParseError::WrongVersion);
    }

    for value in contents["cards"].members() {
        match load_card(value, &areg, &sereg) {
            Ok(card) => out.push(card),
            Err(err) => eprintln!(
                "WARNING: Could not parse card {} ({})",
                load_card_name(value),
                err
            ),
        }
    }

    Ok(out)
}

fn dump_card(card: &Card) -> Result<JsonValue, ParseError> {
    let mut obj = json::JsonValue::new_object();

    obj["name"] = card.name.clone().into();
    obj["rules_as_written"] = card.rules_as_written.clone().into();
    obj["power"] = card.power.into();
    obj["toughness"] = card.toughness.into();
    obj["loyalty"] = card.loyalty.into();

    // Types
    obj["types"] = json::JsonValue::new_array();
    for t in &card.types {
        obj["types"].push(format!("{}", t))?;
    }
    obj["subtypes"] = json::JsonValue::new_array();
    for t in &card.subtypes {
        obj["subtypes"].push(format!("{}", t))?;
    }
    obj["supertypes"] = json::JsonValue::new_array();
    for t in &card.supertypes {
        obj["supertypes"].push(format!("{}", t))?;
    }

    // Colours
    obj["colours"] = json::JsonValue::new_array();
    for c in &card.colours {
        obj["colours"].push(format!("{}", c))?;
    }

    // Cast Choices
    obj["cast_choices"] = json::JsonValue::new_array();
    for cc in &card.cast_choices {
        obj["cast_choices"].push(cc.to_json())?;
    }

    // Resolution Effects
    obj["resolution_effects"] = json::JsonValue::new_array();
    for re in &card.resolution_effects {
        obj["resolution_effects"].push(re.to_json())?;
    }

    // Static Effects
    obj["static_effects"] = json::JsonValue::new_array();
    for se in &card.static_effects {
        obj["static_effects"].push(se.to_json())?;
    }

    // Activated Abilities
    obj["activated_abilities"] = json::JsonValue::new_array();
    for aa in &card.activated_abilities {
        obj["activated_abilities"].push(aa.to_json())?;
    }

    // Triggered Abilities
    obj["triggered_abilities"] = json::JsonValue::new_array();
    for ta in &card.triggered_abilities {
        obj["triggered_abilities"].push(ta.to_json())?;
    }

    obj["cost"] = dump_cost(&card.cost).into();

    Ok(obj)
}

pub fn card_to_json(card: &Card) -> Result<JsonValue, ParseError> {
    dump_card(card)
}

fn load_card_name(obj: &JsonValue) -> String {
    obj["name"].to_string()
}

fn load_card(
    obj: &JsonValue,
    areg: &TypedRegistry<dyn Action>,
    sereg: &TypedRegistry<dyn StaticEffect>,
) -> Result<Card, ParseError> {
    let mut out: Card = Default::default();
    out.name = obj["name"].to_string();

    out.power = obj["power"]
        .as_i32()
        .ok_or(ParseError::WrongType("power"))?;
    out.toughness = obj["toughness"]
        .as_i32()
        .ok_or(ParseError::WrongType("toughness"))?;
    out.loyalty = obj["loyalty"]
        .as_i32()
        .ok_or(ParseError::WrongType("loyalty"))?;
    out.rules_as_written = obj["rules_as_written"].to_string();

    out.cost = parse_cost(obj["cost"].as_str().ok_or(ParseError::WrongType("cost"))?)?;

    // Types
    for tstr in obj["types"].members() {
        out.types.push(CardType::from_str(
            tstr.as_str().ok_or(ParseError::WrongType("types"))?,
        )?);
    }
    for tstr in obj["subtypes"].members() {
        out.subtypes.push(CardSubtype::from_str(
            tstr.as_str().ok_or(ParseError::WrongType("subtypes"))?,
        )?);
    }
    for tstr in obj["supertypes"].members() {
        out.supertypes.push(CardSupertype::from_str(
            tstr.as_str().ok_or(ParseError::WrongType("supertypes"))?,
        )?);
    }

    // Colours
    for tstr in obj["colours"].members() {
        out.colours.push(parse_colour(
            tstr.as_str().ok_or(ParseError::WrongType("colours"))?,
        )?);
    }

    // Cast Choices
    for cc in obj["cast_choices"].members() {
        out.cast_choices.push(areg.load_action(cc)?);
    }

    // Resolution Effects
    for re in obj["resolution_effects"].members() {
        out.resolution_effects.push(areg.load_action(re)?);
    }

    // Static Effects
    for re in obj["static_effects"].members() {
        out.static_effects.push(sereg.load_staticeffect(re)?);
    }

    // Static Effects
    for re in obj["activated_abilities"].members() {
        out.activated_abilities
            .push(ActivatedAbility::from_json(re)?);
    }

    // Triggered Abilities
    for re in obj["triggered_abilities"].members() {
        out.triggered_abilities
            .push(TriggeredAbility::from_json(re)?);
    }

    Ok(out)
}

pub fn json_to_card(obj: &JsonValue) -> Result<Card, ParseError> {
    let areg = get_actions_registry();
    let sereg = get_static_effects_registry();

    load_card(obj, &areg, &sereg)
}
