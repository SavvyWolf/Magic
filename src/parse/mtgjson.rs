use cards::mana::Colour;
use cards::properties::{CardSubtype, CardSupertype, CardType};
use cards::Card;

use super::common::*;
use super::ParseError;

use std::fs;

extern crate json;
use self::json::{parse, JsonValue};
use std::path::PathBuf;
use std::str::FromStr;

pub fn load(file: PathBuf) -> Result<Vec<Card>, ParseError> {
    let contents = fs::read_to_string(file).unwrap();
    let contents = parse(&contents)?;
    let mut out = vec![];

    for (_, value) in contents.entries() {
        out.append(&mut parse_set(value)?);
    }

    Ok(out)
}

pub fn load_set(file: PathBuf, set: &str) -> Result<Vec<Card>, ParseError> {
    let contents = fs::read_to_string(file).unwrap();
    let contents = parse(&contents)?;
    let mut out = vec![];

    for (s, value) in contents.entries() {
        if s == set {
            out.append(&mut parse_set(value)?);
        }
    }

    Ok(out)
}

fn parse_set(set: &JsonValue) -> Result<Vec<Card>, ParseError> {
    let mut out = vec![];
    // These sets have wierd cards in them
    if set["code"].as_str().unwrap() == "PCEL" {
        return Ok(out);
    }
    if set["code"].as_str().unwrap().starts_with("THP") {
        return Ok(out);
    }

    for card in set["cards"].members() {
        // Ignore silver bordered cards (for now?)
        if card["borderColor"].as_str().unwrap() == "silver" {
            continue;
        }

        let mut run = true;
        for t in card["types"].members() {
            // Advert card? Ignore
            if t.as_str().unwrap() == "Card" {
                run = false;
            }
        }

        if run {
            match parse_card(card) {
                Ok(c) => out.push(c),
                Err(e) => eprintln!(
                    "Could not load card {} ({})",
                    card["name"].as_str().unwrap(),
                    e
                ),
            }
        }
    }
    Ok(out)
}

fn parse_card(element: &JsonValue) -> Result<Card, ParseError> {
    let mut out: Card = Default::default();
    if element.has_key("name") {
        out.name = element["name"].to_string();
    }
    println!("Parsing '{}'...", out.name);

    if element.has_key("manaCost") {
        out.cost = parse_cost(
            element["manaCost"]
                .as_str()
                .ok_or(ParseError::WrongType("types"))?,
        )?;
    }

    if element.has_key("types") {
        let mut t: Vec<CardType> = vec![];
        for tstr in element["types"].members() {
            t.push(CardType::from_str(
                tstr.as_str().ok_or(ParseError::WrongType("types"))?,
            )?);
        }
        out.types = t;
    }

    if element.has_key("subtypes") {
        let mut t: Vec<CardSubtype> = vec![];
        for tstr in element["subtypes"].members() {
            t.push(CardSubtype::from_str(
                tstr.as_str().ok_or(ParseError::WrongType("subtypes"))?,
            )?);
        }
        out.subtypes = t;
    }

    if element.has_key("supertypes") {
        let mut t: Vec<CardSupertype> = vec![];
        for tstr in element["supertypes"].members() {
            t.push(CardSupertype::from_str(
                tstr.as_str().ok_or(ParseError::WrongType("supertypes"))?,
            )?);
        }
        out.supertypes = t;
    }

    if element.has_key("colors") {
        let mut t: Vec<Colour> = vec![];
        for tstr in element["colors"].members() {
            t.push(parse_colour(
                tstr.as_str().ok_or(ParseError::WrongType("colours"))?,
            )?);
        }
        out.colours = t;
    }

    if element.has_key("power") {
        out.power = parse_pt(&element["power"]);
    }
    if element.has_key("toughness") {
        out.toughness = parse_pt(&element["toughness"]);
    }
    if element.has_key("loyalty") {
        out.loyalty = parse_pt(&element["loyalty"]);
    }
    if element.has_key("text") {
        out.rules_as_written = element["text"].to_string();
    }

    Ok(out)
}

fn parse_pt(val: &JsonValue) -> i32 {
    let s = val.to_string();
    match i32::from_str(&s) {
        Ok(i) => i,
        Err(_) => 0,
    }
}
