use abilities;
use abilities::{layers, TriggeredAbility};
use actions::Action;
use actions::ActionEntry;
use actions::ExpectedUserInput;
use actions::{ActionStack, ActionStackEntry, UserInputKind};
use agent::AgentMap;
use cards::add_rules_card;
use cards::mana::Cost;
use cards::mana::Mana;
use cards::Oracle;
use filter::Filter;
use gamestate::phases::GameState;
use gamestate::phases::Process;
use gamestate::phases::Step;
use gamestate::AttackTarget;
use gamestate::ObjectType;
use gamestate::Player;
use gamestate::PlayerId;
use gamestate::RcGameObject;
use gamestate::TeamId;
use gamestate::Zone;
use gamestate::ZoneType;
use rules::ReplacementEffect;
use rules::*;
use utils::get_attackers;

use utils::apnap;

use std::path::PathBuf;

/// Stores all game zones, game objects and actions
///
/// This struct is in charge of all aspects of running the game, such as taking turns and performing actions. When
/// rendering the game state, fields of this struct may be used.
///
/// TODO: Describe the new interface
#[derive(Clone)]
pub struct Game {
    /// An oracle providing this `Game`'s cards
    pub oracle: Oracle,
    /// Each player's status
    pub players: Vec<Player>,

    /// The shared battlefield zone
    pub battlefield: Zone,
    /// Each player's hand zone
    pub hands: Vec<Zone>,
    /// Each player's library zone
    pub libraries: Vec<Zone>,
    /// Each player's graveyard
    pub graveyards: Vec<Zone>,
    /// A special zone which should not be presented to the user
    ///
    /// This holds several GameObjects which are required for the game engine to run.
    pub rules: Zone,
    /// The shared stack zone
    pub stack: Zone,
    /// The shared zone where triggered abilities wait
    pub triggered_waiting: Zone,
    /// Exile
    pub exile: Zone,

    /// The `Game`'s action stack
    ///
    /// Every action the game engine performs (such as drawing a card or paying mana) happens on a stack. A spell, for
    /// example, will push all its effects onto the stack, then each one will be popped and performed.
    action_stack: ActionStack,

    /// The current game state
    ///
    /// This stores the current phase, and the current player with priority, among other things
    pub state: GameState,
    /// The number of combat phases left this combat
    pub combat_phases_left: usize,

    /// The cost currently being paid by the cost mechanism
    ///
    /// When a spell is cast, it writes the spell cost into here. Then as mana payments are made, they are removed from
    /// this list until it is empty.
    pub current_cost: Cost,
}

impl Game {
    /// Create a new `Game` instance
    ///
    /// The number of players in this game, and the oracle used for providing the `Game`'s cards are provided.
    pub fn new(players: Vec<TeamId>, card_dir: PathBuf) -> Game {
        let mut oracle = Oracle::new(card_dir);
        oracle.load_included();
        Game {
            oracle,

            battlefield: Zone::shared(ZoneType::Battlefield, ObjectType::Permanent),
            rules: Zone::shared(ZoneType::Rules, ObjectType::Card),
            stack: Zone::shared(ZoneType::Stack, ObjectType::Stack),
            exile: Zone::shared(ZoneType::Exile, ObjectType::Card),
            triggered_waiting: Zone::shared(ZoneType::TriggeredWaiting, ObjectType::Stack),

            players: players
                .iter()
                .enumerate()
                .map(|(id, team)| Player::new(id, *team, 20))
                .collect(),
            hands: players
                .iter()
                .enumerate()
                .map(|(x, _)| Zone::owned(ZoneType::Hand(x), ObjectType::Card, x))
                .collect(),
            libraries: players
                .iter()
                .enumerate()
                .map(|(x, _)| Zone::owned(ZoneType::Library(x), ObjectType::Card, x))
                .collect(),
            graveyards: players
                .iter()
                .enumerate()
                .map(|(x, _)| Zone::owned(ZoneType::Graveyard(x), ObjectType::Card, x))
                .collect(),

            state: GameState::new(),
            combat_phases_left: 0,
            action_stack: ActionStack::new(),
            current_cost: vec![],
        }
    }

    pub fn new_no_teams(players: usize, cards_dir: PathBuf) -> Game {
        Game::new((0..players).collect(), cards_dir)
    }

    pub fn run<'a>(&mut self, agents: &mut AgentMap<'a>) {
        self.begin();
        loop {
            let (player, kind) = self.get_input_kind();
            let eui = self.get_expected_user_input().1;
            let agent = agents.get_mut(&player).unwrap();
            match kind {
                UserInputKind::None => (),
                UserInputKind::ActionChoice => {
                    let (_, actions) = self.get_game_actions();
                    if !actions.is_empty() {
                        let choice = agent.request_action_choice(self, eui, actions);
                        self.use_game_action(choice);
                    }
                }
                UserInputKind::Range(a, b) => {
                    let val = agent.request_range(self, eui, a, b);
                    self.use_range(val);
                }
            }

            agent.tell_game_action_performed(self, self.peek_action().as_action_entry());
            self.perform_top_action();
        }
    }

    /// Given a `ZoneType`, returns a reference to the Zone having that type
    pub fn find_zone(&self, zone: ZoneType) -> &Zone {
        match zone {
            ZoneType::Hand(i) => &self.hands[i],
            ZoneType::Library(i) => &self.libraries[i],
            ZoneType::Graveyard(i) => &self.graveyards[i],
            ZoneType::Battlefield => &self.battlefield,
            ZoneType::Rules => &self.rules,
            ZoneType::Stack => &self.stack,
            ZoneType::Exile => &self.exile,
            ZoneType::TriggeredWaiting => &self.triggered_waiting,
        }
    }
    /// Given a `ZoneType`, returns a mutable reference to the Zone having that type
    pub fn find_zone_mut(&mut self, zone: ZoneType) -> &mut Zone {
        match zone {
            ZoneType::Hand(i) => &mut self.hands[i],
            ZoneType::Library(i) => &mut self.libraries[i],
            ZoneType::Graveyard(i) => &mut self.graveyards[i],
            ZoneType::Battlefield => &mut self.battlefield,
            ZoneType::Rules => &mut self.rules,
            ZoneType::Stack => &mut self.stack,
            ZoneType::Exile => &mut self.exile,
            ZoneType::TriggeredWaiting => &mut self.triggered_waiting,
        }
    }

    /// Returns a list of actions that the user may take from this game state
    ///
    /// It is expected that one of these entries is chosen, and then given to `use_game_action`.
    ///
    /// This list may be empty, in which case no user action is expected, and `perform_top_action` may be called to
    /// advance the game state.
    pub fn get_game_actions(&self) -> (isize, Vec<ActionEntry>) {
        let entry = self.action_stack.top();
        let (player, expected) = entry
            .action
            .get_expected_user_input(self, entry.object.as_ref());
        if expected.get_input_kind() != UserInputKind::ActionChoice {
            panic!("Attempted to call get_game_actions for non-ActionChoice");
        }
        self.get_game_actions_for(expected, player)
    }

    pub fn get_input_kind(&self) -> (PlayerId, UserInputKind) {
        let entry = self.action_stack.top();
        let (player, expected) = entry
            .action
            .get_expected_user_input(self, entry.object.as_ref());
        (player, expected.get_input_kind())
    }

    pub fn get_expected_user_input(&self) -> (PlayerId, ExpectedUserInput) {
        let entry = self.action_stack.top();
        entry
            .action
            .get_expected_user_input(self, entry.object.as_ref())
    }

    /// Returns a list of actions that the provided player may take
    ///
    /// This requires an explicit ExpectedUserInput and PlayerId, rather than reading it from the top action.
    pub fn get_game_actions_for(
        &self,
        input: ExpectedUserInput,
        priority: PlayerId,
    ) -> (isize, Vec<ActionEntry>) {
        match input {
            ExpectedUserInput::None => (-1, vec![]),
            _ => abilities::get_actions(&self, self.get_static_effects(), input, priority),
        }
    }

    /// Return a structure indicating the available mana for a player
    ///
    /// This is a three nested deep lists, with the following layers:
    /// * Each action that could produce mana
    /// * Each option of mana on that action
    /// * Each mana that would be produced using that option
    ///
    /// TODO: This should be per object, not per action
    pub fn get_available_mana(&self, player: PlayerId) -> Vec<Vec<Vec<Mana>>> {
        let mut out = vec![];
        for entry in self
            .get_game_actions_for(ExpectedUserInput::ActivateManaAbilities, player)
            .1
        {
            out.push(entry.action.could_produce_mana(self, entry.object.as_ref()));
        }
        out
    }

    /// Given an ActionEntry, pushes it on top of the action stack
    ///
    /// After providing the action, you should call `perform_top_action` to execute it.
    pub fn use_game_action(&mut self, action: ActionEntry) {
        self.perform_top_action();
        self.action_stack
            .push(action.object, action.action.box_clone());
    }

    pub fn use_range(&mut self, value: i32) {
        self.action_stack.top_mut().action.use_value(value);
    }

    /// Update the gamestate with all currently active static effects
    ///
    /// This is called automatically, and should not be called by users of this library unless you are manipulating
    /// objects directly.
    pub fn calculate(&mut self) {
        self.reset();
        abilities::update(self, self.get_static_effects());
    }

    fn reset(&mut self) {
        for z in self.all_zones_mut() {
            z.reset();
        }
    }

    fn get_static_effects(&self) -> layers::LayerMap {
        let mut out: layers::LayerMap = Default::default();
        let mut handle = |zone: &Zone| {
            for obj in zone.iter() {
                for effect in &obj.borrow().static_effects {
                    out[effect.layer()].push((obj.clone(), effect.clone()));
                }
            }
        };
        for z in &self.all_zones() {
            handle(z);
        }
        out
    }

    fn get_replacement_effects(&self) -> Vec<(Option<RcGameObject>, usize, ReplacementEffect)> {
        let mut objs = vec![];
        for z in &self.all_zones() {
            for go in z.iter() {
                for (offset, effect) in go.borrow().static_effects.iter().enumerate() {
                    if let Some(re) = effect.as_replacement_effect() {
                        objs.push((Some(go.clone()), offset, re.clone()));
                    }
                }
            }
        }
        objs
    }

    fn get_triggered_abilities(&self) -> Vec<(RcGameObject, usize, TriggeredAbility)> {
        let mut objs = vec![];
        for z in &self.all_zones() {
            for go in z.iter() {
                for (offset, ability) in go.borrow().triggered_abilities.iter().enumerate() {
                    objs.push((go.clone(), offset, ability.clone()));
                }
            }
        }
        objs
    }

    /// Advance the underlying game state
    ///
    /// This handles the transitions between different stages, turn based actions and priority
    ///
    /// This should not be called unless you know what you are doing.
    pub fn advance(&mut self) {
        self.action_stack
            .push(None, Box::new(AdvanceGameStateAction {}));

        let old_step = self.state.step;
        self.state = self.state.clone().advance(self);

        if old_step != self.state.step && self.state.step == Step::BeginningOfCombat {
            // Consume a combat "credit"
            self.combat_phases_left -= 1;
        }

        match self.state.process {
            Process::ResolvingTopOfStack => {
                self.push_action(
                    Some(self.stack.top().clone()),
                    Box::new(ResolveStackObject::new()),
                );
            }

            Process::HavePriority => {
                self.action_stack
                    .push(None, Box::new(HavePriorityAction::new()));
                self.action_stack.push(None, Box::new(SBAAction::new()));
                for p in apnap(self.state.priority, self.num_players()) {
                    self.action_stack
                        .push(None, Box::new(MaybePutTriggeredAbilityAction::new(p)));
                }
            }

            Process::PassToNextStep => {
                // Pass
            }
        }

        if old_step != self.state.step {
            // Step change, empty mana pools
            for p in 0..self.num_players() {
                self.action_stack
                    .push(None, Box::new(EmptyManaPoolsAction::new(p)));
            }

            // Perform any TBAs on step change
            match self.state.step {
                Step::Draw => {
                    // TBA draw a card
                    let ap = self.state.active_player;
                    self.push_action(
                        None,
                        Box::new(DrawCardAction::new(vec![Filter::IsAbsolutePlayer(ap)])),
                    );
                }

                Step::DeclareAttackers => {
                    // TBA declare attackers
                    let ap = self.state.active_player;
                    self.push_action(None, Box::new(DeclareAttackersAction::new(ap)));
                }

                Step::DeclareBlockers => {
                    // TBA declare blockers

                    // Declare blockers damage assignment order
                    //TODO

                    // Declare attackers damage assignment order
                    self.push_action(
                        None,
                        Box::new(DeclareAttackerDamageOrderAction::new(self.state.priority)),
                    );

                    // Declare blockers
                    for offset in 0..(self.num_players() as i32 - 1) {
                        let p = self.state.active_player as i32 - offset - 1;
                        let p = if p < 0 {
                            p + self.num_players() as i32
                        } else {
                            p
                        };
                        self.push_action(None, Box::new(DeclareBlockersAction::new(p as usize)));
                    }
                }

                Step::CombatDamage => {
                    // TBA perform damage
                    self.push_action(None, Box::new(PerformAllDamageAction::new()));

                    // Declare blockers damage assignment
                    //TODO

                    // Declare attackers damage assignment
                    for a in get_attackers(self) {
                        let power = a.borrow().power;
                        a.borrow_mut().attack_damage_remaining = power;
                        let order = &a.borrow().attack_assignment_order;
                        for i in 0..order.len() {
                            self.push_action(
                                Some(a.clone()),
                                Box::new(DeclareDamageAction::new(i == 0, order.len() - 1 - i)),
                            );
                        }
                    }
                }

                Step::Main => {
                    // Creatures no longer are attacking or blocking
                    for o in self.battlefield.iter() {
                        let mut obj = o.borrow_mut();
                        obj.attacking = AttackTarget::None;
                        obj.blocking = None;
                    }
                }

                Step::Untap => {
                    // Untap step, start a new turn and untap everything
                    // Switch the active player
                    if old_step != Step::NewGame {
                        self.state.active_player += 1;
                        self.state.active_player %= self.num_players();
                        self.state.priority = self.state.active_player;
                    }
                    let ap = self.state.active_player;

                    // Reset combat and land drops
                    self.combat_phases_left = 1;
                    for p in self.players.iter_mut() {
                        if p.id == self.state.active_player {
                            p.land_drops_left = 1
                        } else {
                            p.land_drops_left = 0
                        }
                    }

                    // Creatures stop being summoning sick
                    for o in self.battlefield.iter() {
                        if o.borrow().controller == ap {
                            o.borrow_mut().summoning_sick = false;
                        }
                    }

                    // Do the actual untapping
                    self.action_stack
                        .push(None, Box::new(UntapStepAction::new(ap)));
                }

                _ => (),
            }
        }
    }

    /// Returns the number of players
    ///
    /// Players leaving the game doesn't cause this number to decrease.
    pub fn num_players(&self) -> usize {
        2
    }

    /// Are there any attacking creatures?
    pub fn any_attackers(&self) -> bool {
        for o in self.battlefield.iter() {
            let obj = o.borrow();
            if obj.attacking != AttackTarget::None {
                return true;
            }
        }
        false
    }

    /// Are there any items in the stack zone?
    pub fn stack_is_empty(&self) -> bool {
        self.stack.is_empty()
    }

    /// Return the current player with priority
    pub fn get_priority(&self) -> PlayerId {
        self.state.priority
    }

    /// Return the current active player
    pub fn get_ap(&self) -> PlayerId {
        self.state.active_player
    }

    /// Can the provided player cast sorceries (or things that happen at sorcery speed)?
    pub fn can_cast_sorceries(&self, player: PlayerId) -> bool {
        self.state.active_player == self.state.priority
            && self.can_cast_instants(player)
            && self.stack_is_empty()
            && self.state.step == Step::Main
    }

    /// Can the provided player play things at instant speed?
    pub fn can_cast_instants(&self, player: PlayerId) -> bool {
        self.state.priority == player
    }

    /// The provided player draws a number of cards from the top of their library
    pub fn draw(&mut self, player: PlayerId, count: usize) {
        for _ in 0..count {
            self.hands[player].transfer_from_by_index(&mut self.libraries[player], 0);
        }
    }

    /// Perform initialization of the game state
    ///
    /// This should be called before requesting or submitting actions.
    ///
    /// TODO: This should not draw the initial hand
    pub fn begin(&mut self) {
        add_rules_card(self);

        for i in 0..self.num_players() {
            self.draw(i, 7);
        }

        self.action_stack
            .push(None, Box::new(AdvanceGameStateAction {}));
    }

    /// Pop the top action from the stack and execute it
    pub fn perform_top_action(&mut self) {
        loop {
            let mut entry = self.action_stack.pop();
            let action = entry.action.clone();
            let obj = entry.object.clone();

            let hook = action.get_hooks(self, obj.as_ref());
            if !hook.is_empty() {
                // Handle replacement effects
                let repl = self.get_replacement_effects();
                // TODO: Handle multiple hooks
                let repl: Vec<_> = repl
                    .iter()
                    .filter_map(|(repl_obj, i, r)| {
                        if r.watch_type != hook[0].hook_type {
                            return None;
                        }
                        if entry.is_modified_by(repl_obj.as_ref().unwrap().borrow().uid, *i) {
                            return None;
                        }
                        let data = r.does_match(
                            self,
                            repl_obj.as_ref(),
                            hook[0].args.clone(),
                            hook[0].data.clone(),
                        );
                        if let Some(data) = data {
                            Some((data, repl_obj, i, r))
                        } else {
                            None
                        }
                    })
                    .collect();

                if !repl.is_empty() {
                    // Apply replacement effects and continue
                    println!(
                        "Applying replacement effects for: {}",
                        action.description(self, obj.as_ref(), false)
                    );

                    assert_eq!(repl.len(), 1, "Can't handle multiple REs yet!");
                    let (data, re_object, offset, re) = &repl[0];
                    entry.insert_modified_by(re_object.as_ref().unwrap().borrow().uid, **offset);

                    if re.keep_original {
                        self.push_action_entry(entry.clone());
                    }
                    for replace_action in &re.replace_with {
                        let mut act = replace_action.box_clone();
                        act.apply_template(data);

                        let mut new_entry = ActionStackEntry::new((*re_object).clone(), act);
                        new_entry.copy_modified_by(&entry);
                        self.push_action_entry(new_entry);
                    }
                    continue;
                }
            }

            println!(
                "Performing top action: {}",
                action.description(self, obj.as_ref(), false)
            );

            // Now look for triggered abilities
            let mut tas_pending = vec![];
            if !hook.is_empty() {
                let tas = self.get_triggered_abilities();

                tas_pending.extend(tas.iter().filter_map(|(a_obj, _i, a)| {
                    if a.watch_type != hook[0].hook_type {
                        return None;
                    }
                    let data = a.does_match(
                        self,
                        Some(a_obj),
                        hook[0].args.clone(),
                        hook[0].data.clone(),
                    );
                    if let Some(_data) = data {
                        Some((a_obj.clone(), a.clone()))
                    } else {
                        None
                    }
                }));
            }

            action.perform(self, obj);
            self.calculate();

            // And put the actions to perform the TAs
            for (a_obj, a) in tas_pending.drain(..) {
                self.push_action(Some(a_obj), Box::new(TriggerTriggeredAbilityAction::new(a)));
            }
            break;
        }
    }

    pub fn push_action(&mut self, object: Option<RcGameObject>, action: Box<dyn Action>) {
        self.action_stack.push(object, action);
    }

    pub fn push_action_entry(&mut self, entry: ActionStackEntry) {
        self.action_stack.push_entry(entry);
    }

    pub fn pop_action(&mut self) -> ActionStackEntry {
        self.action_stack.pop()
    }

    pub fn peek_action(&self) -> &ActionStackEntry {
        self.action_stack.top()
    }

    /// Return a vector of all zones
    pub fn all_zones(&self) -> Vec<&Zone> {
        let mut out = vec![];
        out.push(&self.battlefield);
        out.push(&self.rules);
        out.push(&self.exile);
        out.push(&self.triggered_waiting);
        out.push(&self.stack);
        for p in 0..self.num_players() {
            out.push(&self.hands[p]);
            out.push(&self.libraries[p]);
            out.push(&self.graveyards[p]);
        }
        out
    }

    /// Return a vector of all zones
    pub fn all_zones_mut(&mut self) -> Vec<&mut Zone> {
        let mut out = vec![];
        out.push(&mut self.battlefield);
        out.push(&mut self.rules);
        out.push(&mut self.exile);
        out.push(&mut self.triggered_waiting);
        out.push(&mut self.stack);
        fn owned<'a>(out: &mut Vec<&'a mut Zone>, zones: &'a mut [Zone]) {
            if zones.len() == 1 {
                out.push(&mut zones[0]);
            } else {
                let (head, tail) = zones.split_at_mut(1);
                out.push(&mut head[0]);
                owned(out, tail);
            }
        };
        owned(&mut out, &mut self.hands[..]);
        owned(&mut out, &mut self.libraries[..]);
        owned(&mut out, &mut self.graveyards[..]);
        out
    }

    /// Set the provided player's library to contain these cards
    ///
    /// The vector should contain a number of `(count, name)` entries, such as `(4, "Storm Crow")`.
    ///
    /// The oracle will be used to look up these cards.
    ///
    /// TODO: Handle the case where card names are invalid
    ///
    /// TODO: This should not shuffle libraries
    pub fn submit_library(&mut self, player: PlayerId, library: Vec<(u32, &str)>) {
        self.import_from_list(ZoneType::Library(player), library);
        self.libraries[player].shuffle();
    }

    pub fn insert_by_name(&mut self, zone: ZoneType, name: &str, owner: PlayerId) -> &RcGameObject {
        let card = match self.oracle.get_card_by_name(name) {
            Some(c) => c.clone(),
            None => panic!("Card {} doesn't exist", name),
        };
        let z = self.find_zone_mut(zone);
        z.insert_from_card(card, owner)
    }

    pub fn import_from_list(&mut self, zone: ZoneType, list: Vec<(u32, &str)>) {
        let owner = zone.owner().unwrap();
        for (count, name) in list {
            for _ in 0..count {
                self.insert_by_name(zone, name, owner);
            }
        }
    }

    pub fn need_first_strike_damage_step(&self) -> bool {
        false
    }
}
