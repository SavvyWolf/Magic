mod gameobject;
mod manapool;
pub mod phases;
mod player;
mod zone;

pub use self::zone::Zone;
pub use self::zone::ZoneType;

pub use self::gameobject::AttackTarget;
pub use self::gameobject::GameObject;
pub use self::gameobject::ObjectId;
pub use self::gameobject::ObjectType;
pub use self::gameobject::ObjectUid;

pub use self::player::Life;
pub use self::player::Player;
pub use self::player::PlayerId;
pub use self::player::TeamId;

pub use self::manapool::ManaPool;

use std::cell::RefCell;
use std::rc::Rc;
pub type RcGameObject = Rc<RefCell<GameObject>>;
