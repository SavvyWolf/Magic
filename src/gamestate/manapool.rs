use cards::mana::Mana;

use std::collections::HashMap;

#[derive(Default, Clone, Debug)]
pub struct ManaPool {
    pool: HashMap<Mana, usize>,
    count: usize,
}

impl ManaPool {
    pub fn insert(&mut self, mana: Mana) {
        let count = *self.pool.get(&mana).unwrap_or(&0);
        self.pool.insert(mana, count + 1);
        self.count += 1;
    }

    pub fn remove(&mut self, mana: Mana) -> Mana {
        let count = *self.pool.get(&mana).unwrap_or(&0);
        assert!(count > 0);
        self.pool.insert(mana, count - 1);
        self.count -= 1;
        mana
    }

    pub fn extend(&mut self, elements: &[Mana]) {
        for e in elements {
            self.insert(*e);
        }
    }

    pub fn from(elements: &[Mana]) -> ManaPool {
        let mut mp: ManaPool = Default::default();
        mp.extend(elements);
        mp
    }

    pub fn get(&self, mana: Mana) -> usize {
        *self.pool.get(&mana).unwrap_or(&0)
    }

    pub fn types(&self) -> Vec<Mana> {
        self.pool
            .keys()
            .cloned()
            .filter(|x| self.get(*x) != 0)
            .collect()
    }

    pub fn empty(&mut self) {
        self.count = 0;
        self.pool.clear()
    }

    pub fn is_empty(&self) -> bool {
        self.types().is_empty()
    }

    pub fn count(&self) -> usize {
        self.count
    }
}
