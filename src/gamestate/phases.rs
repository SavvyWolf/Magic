use game::Game;
use gamestate::PlayerId;

use std::fmt;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Step {
    // Beginning phase
    Untap,
    Upkeep,
    Draw,

    Main,

    // Combat phase
    BeginningOfCombat,
    DeclareAttackers,
    DeclareBlockers,
    FirstStrikeCombatDamage,
    CombatDamage,
    EndOfCombat,

    // End phase
    End,
    Cleanup,

    // Phases used internally by the engine
    NewGame,
}

impl Step {
    pub fn player_has_priority(self) -> bool {
        match self {
            Step::Untap => false,
            Step::Cleanup => false,
            Step::NewGame => false,
            _ => true,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Process {
    // A player has priority
    HavePriority,

    // Passing to the next without priority
    PassToNextStep,

    // Resolving stack
    ResolvingTopOfStack,
}

#[derive(Clone, PartialEq, Eq)]
pub struct GameState {
    pub step: Step,
    pub process: Process,
    pub active_player: PlayerId,
    pub priority: PlayerId,
}

impl GameState {
    pub fn new() -> GameState {
        GameState {
            step: Step::NewGame,
            process: Process::PassToNextStep,
            active_player: 0,
            priority: 0,
        }
    }

    fn next_step(curr: Step) -> Step {
        match curr {
            Step::NewGame => Step::Untap,
            Step::Untap => Step::Upkeep,
            Step::Upkeep => Step::Draw,
            Step::Draw => Step::Main,
            Step::Main => Step::BeginningOfCombat,
            Step::BeginningOfCombat => Step::DeclareAttackers,
            Step::DeclareAttackers => Step::DeclareBlockers,
            Step::DeclareBlockers => Step::FirstStrikeCombatDamage,
            Step::FirstStrikeCombatDamage => Step::CombatDamage,
            Step::CombatDamage => Step::EndOfCombat,
            Step::EndOfCombat => Step::Main,
            Step::End => Step::Cleanup,
            Step::Cleanup => Step::Untap,
        }
    }

    pub fn advance(mut self, game: &Game) -> GameState {
        match self.process {
            Process::HavePriority => {
                self.priority += 1;
                self.priority %= game.num_players();

                if self.priority == self.active_player {
                    // Every player has passed priority
                    self.goto_next_step(game);
                }
            }
            Process::PassToNextStep => {
                self.goto_next_step(game);
            }
            Process::ResolvingTopOfStack => {
                self.process = Process::HavePriority;
            }
        };
        self
    }

    pub fn goto_next_step(&mut self, game: &Game) {
        if !game.stack_is_empty() {
            self.process = Process::ResolvingTopOfStack;
        } else {
            self.priority = self.active_player;
            self.process = Process::HavePriority;
            self.step = Self::next_step(self.step);

            // Skip combat if we have no combat phases left
            if self.step == Step::BeginningOfCombat && game.combat_phases_left == 0 {
                self.step = Step::End;
            }

            // Skip other combat steps if there are no attackers
            if self.step == Step::DeclareBlockers && !game.any_attackers() {
                self.step = Step::EndOfCombat;
            }

            // Don't do first strike damage if there is no first strikers
            if self.step == Step::FirstStrikeCombatDamage && !game.need_first_strike_damage_step() {
                self.step = Step::CombatDamage;
            }

            if !self.step.player_has_priority() {
                // Players don't gain priority in these steps
                self.process = Process::PassToNextStep;
            }
        }
    }
}

impl fmt::Display for GameState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "<GameState {:?}:{:?} (AP: {}, P: {})>",
            self.step, self.process, self.active_player, self.priority
        )
    }
}

impl Default for GameState {
    fn default() -> Self {
        GameState::new()
    }
}
