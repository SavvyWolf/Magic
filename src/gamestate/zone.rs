use super::RcGameObject;
use cards::Card;
use gamestate::GameObject;
use gamestate::ObjectId;
use gamestate::ObjectType;
use gamestate::PlayerId;

use std::cell::RefCell;
use std::iter::FromIterator;
use std::ops::Index;
use std::rc::Rc;
use std::slice::{Iter, IterMut};

use strum_macros::{Display, EnumString};

#[derive(EnumString, Display, Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum ZoneType {
    Rules,
    Battlefield,
    Stack,
    TriggeredWaiting,
    Exile,
    Hand(PlayerId),
    Library(PlayerId),
    Graveyard(PlayerId),
}

impl ZoneType {
    pub fn is_battlefield(&self) -> bool {
        match self {
            ZoneType::Battlefield => true,
            _ => false,
        }
    }
    pub fn is_hand(&self) -> bool {
        match self {
            ZoneType::Hand(..) => true,
            _ => false,
        }
    }
    pub fn is_library(&self) -> bool {
        match self {
            ZoneType::Library(..) => true,
            _ => false,
        }
    }
    pub fn is_rules(&self) -> bool {
        match self {
            ZoneType::Rules => true,
            _ => false,
        }
    }
    pub fn is_stack(&self) -> bool {
        match self {
            ZoneType::Stack => true,
            _ => false,
        }
    }
    pub fn owner(&self) -> Option<PlayerId> {
        match self {
            ZoneType::Library(o) => Some(*o),
            ZoneType::Hand(o) => Some(*o),
            ZoneType::Graveyard(o) => Some(*o),
            ZoneType::Battlefield => None,
            ZoneType::Rules => None,
            ZoneType::Stack => None,
            ZoneType::TriggeredWaiting => None,
            ZoneType::Exile => None,
        }
    }
}

use rand;
use rand::Rng;

pub struct Zone {
    contents_type: ObjectType,
    objects: Vec<RcGameObject>,
    zone_type: ZoneType,
    id_counter: ObjectId,
}

impl Zone {
    pub fn shared(zone_type: ZoneType, contents_type: ObjectType) -> Zone {
        Zone {
            contents_type,
            objects: Vec::new(),
            zone_type,
            id_counter: 0,
        }
    }

    pub fn owned(zone_type: ZoneType, contents_type: ObjectType, _player: PlayerId) -> Zone {
        Zone {
            contents_type,
            objects: Vec::new(),
            zone_type,
            id_counter: 0,
        }
    }

    fn reverse_index(&self, i: usize) -> usize {
        self.objects.len() - i - 1
    }

    pub fn filter(&self, pred: &dyn Fn(&GameObject) -> bool) -> Vec<&RcGameObject> {
        Vec::from_iter(self.objects.iter().filter(|x| pred(&x.borrow())))
    }

    pub fn len(&self) -> usize {
        self.objects.len()
    }

    pub fn is_empty(&self) -> bool {
        self.objects.is_empty()
    }

    pub fn iter(&self) -> Iter<RcGameObject> {
        self.objects.iter()
    }

    pub fn iter_mut(&mut self) -> IterMut<RcGameObject> {
        self.objects.iter_mut()
    }

    pub fn push(&mut self, object: RcGameObject) -> &RcGameObject {
        assert!(object.borrow().go_type == self.contents_type);
        object.borrow_mut().zone = Some(self.zone_type);
        object.borrow_mut().id = self.id_counter;
        object.borrow_mut().summoning_sick = true;
        self.id_counter += 1;
        self.objects.push(object);
        &self.objects[self.objects.len() - 1]
    }

    pub fn pop(&mut self) -> RcGameObject {
        let top = self.objects.pop().unwrap();
        let mut top_borrow = top.borrow_mut();
        top_borrow.zone = None;
        top_borrow.id = 0;
        drop(top_borrow);
        top
    }

    pub fn top(&self) -> &RcGameObject {
        &self.objects[self.objects.len() - 1]
    }

    pub fn remove_id(&mut self, id: ObjectId) -> Option<RcGameObject> {
        let pos;
        {
            pos = self.objects.iter().position(|x| x.borrow().id == id)?;
        }
        let to_return = self.objects.remove(pos);
        let mut to_return_borrow = to_return.borrow_mut();
        to_return_borrow.zone = None;
        drop(to_return_borrow);
        Some(to_return)
    }

    pub fn remove_index(&mut self, index: usize) -> Option<RcGameObject> {
        if index > self.objects.len() {
            None
        } else {
            let id = self.reverse_index(index);
            let to_return = self.objects.remove(id);
            let mut to_return_borrow = to_return.borrow_mut();
            to_return_borrow.zone = None;
            drop(to_return_borrow);
            Some(to_return)
        }
    }

    pub fn get_id(&self, id: ObjectId) -> Option<&RcGameObject> {
        Some(self.objects.iter().find(|x| x.borrow().id == id)?)
    }

    pub fn shuffle(&mut self) {
        rand::thread_rng().shuffle(&mut self.objects);
    }

    pub fn transfer_from_by_index(&mut self, other: &mut Zone, i: usize) {
        let obj = other.remove_index(i).unwrap();
        self.push(obj);
    }

    pub fn insert_from_card(&mut self, card: Rc<Card>, owner: PlayerId) -> &RcGameObject {
        let new = Rc::new(RefCell::new(GameObject::cardobject_from_card(&card, owner)));
        new.borrow_mut().go_type = self.contents_type;
        self.push(new)
    }

    pub fn reset(&mut self) {
        for o in self.objects.iter_mut() {
            o.borrow_mut().reset();
        }
    }

    pub fn get_type(&self) -> ZoneType {
        self.zone_type
    }
}

impl Index<usize> for Zone {
    type Output = RcGameObject;

    fn index(&self, i: usize) -> &RcGameObject {
        &self.objects[self.reverse_index(i)]
    }
}

impl Clone for Zone {
    fn clone(&self) -> Self {
        Self {
            contents_type: self.contents_type,
            objects: self
                .objects
                .iter()
                .map(|x| Rc::new(RefCell::new(x.borrow().clone())))
                .collect(),
            zone_type: self.zone_type,
            id_counter: self.id_counter,
        }
    }
}
