use super::zone::ZoneType;
use abilities::{ActivatedAbility, StaticEffect, TriggeredAbility};
use cards::properties::DisplayColour;
use cards::properties::{CardSubtype, CardSupertype, CardType};
use cards::Card;
use filter::PlaceholderData;
use gamestate::PlayerId;

use std::cell::RefCell;
use std::fmt;
use std::hash::{Hash, Hasher};
use std::rc::Rc;
use std::sync::atomic::{AtomicUsize, Ordering};

static UID_COUNTER: AtomicUsize = AtomicUsize::new(0);

pub type ObjectId = usize;
pub type ObjectUid = usize;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum AttackTarget {
    Player(PlayerId),
    None,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum ObjectType {
    Permanent,
    Card,
    Stack,
}

#[derive(Clone)]
pub struct GameObject {
    pub go_type: ObjectType,
    pub card: Rc<Card>,

    pub id: ObjectId,
    pub uid: ObjectUid,
    pub zone: Option<ZoneType>,
    pub activated_abilities: Vec<ActivatedAbility>,
    pub static_effects: Vec<Box<dyn StaticEffect>>,
    pub triggered_abilities: Vec<TriggeredAbility>,

    pub stored_data: PlaceholderData,

    pub owner: PlayerId,
    pub controller: PlayerId,

    pub name: String,
    pub types: Vec<CardType>,
    pub subtypes: Vec<CardSubtype>,
    pub supertypes: Vec<CardSupertype>,

    pub power: i32,
    pub toughness: i32,
    pub loyalty: i32,
    pub marked_damage: i32,

    pub tapped: bool,

    pub attacking: AttackTarget,
    pub attack_pending: AttackTarget,
    pub attack_assignment_order: Vec<ObjectId>,
    pub attack_damage_remaining: i32,
    pub attack_damage_values: Vec<i32>,

    pub blocking: Option<ObjectId>,
    pub block_pending: Option<ObjectId>,

    pub summoning_sick: bool,
}
impl GameObject {
    pub fn cardobject_from_card(card: &Rc<Card>, owner: PlayerId) -> GameObject {
        let uid = UID_COUNTER.fetch_add(1, Ordering::SeqCst);
        GameObject {
            go_type: ObjectType::Card,
            card: card.clone(),
            zone: None,
            id: 0,
            uid,
            activated_abilities: vec![],
            static_effects: vec![],
            triggered_abilities: vec![],
            stored_data: Default::default(),
            owner,
            controller: owner,
            name: card.name.clone(),
            types: card.types.clone(),
            subtypes: card.subtypes.clone(),
            supertypes: card.supertypes.clone(),
            tapped: false,
            power: card.power,
            toughness: card.toughness,
            loyalty: 0,
            marked_damage: 0,
            attacking: AttackTarget::None,
            attack_pending: AttackTarget::None,
            // Stored first -> last
            attack_assignment_order: vec![],
            attack_damage_remaining: 0,
            attack_damage_values: vec![],
            blocking: None,
            block_pending: None,
            summoning_sick: true,
        }
    }

    pub fn sig(&self) -> String {
        format!("{} ({})", self.name, self.id)
    }

    pub fn is_type(&self, card_type: CardType) -> bool {
        self.types.contains(&card_type)
    }
    pub fn is_subtype(&self, card_subtype: CardSubtype) -> bool {
        self.subtypes.contains(&card_subtype)
    }
    pub fn is_supertype(&self, card_supertype: CardSupertype) -> bool {
        self.supertypes.contains(&card_supertype)
    }

    pub fn reset(&mut self) {
        self.controller = self.owner;
        self.name = self.card.name.clone();
        self.types = self.card.types.clone();
        self.subtypes = self.card.subtypes.clone();
        self.supertypes = self.card.supertypes.clone();
        self.activated_abilities = vec![];
        self.static_effects = self.card.static_effects.clone();
        self.triggered_abilities = self.card.triggered_abilities.clone();
        self.activated_abilities = self.card.activated_abilities.clone();
        self.power = self.card.power;
        self.toughness = self.card.toughness;
    }

    pub fn get_display_colour(&self) -> DisplayColour {
        self.card.display_colour()
    }

    pub fn transform(&self, new_type: ObjectType) -> Rc<RefCell<GameObject>> {
        assert_eq!(
            self.zone, None,
            "Cannot transform an object if it's in a zone"
        );
        let mut new = self.clone();
        new.go_type = new_type;

        // Any logic happening on type switching should go here
        Rc::new(RefCell::new(new))
    }

    pub fn can_attack(&self, player: PlayerId) -> bool {
        // TODO: Haste and restrictions
        self.is_type(CardType::Creature)
            && !self.tapped
            && self.controller == player
            && !self.summoning_sick
    }

    pub fn can_block(&self, player: PlayerId) -> bool {
        // TODO: Restrictions
        self.is_type(CardType::Creature) && !self.tapped && self.controller == player
    }
}

impl fmt::Display for GameObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ({})", self.card.name, self.id)
    }
}

impl fmt::Debug for GameObject {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} ({})", self.card.name, self.id)
    }
}

impl PartialEq<GameObject> for GameObject {
    fn eq(&self, o: &Self) -> bool {
        self.uid == o.uid
    }
}
impl Eq for GameObject {}

impl Hash for GameObject {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.uid.hash(state);
    }
}
