use gamestate::ManaPool;

pub type PlayerId = usize;
pub type TeamId = usize;
pub type Life = i32;

#[derive(Clone)]
pub struct Player {
    pub id: PlayerId,
    pub team: TeamId,
    pub land_drops_left: usize,
    pub mana_pool: ManaPool,
    pub life: Life,
}

impl Player {
    pub fn new(id: PlayerId, team: TeamId, life: Life) -> Player {
        Player {
            id,
            team,
            land_drops_left: 0,
            mana_pool: Default::default(),
            life,
        }
    }
}
