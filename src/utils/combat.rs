use game::Game;
use gamestate::{AttackTarget, RcGameObject};

pub fn get_blockers(game: &Game, attacker: &RcGameObject) -> Vec<RcGameObject> {
    let attacker = attacker.borrow();
    game.battlefield
        .iter()
        .filter(|go| {
            let go = go.borrow();
            go.blocking == Some(attacker.id)
        })
        .cloned()
        .collect()
}

pub fn get_attackers(game: &Game) -> Vec<RcGameObject> {
    game.battlefield
        .iter()
        .filter(|go| {
            let go = go.borrow();
            go.attacking != AttackTarget::None
        })
        .cloned()
        .collect()
}
