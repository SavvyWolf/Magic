mod combat;
mod payment;

use game::Game;
use gamestate::{ObjectType, PlayerId, RcGameObject};

pub use self::combat::*;
pub use self::payment::*;

pub fn put_into_play(game: &mut Game, object: &RcGameObject) {
    let original_zone = object.borrow().zone;
    let zone = game.find_zone_mut(original_zone.unwrap());
    let id = object.borrow().id;

    let card = zone.remove_id(id).unwrap();
    let card = card.borrow().transform(ObjectType::Permanent);

    game.battlefield.push(card);
}

pub fn apnap(ap: PlayerId, num_players: usize) -> Box<dyn Iterator<Item = PlayerId>> {
    Box::new((ap..num_players).chain(0..ap))
}
