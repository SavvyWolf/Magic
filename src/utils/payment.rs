use actions::{Action, Usability};
use cards::mana::{CostEntry, Mana};
use game::Game;
use gamestate::PlayerId;
use gamestate::RcGameObject;
use gamestate::{Life, ManaPool, ZoneType};
use rules::TapObjectSymbolAction;

type CostOffset = usize;
type PaymentCandidate = Vec<(CostOffset, CostEntry, Mana)>;
type TapList = Vec<(RcGameObject, usize)>;

#[derive(Copy, Clone, Hash, Debug)]
pub enum PaymentChoice {
    NotNeeded,
}

#[derive(Default, Clone, Debug)]
pub struct PaymentPlan {
    pub tap: Vec<(RcGameObject, usize)>,
    pub resolve: Vec<PaymentChoice>,
    pub payment: Vec<(CostEntry, Mana)>,
}

fn min_max_cmc(cost: &[CostEntry]) -> (usize, usize) {
    let mut min = 0;
    let mut max = 0;
    for c in cost {
        match c {
            CostEntry::Basic(_) => {
                min += 1;
                max += 1;
            }
            CostEntry::Hybrid(_, _) => {
                min += 1;
                max += 1;
            }
            CostEntry::GenericHybrid(_) => {
                min += 1;
                max += 2;
            }
            CostEntry::Phyrexian(_) => {
                max += 1;
            }
            CostEntry::Generic(x) => {
                min += x;
                max += x;
            }
            CostEntry::X => {
                min += 0;
                max += 0;
            }
            CostEntry::Snow => {
                min += 1;
                max += 1;
            }
        }
    }
    (min, max)
}

/// Takes a mana cost and a mana pool, and returns whether the pool could satisfy the cost
///
/// This functions with all types of mana, and will return true iff mana in the pool could, in some combination, pay the
/// cost.
///
/// The player's life total must be provided, for Phyrexian mana.
///
/// Please note that at the current time, this is not 100% guaranteed. Static effects in the board could mean that, for
/// example, Phyrexian mana couldn't be paid.
#[derive(Clone, Debug)]
pub enum PaymentType {
    Pool,
    Tap(RcGameObject, usize), // Source, offset
}

fn options_from_board(
    cost: &[CostEntry],
    game: &Game,
    player: PlayerId,
) -> Vec<(TapList, PaymentCandidate)> {
    // Crack each combination of objects, and see if it works
    let pool = game.players[player].mana_pool.clone();
    let candidates: Vec<_> = game
        .find_zone(ZoneType::Battlefield)
        .iter()
        .filter(|x| x.borrow().controller == player)
        .map(|x| (x.clone(), can_tap_for(x, game)))
        .filter(|(_go, m)| !m.is_empty())
        .collect();
    let min_max = min_max_cmc(&cost);
    let cost: Vec<_> = cost.iter().cloned().enumerate().collect();

    options_from_board_inner(&cost, game, player, &candidates, pool, vec![], min_max)
}

pub fn options_from_board_inner(
    cost: &[(usize, CostEntry)],
    game: &Game,
    player: PlayerId,
    candidates: &[(RcGameObject, Vec<Vec<Mana>>)],
    built: ManaPool,
    taplist: TapList,
    (min, max): (usize, usize),
) -> Vec<(TapList, PaymentCandidate)> {
    // Crack each combination of objects, and see if it works
    if candidates.is_empty() || built.count() >= max {
        // No more can be cracked, we have a full pool
        // OR we have the maximum CMC, and we don't need to crack anything else
        payment_info(&cost, built, game.players[player].life)
            .iter()
            .map(|x| (taplist.clone(), x.clone()))
            .collect()
    } else {
        let mut out = vec![];

        // Do every combination of the head of the list
        let head = &candidates[0];
        let tail = &candidates[1..];

        // Don't bother checking if we can't pay
        let (obj, options) = head;
        for (i, o) in options.iter().enumerate() {
            // Card would have {T}: Add {G} or {W}, for example, each is a seperate option
            let mut new = built.clone();
            let mut new_tl = taplist.clone();
            new.extend(o);
            new_tl.push((obj.clone(), i));
            out.extend(options_from_board_inner(
                cost,
                game,
                player,
                tail,
                new,
                new_tl,
                (min, max),
            ));
        }
        // Also check if we can cast the spell without tapping anything
        out.extend(options_from_board_inner(
            cost,
            game,
            player,
            tail,
            built,
            taplist,
            (min, max),
        ));
        out
    }
}

pub fn payment_plan_from_board(
    cost: &[CostEntry],
    game: &Game,
    player: PlayerId,
) -> Vec<PaymentPlan> {
    let mut out = vec![];
    for (taplist, option) in options_from_board(cost, game, player) {
        let mut plan: PaymentPlan = Default::default();

        // What things need tapped
        for (obj, offset) in &taplist {
            plan.tap.push((obj.clone(), *offset));
        }

        // Resolve options (Pay life for phyrexian, etc)
        plan.resolve = cost
            .iter()
            .map(|c| match c {
                CostEntry::Basic(_) => PaymentChoice::NotNeeded,
                CostEntry::Generic(_) => PaymentChoice::NotNeeded,
                _ => panic!("Could not generate payment plan for {}", c),
            })
            .collect();

        // Compute the payments needed
        plan.payment = option
            .iter()
            .map(|(_, entry, mana)| (*entry, *mana))
            .collect();

        out.push(plan);
    }
    out
}

fn could_pay(cost: &[(usize, CostEntry)], sources: ManaPool, life: Life) -> bool {
    !payment_info(cost, sources, life).is_empty()
}

pub fn could_pay_from_board(cost: &[CostEntry], game: &Game, player: PlayerId) -> bool {
    !options_from_board(cost, game, player).is_empty()
}

pub fn could_pay_from_pool(cost: &[CostEntry], pool: ManaPool, life: i32) -> bool {
    let cost: Vec<_> = cost.iter().cloned().enumerate().collect();
    could_pay(&cost, pool, life)
}

fn payment_info(
    cost: &[(usize, CostEntry)],
    sources: ManaPool,
    life: Life,
) -> Vec<PaymentCandidate> {
    if cost.is_empty() {
        return vec![vec![]];
    }
    let head = cost[0];
    let tail: &[(usize, CostEntry)] = &cost[1..];

    if let (i, CostEntry::Generic(n)) = head {
        if n > 1 {
            let mut new_tail = tail.to_vec();
            for _ in 0..n {
                new_tail.push((i, CostEntry::Generic(1)));
            }
            return payment_info(&new_tail, sources, life);
        }
    }

    let mut my_payment_options = vec![];

    let mut add_option = |new: Option<Mana>, tail, sources, life| {
        let child_payment_options = payment_info(tail, sources, life);
        for cpo in child_payment_options {
            if let Some(m) = new {
                my_payment_options.push([vec![(head.0, head.1, m)], cpo].concat());
            } else {
                my_payment_options.push(cpo);
            }
        }
    };

    let index = head.0;
    match head.1 {
        CostEntry::X => {
            add_option(None, tail, sources, life);
        }
        CostEntry::Generic(0) => {
            add_option(None, tail, sources, life);
        }
        CostEntry::Phyrexian(c) => {
            // Either 2 life or mana
            // Life
            if life >= 2 {
                add_option(None, tail, sources.clone(), life - 2);
            }

            // Mana
            let mut new_cost = tail.to_vec();
            new_cost.push((index, CostEntry::Basic(c)));
            add_option(None, &new_cost, sources, life);
        }
        CostEntry::GenericHybrid(c) => {
            // Either {2} or {M}
            // {M}
            let mut new_cost = tail.to_vec();
            new_cost.push((index, CostEntry::Basic(c)));
            add_option(None, &new_cost, sources.clone(), life);

            // {2}
            let mut new_cost = tail.to_vec();
            new_cost.push((index, CostEntry::Generic(2)));
            add_option(None, &new_cost, sources, life);
        }
        _ => {
            for m in sources.types() {
                if m.could_pay(&head.1) {
                    let mut new_sources = sources.clone();
                    new_sources.remove(m);
                    add_option(Some(m), tail, new_sources, life);
                }
            }
        }
    }
    my_payment_options
}

pub fn can_tap_for(go: &RcGameObject, game: &Game) -> Vec<Vec<Mana>> {
    let mut out = vec![];
    let go_bor = go.borrow();

    for aa in &go_bor.activated_abilities {
        if aa.cost.len() != 1 {
            continue;
        }
        if let Some(tap) = aa.cost[0].as_any().downcast_ref::<TapObjectSymbolAction>() {
            if tap.get_usability(game, Some(go)) == Usability::Usable {
                for option in aa.action.could_produce_mana(game, Some(go)) {
                    out.push(option);
                }
            }
        }
    }

    out
}
