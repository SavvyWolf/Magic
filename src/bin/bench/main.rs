extern crate magic;

use magic::cards::mana::Colour::*;
use magic::cards::mana::CostEntry;
use magic::cards::mana::Mana;
use magic::cards::properties::{CardSubtype, CardType};
use magic::cards::Card;
use magic::game::Game;
use magic::gamestate::ZoneType;
use magic::utils::payment_plan_from_board;

use std::path::PathBuf;
use std::time::Instant;

const ITERS: u32 = 50_000;

#[cfg_attr(tarpaulin, skip)]
fn main() {
    // ============
    // Game cloning
    // ============
    let mut game = Game::new_no_teams(2, PathBuf::from("cards/"));
    game.submit_library(
        0,
        vec![(20, "Plains"), (20, "Sacred Cat"), (20, "Revitalize")],
    );
    game.submit_library(1, vec![(20, "Forest"), (40, "Young Wolf")]);

    println!("Running game.clone() {} times...", ITERS);
    let now = Instant::now();
    for _ in 0..ITERS {
        let _game = game.clone();
    }
    let duration = now.elapsed();
    println!("Took {:?}", duration / ITERS);

    // ==================
    // Cost Payment Plans
    // ==================
    let mut game = Game::new_no_teams(2, PathBuf::from("cards/"));
    game.submit_library(0, vec![(60, "Plains")]);
    game.submit_library(1, vec![(60, "Forest")]);
    game.oracle.insert(Card {
        name: "Dual".to_string(),
        types: vec![CardType::Land],
        subtypes: vec![CardSubtype::Plains, CardSubtype::Forest],
        ..Default::default()
    });

    game.insert_by_name(ZoneType::Battlefield, "Forest", 0);
    game.insert_by_name(ZoneType::Battlefield, "Forest", 0);
    game.insert_by_name(ZoneType::Battlefield, "Forest", 0);
    game.insert_by_name(ZoneType::Battlefield, "Plains", 0);
    game.insert_by_name(ZoneType::Battlefield, "Plains", 0);
    game.insert_by_name(ZoneType::Battlefield, "Dual", 0);
    game.begin();
    game.calculate();

    println!("Running payment_plan_from_board {} times...", ITERS);
    let now = Instant::now();
    for _ in 0..ITERS {
        payment_plan_from_board(
            &[
                CostEntry::Generic(3),
                CostEntry::Basic(White),
                CostEntry::Basic(Green),
            ],
            &game,
            0,
        );
    }
    let duration = now.elapsed();
    println!("Took {:?}", duration / ITERS);

    // ==================
    // Pool Payment Plans
    // ==================
    let mut game = Game::new_no_teams(2, PathBuf::from("cards/"));
    game.submit_library(0, vec![(60, "Plains")]);
    game.submit_library(1, vec![(60, "Forest")]);
    game.oracle.insert(Card {
        name: "Dual".to_string(),
        types: vec![CardType::Land],
        subtypes: vec![CardSubtype::Plains, CardSubtype::Forest],
        ..Default::default()
    });

    game.begin();
    game.calculate();

    game.players[0]
        .mana_pool
        .insert(Mana::new_unconditional(Green));
    game.players[0]
        .mana_pool
        .insert(Mana::new_unconditional(Green));
    game.players[0]
        .mana_pool
        .insert(Mana::new_unconditional(Green));
    game.players[0]
        .mana_pool
        .insert(Mana::new_unconditional(White));
    game.players[0]
        .mana_pool
        .insert(Mana::new_unconditional(White));
    game.players[0]
        .mana_pool
        .insert(Mana::new_unconditional(White));

    println!(
        "Running payment_plan_from_board {} times (mana pool)...",
        ITERS
    );
    let now = Instant::now();
    for _ in 0..ITERS {
        payment_plan_from_board(
            &[
                CostEntry::Generic(3),
                CostEntry::Basic(White),
                CostEntry::Basic(Green),
            ],
            &game,
            0,
        );
    }
    let duration = now.elapsed();
    println!("Took {:?}", duration / ITERS);
}
