extern crate magic;

#[cfg_attr(tarpaulin, skip)]
mod ui;

use magic::actions::ActionEntry;
use magic::actions::ExpectedUserInput;
use magic::agent::{Agent, AgentMap, GoldfishAgent};
use magic::game::Game;
use std::path::PathBuf;
use ui::textdisplay::TextDisplay;

struct ConsoleAgent {
    display: TextDisplay,
}

impl ConsoleAgent {
    fn new() -> ConsoleAgent {
        ConsoleAgent {
            display: TextDisplay::new(),
        }
    }
}

impl Agent for ConsoleAgent {
    fn request_action_choice(
        &mut self,
        game: &Game,
        expected: ExpectedUserInput,
        options: Vec<ActionEntry>,
    ) -> ActionEntry {
        self.display.read_action(&game, expected, &options)
    }

    fn request_range(
        &mut self,
        game: &Game,
        _expected: ExpectedUserInput,
        min: i32,
        max: i32,
    ) -> i32 {
        self.display.read_range(&game, min, max)
    }
}

#[cfg_attr(tarpaulin, skip)]
fn main() {
    let mut game = Game::new_no_teams(2, PathBuf::from("cards/"));

    game.submit_library(
        0,
        vec![
            (20, "Plains"),
            (20, "Sacred Cat"),
            (20, "Revitalize"),
            (20, "Shock, but white"),
        ],
    );
    game.submit_library(1, vec![(20, "Forest"), (40, "Young Wolf")]);

    let mut am: AgentMap = Default::default();
    am.insert(0, Box::new(ConsoleAgent::new()));
    //am.insert(1, Box::new(ConsoleAgent::new()));
    am.insert(1, Box::new(GoldfishAgent {}));

    game.run(&mut am);
}
