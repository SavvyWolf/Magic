use magic::actions::{ActionEntry, ExpectedUserInput, Usability};
use magic::agent::AutoPasser;
use magic::cards::mana::CostEntry;
use magic::cards::properties::CardType;
use magic::cards::properties::DisplayColour;
use magic::cards::CardKind;
use magic::game::Game;
use magic::gamestate::AttackTarget;
use magic::gamestate::GameObject;
use magic::gamestate::{PlayerId, RcGameObject};
use magic::rules::DeclareDamageAction;

use std::io;
use std::io::Write;

extern crate console;
use self::console::Style;
use self::console::Term;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Context {
    Hand,
    Land,
    Battlefield,
    Stack,
}

const COMMANDS: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

const FRAME_NORMAL: &str = "═║╔╗╚╝";
const FRAME_TOKEN: &str = "─│┌┐└┘";
const F_HOR: usize = 0;
const F_VER: usize = 1;
const F_UL: usize = 2;
const F_UR: usize = 3;
const F_BL: usize = 4;
const F_BR: usize = 5;

fn get_frame_char(target: &GameObject, select: usize) -> char {
    if target.card.kind == CardKind::CardOrPermanent {
        FRAME_NORMAL.chars().nth(select).unwrap()
    } else {
        FRAME_TOKEN.chars().nth(select).unwrap()
    }
}

trait DisplayAsText {
    fn text_display(&self, context: Context, colour: Style, width: usize) -> String;
    fn text_display_2(&self, _context: Context, _colour: Style, width: usize) -> String {
        str::repeat(" ", width)
    }
}

impl DisplayAsText for GameObject {
    fn text_display(&self, _context: Context, _colour: Style, width: usize) -> String {
        format!(
            "{:03} {}",
            self.id,
            pad(&shorten(self.name.clone(), width - 4), width - 4)
        )
    }
    fn text_display_2(&self, context: Context, _colour: Style, width: usize) -> String {
        if context == Context::Battlefield {
            let astr = if self.attacking != AttackTarget::None {
                "⚔️ "
            } else if self.blocking != None {
                "🛡️ "
            } else {
                "  "
            }; // Need the space for terminals
            let ssstr = if self.summoning_sick && self.is_type(CardType::Creature) {
                "🌀"
            } else {
                "  "
            };

            let mut ptstr_len = 0;

            // Power
            ptstr_len += self.power.to_string().chars().count();
            let pstr = Style::new().white().apply_to(self.power);

            // Toughness
            let tstr = if self.marked_damage != 0 {
                let toughness = self.toughness - self.marked_damage;
                ptstr_len += toughness.to_string().chars().count();
                Style::new().red().apply_to(toughness)
            } else {
                ptstr_len += self.toughness.to_string().chars().count();
                Style::new().white().apply_to(self.toughness)
            };

            // Composed string
            let ptstr = format!(
                "{}{}{}{}",
                " ".repeat(width - 5 - ptstr_len),
                pstr,
                Style::new().white().apply_to("/"),
                tstr
            );
            format!("{}{}{}", astr, ssstr, ptstr)
        } else {
            let mut cstr = String::new();
            for c in &self.card.cost {
                cstr.push_str(&format!("{}", c));
            }
            format!("{}{}", " ".repeat(width - cstr.len()), cstr)
        }
    }
}

fn style_for_display_colour(object: &GameObject) -> Style {
    let display = object.get_display_colour();
    let mut base = match display {
        DisplayColour::White => Style::new().white(),
        DisplayColour::Blue => Style::new().blue(),
        DisplayColour::Black => Style::new().black(),
        DisplayColour::Red => Style::new().red(),
        DisplayColour::Green => Style::new().green(),
        DisplayColour::Gold => Style::new().yellow(),
        DisplayColour::Colorless => Style::new().white(),
    };
    if object.tapped {
        base = base.reverse();
    };
    base
}

fn pad(text: &str, len: usize) -> String {
    let len = len - text.len();
    format!("{}{}", text, " ".repeat(len))
}

fn shorten(mut text: String, limit: usize) -> String {
    while text.len() > limit {
        let mut new_text = String::new();
        {
            let frags: Vec<&str> = text.split(' ').collect();

            let longest = frags
                .iter()
                .max_by_key(|x| x.len())
                .expect("Card name is empty");
            if longest.len() == 2 {
                break;
            }
            for f in frags.iter() {
                if f == longest {
                    new_text.push_str(&f[0..1]);
                    new_text.push_str(".");
                } else {
                    new_text.push_str(f);
                }
                new_text.push_str(" ");
            }
        }
        text = new_text;
    }
    if text.len() > limit {
        text = text[0..(limit - 3)].to_string() + "...";
    }
    text
}

fn cost_string(cost: &[CostEntry]) -> String {
    let mut c = String::new();
    for m in cost.iter() {
        c = format!("{}{}", c, m);
    }

    c
}

pub struct TextDisplay {
    width: usize,
    auto_pass: bool,
    auto_passer: AutoPasser,
    text_width: usize,
    box_width: usize,
}

impl TextDisplay {
    pub fn new() -> TextDisplay {
        TextDisplay {
            width: 0,
            text_width: 0,
            box_width: 0,
            auto_pass: false,
            auto_passer: Default::default(),
        }
    }

    fn box_list(&self, objects: Vec<&RcGameObject>, context: Context) {
        let mut y = 0;
        let blocks = (self.width - 2) / self.box_width;
        let rem = (self.width - 2) % self.box_width;

        loop {
            if y >= objects.len() {
                break;
            }

            // Top row
            print!("│");
            for x in 0..blocks {
                if y + x >= objects.len() {
                    print!("{}", " ".repeat(self.box_width));
                } else {
                    let o = &objects[y + x].borrow();
                    let style = style_for_display_colour(o);
                    print!(
                        "{}",
                        style.apply_to(format!(
                            "{}{}{}",
                            get_frame_char(o, F_UL),
                            get_frame_char(o, F_HOR)
                                .to_string()
                                .repeat(self.box_width - 2),
                            get_frame_char(o, F_UR)
                        ))
                    );
                }
            }
            println!("{}│", " ".repeat(rem));

            // Middle Row 1
            print!("│");
            for x in 0..blocks {
                if y + x >= objects.len() {
                    print!("{}", " ".repeat(self.box_width));
                } else {
                    let o = &objects[y + x].borrow();
                    let style = style_for_display_colour(o);
                    print!("{}", style.apply_to(get_frame_char(o, F_VER)));
                    print!(
                        "{}",
                        style.apply_to(objects[y + x].borrow().text_display(
                            context,
                            style.clone(),
                            self.box_width - 2
                        ))
                    );
                    print!("{}", style.apply_to(get_frame_char(o, F_VER)));
                }
            }
            println!("{}│", " ".repeat(rem));

            // Middle Row 2
            print!("│");
            for x in 0..blocks {
                if y + x >= objects.len() {
                    print!("{}", " ".repeat(self.box_width));
                } else {
                    let o = &objects[y + x].borrow();
                    let style = style_for_display_colour(o);
                    print!("{}", style.apply_to(get_frame_char(o, F_VER)));
                    print!(
                        "{}",
                        style.apply_to(objects[y + x].borrow().text_display_2(
                            context,
                            style.clone(),
                            self.box_width - 2
                        ))
                    );
                    print!("{}", style.apply_to(get_frame_char(o, F_VER)));
                }
            }
            println!("{}│", " ".repeat(rem));

            // Bottom
            print!("│");
            for x in 0..blocks {
                if y + x >= objects.len() {
                    print!("{}", " ".repeat(self.box_width));
                } else {
                    let o = &objects[y + x].borrow();
                    let style = style_for_display_colour(o);
                    print!(
                        "{}",
                        style.apply_to(format!(
                            "{}{}{}",
                            get_frame_char(o, F_BL),
                            get_frame_char(o, F_HOR)
                                .to_string()
                                .repeat(self.box_width - 2),
                            get_frame_char(o, F_BR)
                        ))
                    );
                }
            }
            println!("{}│", " ".repeat(rem));

            y += blocks;
        }
    }

    fn status_bar(&self, game: &Game, p: PlayerId) {
        let pool = &game.players[p].mana_pool;
        let life = game.players[p].life;

        let mut pool_string = "".to_string();
        for m in pool.types() {
            let d = m.description();
            pool_string.push_str(&format!("{}: {}", &d, pool.get(m)));
        }

        let status = format!("Player {} ({}) {}", p, life, pool_string);

        println!("│{}│", pad(&status, self.width - 2));
    }

    fn action_list<'a>(&self, objects: &'a [ActionEntry], game: &'a Game) {
        let mut y = 0;
        let blocks = (self.width - 2) / self.text_width;
        let rem = (self.width - 2) % self.text_width;

        let mut options = vec![];
        for (i, o) in objects.iter().enumerate() {
            let desc = o.action.description(game, o.object.as_ref(), false);
            let c = COMMANDS
                .chars()
                .nth(i)
                .expect("Ran out of characters for actions");
            options.push((c, desc));
        }
        options.push(('+', "Display all".to_string()));
        if self.auto_pass {
            options.push(('*', "Disable auto-pass".to_string()));
        } else {
            options.push(('*', "Enable auto-pass".to_string()));
        }

        // Action list
        loop {
            if y >= options.len() {
                break;
            }

            print!("│");
            for x in 0..blocks {
                if y + x >= options.len() {
                    print!("{}", " ".repeat(self.text_width));
                } else {
                    print!(
                        "{}",
                        pad(
                            &format!(
                                "{}) {}",
                                options[y + x].0,
                                shorten(options[y + x].1.clone(), self.text_width - 3)
                            ),
                            self.text_width
                        )
                    );
                }
            }
            println!("{}│", " ".repeat(rem));

            y += blocks;
        }
    }

    fn hr(&self, char: &str) {
        println!("├{}┤", char.repeat(self.width - 2));
    }

    fn filter_actions(&self, game: &Game, actions: &[ActionEntry], all: bool) -> Vec<ActionEntry> {
        if all {
            actions.to_vec()
        } else {
            actions
                .iter()
                .filter(|x| x.action.get_usability(game, x.object.as_ref()) == Usability::Usable)
                .cloned()
                .collect()
        }
    }

    pub fn display_game_state(&mut self, game: &Game) {
        self.width = Term::stdout().size().1 as usize;

        if self.width <= 100 {
            self.text_width = (self.width / 2) - 1;
        } else {
            self.text_width = 50;
        }
        if self.width <= 90 {
            self.box_width = (self.width / 3) - 1;
        } else {
            self.box_width = 30;
        }

        let (activity_player, activity) = game.get_expected_user_input();

        println!("┌{}┐", "─".repeat(self.width - 2));

        // Player 1 status
        self.status_bar(game, 1);
        self.box_list(game.hands[1].filter(&|_| true), Context::Hand);
        self.hr("─");

        self.box_list(
            game.battlefield
                .filter(&|x| x.controller == 1 && x.is_type(CardType::Land)),
            Context::Land,
        );
        self.hr("-");
        self.box_list(
            game.battlefield
                .filter(&|x| x.controller == 1 && !x.is_type(CardType::Land)),
            Context::Battlefield,
        );
        self.hr("═");
        self.box_list(
            game.battlefield
                .filter(&|x| x.controller == 0 && !x.is_type(CardType::Land)),
            Context::Battlefield,
        );
        self.hr("-");
        self.box_list(
            game.battlefield
                .filter(&|x| x.controller == 0 && x.is_type(CardType::Land)),
            Context::Land,
        );

        // Player 0 status
        self.hr("─");
        self.status_bar(game, 0);
        self.box_list(game.hands[0].filter(&|_| true), Context::Hand);

        // Stack
        self.hr("─");
        self.box_list(game.stack.filter(&|_| true), Context::Stack);

        // Tell the user what is expected
        self.hr("─");
        let msg: String;
        match activity {
            ExpectedUserInput::None => msg = "!!! THIS IS A BUG !!!".to_string(),
            ExpectedUserInput::PriorityChoice => msg = "You have priority".to_string(),
            ExpectedUserInput::ActivateManaAbilities => {
                msg = format!(
                    "You may activate mana abilities to pay for {}",
                    cost_string(&game.current_cost)
                )
            }
            ExpectedUserInput::MakeManaPayment => {
                msg = format!("Pay {}", cost_string(&game.current_cost))
            }
            ExpectedUserInput::DeclareAttackers => msg = "Declare attackers".to_string(),
            ExpectedUserInput::DeclareBlockers => msg = "Declare blockers".to_string(),
            ExpectedUserInput::PutTriggeredAbilityOnStack => {
                msg = "Put triggered ability onto the stack".to_string()
            }
            ExpectedUserInput::DeclareAttackerAssignmentOrder => {
                let top = &game.peek_action().object;
                msg = format!(
                    "Damage assignment order, select the next blocker for {}",
                    top.as_ref().unwrap().borrow().sig()
                );
            }
            ExpectedUserInput::DeclareAttackerAssignment(_, _) => {
                let action = &game.peek_action().action;
                let as_dam = action
                    .as_any()
                    .downcast_ref::<DeclareDamageAction>()
                    .unwrap();
                msg = format!(
                    "Damage assignment, enter the next damage for {}",
                    as_dam
                        .target(game, &game.peek_action().object.as_ref().unwrap())
                        .borrow()
                        .sig()
                );
            }
            ExpectedUserInput::SelectTarget => msg = "Select target".to_string(),
        };
        let msg = format!(
            "[Player {}'s {:?}] Player {}: {}",
            game.state.active_player, game.state.step, activity_player, msg
        );
        println!("│{}│", pad(&msg, self.width - 2));
    }

    pub fn display_action_box(&self, game: &Game, actions: &[ActionEntry]) {
        self.hr("─");

        // Show usable actions
        self.action_list(&actions, game);

        println!("└{}┘", "─".repeat(self.width - 2));
    }

    pub fn display_range_box(&self, _game: &Game, bottom: i32, top: i32) {
        self.hr("─");

        // Show usable actions
        println!(
            "│{}│",
            pad(
                &format!("Select a value between {} and {}", bottom, top - 1),
                self.width - 2
            )
        );

        println!("└{}┘", "─".repeat(self.width - 2));
    }

    pub fn read_action(
        &mut self,
        game: &Game,
        expected: ExpectedUserInput,
        actions: &[ActionEntry],
    ) -> ActionEntry {
        if self.auto_pass {
            if let Some(a) =
                self.auto_passer
                    .request_action_choice(game, expected, actions.to_vec())
            {
                return a;
            }
        }

        let mut actions_filtered = self.filter_actions(game, actions, false);
        self.display_game_state(&game);
        self.display_action_box(&game, &actions_filtered);
        loop {
            print!("> ");
            let _ = io::stdout().flush();
            let mut input = String::new();
            match io::stdin().read_line(&mut input) {
                Ok(_n) => {
                    let trimmed = input.trim();
                    if trimmed.len() > 1 {
                        eprintln!("Please specify only a single character");
                    } else if let Some(c) = COMMANDS.find(trimmed) {
                        if c >= actions_filtered.len() {
                            eprintln!("Invalid option");
                        } else {
                            return actions_filtered[c].clone();
                        }
                    } else if trimmed == "+" {
                        actions_filtered = self.filter_actions(game, actions, true);
                        self.display_game_state(&game);
                        self.display_action_box(&game, &actions_filtered);
                    } else if trimmed == "*" {
                        self.auto_pass = !self.auto_pass;
                        self.display_game_state(&game);
                        self.display_action_box(&game, &actions_filtered);
                    } else {
                        eprintln!("Invalid option");
                    }
                }
                Err(_error) => panic!(),
            }
        }
    }

    pub fn read_range(&mut self, game: &Game, bottom: i32, top: i32) -> i32 {
        self.display_game_state(&game);
        self.display_range_box(&game, bottom, top);
        loop {
            print!("> ");
            let _ = io::stdout().flush();
            let mut input = String::new();
            match io::stdin().read_line(&mut input) {
                Ok(_n) => {
                    let input = input.trim();
                    match input.parse::<i32>() {
                        Ok(n) => {
                            if n >= bottom && n < top {
                                return n;
                            } else {
                                eprintln!("That value is out of range!");
                            }
                        }
                        Err(_) => eprintln!("Please enter a number"),
                    }
                }
                Err(_error) => panic!(),
            }
        }
    }
}
