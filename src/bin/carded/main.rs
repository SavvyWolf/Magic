extern crate magic;

use magic::cards::Card;
use magic::cards::Oracle;
use magic::parse::mtgjson;

use std::env::args;
use std::io;
use std::io::Write;
use std::ops::Deref;
use std::path::PathBuf;
use std::process::exit;

extern crate console;
use self::console::Style;

#[cfg_attr(tarpaulin, skip)]
fn get_mtgjson(path: &str, set: &str) -> Oracle {
    // Get file if it doesn't exist
    let path = PathBuf::from(path);
    if !path.exists() {
        panic!("MTG JSON doesn't exist at {}...", path.to_str().unwrap());
    }

    let parsed = mtgjson::load_set(path, set).expect("Couldn't load JSON");

    let mut o = Oracle::new(PathBuf::from("/invalid/"));
    o.load_list(parsed);
    o
}

#[cfg_attr(tarpaulin, skip)]
fn print_comparison_table(mtgjson: &Oracle, savmtg: &mut Oracle, mod_list: &[String]) {
    let mut mtgjson = mtgjson.get_list().clone();
    mtgjson.sort_by(|x, y| x.name.partial_cmp(&y.name).unwrap());
    // Dimensions: 30, 20, 15, 10

    println!(
        "┌{}┬{}┬{}┬{}┐",
        "─".repeat(30),
        "─".repeat(15),
        "─".repeat(15),
        "─".repeat(15)
    );

    let mut letter = mtgjson[0].name.chars().nth(0);
    for c in mtgjson {
        if c.name.chars().nth(0) != letter {
            println!(
                "├{}┼{}┼{}┼{}┤",
                "┄".repeat(30),
                "┄".repeat(15),
                "┄".repeat(15),
                "┄".repeat(15)
            );
            letter = c.name.chars().nth(0);
        }

        let sc = savmtg.get_card_by_name(&c.name);
        let exist_status;
        let match_status;
        let rules_status;

        match sc {
            Some(sc) => {
                // Exist status
                if mod_list.contains(&c.name) {
                    exist_status = Style::new()
                        .on_yellow()
                        .apply_to(format!("{:15}", "Unsaved"));
                } else {
                    exist_status = Style::new()
                        .on_green()
                        .apply_to(format!("{:15}", "Present"));
                }

                // Match status
                if does_match(&c, &sc) {
                    match_status = Style::new()
                        .on_green()
                        .apply_to(format!("{:15}", "Matches"));
                } else {
                    match_status = Style::new()
                        .on_red()
                        .apply_to(format!("{:15}", "Doesn't Match"));
                }

                // Rules Status
                if c.rules_as_written.is_empty() {
                    rules_status = Style::new()
                        .on_magenta()
                        .apply_to(format!("{:15}", "No Rules Text"));
                } else if does_rules_match(&c, &sc) {
                    rules_status = Style::new()
                        .on_green()
                        .apply_to(format!("{:15}", "Rules Match"));
                } else {
                    rules_status = Style::new()
                        .on_red()
                        .apply_to(format!("{:15}", "Rules Incorrect"));
                }
            }
            None => {
                exist_status = Style::new()
                    .on_red()
                    .apply_to(format!("{:15}", "Not Present"));
                match_status = Style::new().on_black().apply_to(format!("{:15}", "N/A"));
                if c.rules_as_written.is_empty() {
                    rules_status = Style::new()
                        .on_magenta()
                        .apply_to(format!("{:15}", "No Rules Text"));
                } else {
                    rules_status = Style::new().on_black().apply_to(format!("{:15}", "N/A"));
                }
            }
        }

        println!(
            "│{:30}│{:15}│{:15}│{:15}│",
            c.name, exist_status, match_status, rules_status
        );
    }

    println!(
        "└{}┴{}┴{}┴{}┘",
        "─".repeat(30),
        "─".repeat(15),
        "─".repeat(15),
        "─".repeat(15)
    );
}

#[cfg_attr(tarpaulin, skip)]
fn does_match(a: &Card, b: &Card) -> bool {
    a.name == b.name
        && a.types == b.types
        && a.subtypes == b.subtypes
        && a.supertypes == b.supertypes
        && a.colours == b.colours
        && a.cost == b.cost
        && a.power == b.power
        && a.toughness == b.toughness
        && a.loyalty == b.loyalty
        && a.rules_as_written == b.rules_as_written
}

#[cfg_attr(tarpaulin, skip)]
fn does_rules_match(_a: &Card, _b: &Card) -> bool {
    false
}

#[cfg_attr(tarpaulin, skip)]
fn main() {
    let args: Vec<String> = args().collect();
    if args.len() < 3 {
        eprintln!("Usage: {} allSets.json set_code", args[0]);
        exit(1);
    }
    let mtgjson_path = &args[1];
    let set = &args[2];

    println!("Loading from mtgjson...");
    let mut mtgjson = get_mtgjson(mtgjson_path, set);
    mtgjson.silent = true;
    println!("Done!");

    let mut self_oracle = Oracle::new(PathBuf::from("cards"));
    self_oracle.silent = true;

    let mut mod_list = vec![];
    print_comparison_table(&mtgjson, &mut self_oracle, &mod_list);

    loop {
        print!("> ");
        let _ = io::stdout().flush();
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(_n) => {
                let trimmed = input.trim();
                let cmd = &trimmed[0..1];
                let arg;
                if trimmed.len() > 2 {
                    arg = &trimmed[2..];
                } else {
                    arg = ""
                }

                match cmd {
                    "t" => {
                        print_comparison_table(&mtgjson, &mut self_oracle, &mod_list);
                    }
                    "i" => {
                        println!("Importing {}", arg);
                        let card = mtgjson.get_card_by_name(arg);
                        match card {
                            Some(c) => {
                                mod_list.push(arg.to_string());
                                self_oracle.remove_card_by_name(arg);
                                self_oracle.insert(c.deref().clone());
                            }
                            None => println!("Card not found"),
                        }
                    }
                    "c" => {
                        for cn in mod_list {
                            println!("Committing {}", cn);
                            self_oracle.commit_card_by_name(&cn);
                        }
                        mod_list = vec![];
                    }
                    _ => {
                        eprintln!("Unknown command!");
                        eprintln!("Valid values: c, t, i!");
                    }
                }
            }
            Err(_error) => panic!(),
        }
    }
}
