#[cfg(test)]
use super::*;
use cards::mana::Colour::*;
use cards::mana::CostEntry::*;
use cards::properties::CardType;
use cards::Card;
use filter::Filter::*;
use filter::Placeholder::This;
use filter::Selectable;
use gamestate::phases::Step::*;
use gamestate::ZoneType::*;
use rules::*;

#[test]
fn cast_instant() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(vec![Generic(1), Basic(White)], vec![]);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land_1 = gf.g.insert_by_name(Battlefield, "Plains", 0).clone();
    let land_2 = gf.g.insert_by_name(Battlefield, "Plains", 0).clone();
    let spell = gf.g.insert_by_name(Hand(0), &spell_name, 0).clone();

    assert!(gf.g.find_zone(Stack).is_empty());

    // Start to cast
    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(spell), &format!("Cast {} (7)", spell_name));
    gf.advance_to_next_option();

    // Tap lands
    assert!(gf.g.players[0].mana_pool.is_empty());
    gf.expect_and_perform(Some(land_1), &"{T}: Add {W}");
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(land_2), &"{T}: Add {W}");
    gf.advance_to_next_option();

    // Pay cost
    gf.expect_and_perform_no_obj(&"Finish");
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Pay {W} for {1}");
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Pay {W} for {W}");
    gf.advance_to_next_option();

    // Check the stack is as expected
    assert!(!gf.g.find_zone(Stack).is_empty());
}

#[test]
fn resolve_instant() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(GainLifeAction::new(
            vec![ControllerOf(Selectable::Placeholder(This))],
            vec![Literal(Selectable::Value(1))],
        ))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();

    assert_eq!(gf.g.players[0].life, 20);

    gf.put_cast(&spell_name);

    assert!(!gf.g.find_zone(Stack).is_empty());

    gf.pass_priority_then_advance(); // Player 0
    gf.pass_priority_then_advance(); // Player 1

    assert!(gf.g.find_zone(Stack).is_empty());
    assert_eq!(gf.g.players[0].life, 21);
    assert_eq!(gf.get_game_state(), (Main, 0, 0));
}

#[test]
fn resolve_creature() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();

    assert_eq!(gf.g.players[0].life, 20);

    gf.put_cast(&spell_name);

    assert!(!gf.g.find_zone(Stack).is_empty());
    assert!(gf.g.find_zone(Battlefield).is_empty());

    gf.pass_priority_then_advance(); // Player 0
    gf.pass_priority_then_advance(); // Player 1

    assert!(gf.g.find_zone(Stack).is_empty());
    assert!(!gf.g.find_zone(Battlefield).is_empty());
    assert_eq!(gf.get_game_state(), (Main, 0, 0));
}

#[test]
fn stack_instant() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(GainLifeAction::new(
            vec![ControllerOf(Selectable::Placeholder(This))],
            vec![Literal(Selectable::Value(1))],
        ))],
    );
    let spell_name_2 = of.add_instant(
        vec![],
        vec![Box::new(GainLifeAction::new(
            vec![ControllerOf(Selectable::Placeholder(This))],
            vec![Literal(Selectable::Value(2))],
        ))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();

    assert_eq!(gf.g.players[0].life, 20);

    // First spell
    gf.put_cast(&spell_name);

    // Second spell
    let spell = gf.g.insert_by_name(Hand(0), &spell_name_2, 0).clone();

    gf.expect_and_perform(Some(spell), &format!("Cast {} (8)", spell_name_2));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish");
    gf.advance_to_next_option();

    assert!(!gf.g.find_zone(Stack).is_empty());

    gf.pass_priority_then_advance(); // Player 0
    gf.pass_priority_then_advance(); // Player 1

    // Topmost spell
    assert!(!gf.g.find_zone(Stack).is_empty());
    assert_eq!(gf.g.players[0].life, 22);
    assert_eq!(gf.get_game_state(), (Main, 0, 0));

    gf.pass_priority_then_advance(); // Player 0
    gf.pass_priority_then_advance(); // Player 1

    // Bottom spell
    assert!(gf.g.find_zone(Stack).is_empty());
    assert_eq!(gf.g.players[0].life, 23);
    assert_eq!(gf.get_game_state(), (Main, 0, 0));
}

#[test]
fn stack_no_sorcery() {
    let mut of = OracleFrame::new();
    let instant_name = of.add_instant(vec![], vec![]);
    let sorc_name = of.add_sorcery(vec![], vec![]);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();

    let spell = gf.g.insert_by_name(Hand(0), &sorc_name, 0).clone();

    gf.expect(Some(spell.clone()), &format!("Cast {} (8)", sorc_name));
    gf.put_cast(&instant_name);
    gf.expect_not(Some(spell), &format!("Cast {} (8)", sorc_name));
}

#[test]
fn step_sorcery_casting() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_sorcery(vec![], vec![]);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let spell = gf.g.insert_by_name(Hand(0), &spell_name, 0).clone();

    gf.advance_to_next_option();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 0, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Draw, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Draw, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // BoC, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // BoC, 0, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // DA, 0, 0
    gf.decline_attackers();
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // DA, 0, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // EoC, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // EoC, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // End, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // End, 0, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 1, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 1, 0
}

#[test]
fn step_instant_casting() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(vec![], vec![]);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let spell = gf.g.insert_by_name(Hand(0), &spell_name, 0).clone();

    gf.advance_to_next_option();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Draw, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Draw, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // BoC, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // BoC, 0, 1
    gf.pass_priority_then_advance();
    gf.decline_attackers();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // DA, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // DA, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // EoC, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // EoC, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Main, 0, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // End, 0, 0
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // End, 0, 1
    gf.pass_priority_then_advance();
    gf.expect_not(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 1, 1
    gf.pass_priority_then_advance();
    gf.expect(Some(spell.clone()), &format!("Cast {} (7)", spell_name)); // Upkeep, 1, 0
}

#[test]
fn cast_choices() {
    let mut of = OracleFrame::new();
    let spell_name = "Test Card".to_owned();
    of.o.insert(Card {
        name: spell_name.clone(),
        types: vec![CardType::Instant],
        cast_choices: vec![Box::new(GainLifeAction::new(
            vec![ControllerOf(Selectable::Placeholder(This))],
            vec![Literal(Selectable::Value(1))],
        ))],
        resolution_effects: vec![Box::new(GainLifeAction::new(
            vec![ControllerOf(Selectable::Placeholder(This))],
            vec![Literal(Selectable::Value(2))],
        ))],
        ..Default::default()
    });
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    let spell = gf.g.insert_by_name(Hand(0), &spell_name, 0).clone();

    assert_eq!(gf.g.players[0].life, 20);

    // Cast spell
    gf.expect_and_perform(Some(spell), &format!("Cast {} (8)", spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish");
    gf.advance_to_next_option();

    assert!(!gf.g.find_zone(Stack).is_empty());
    assert_eq!(gf.g.players[0].life, 21);

    gf.pass_priority_then_advance(); // Player 0
    gf.pass_priority_then_advance(); // Player 1

    assert!(gf.g.find_zone(Stack).is_empty());
    assert_eq!(gf.g.players[0].life, 23);
}
