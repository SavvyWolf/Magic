#[cfg(test)]
use super::*;
use cards::mana::Colour::*;
use cards::mana::Mana;
use filter::Filter::*;
use filter::Placeholder::This;
use filter::Selectable;
use filter::SelectableKind;
use gamestate::phases::Step::*;
use gamestate::ManaPool;
use gamestate::ZoneType::*;
use parse::savmtg;
use rules::*;

use std::ops::Deref;

fn get_test_mana() -> Vec<Mana> {
    vec![
        Mana::new_unconditional(Blue),
        Mana::new_unconditional(White),
        Mana::new_unconditional(Blue),
    ]
}

fn assert_has_test_mana(pool: &ManaPool) {
    assert_eq!(pool.get(Mana::new_unconditional(Blue)), 2);
    assert_eq!(pool.get(Mana::new_unconditional(White)), 1);
}

#[test]
fn single() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(AddManaAction::new(
            get_test_mana(),
            vec![ControllerOf(Selectable::Placeholder(This))],
        ))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert_has_test_mana(&gf.g.players[0].mana_pool);
    assert!(gf.g.players[1].mana_pool.is_empty());
}

#[test]
fn none() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(AddManaAction::new(get_test_mana(), vec![Empty]))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());
}

#[test]
fn multiple() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(AddManaAction::new(
            get_test_mana(),
            vec![IsKind(SelectableKind::Player)],
        ))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert_has_test_mana(&gf.g.players[0].mana_pool);
    assert_has_test_mana(&gf.g.players[1].mana_pool);
}

#[test]
fn description() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(AddManaAction::new(
            get_test_mana(),
            vec![ControllerOf(Selectable::Placeholder(This))],
        ))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    gf.put_cast(&spell_name);

    let card = &gf.g.find_zone(Stack)[0];
    assert_eq!(
        card.borrow().card.resolution_effects[0].description(&gf.g, Some(card), false),
        "Add {U}{W}{U}"
    );
}

#[test]
fn json() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(AddManaAction::new(
            get_test_mana(),
            vec![ControllerOf(Selectable::Placeholder(This))],
        ))],
    );
    let old = of.o.get_card_by_name(&spell_name).unwrap();

    let json = savmtg::dump_string(&[old.deref().clone()]).unwrap();
    let new = savmtg::load_str(&json).unwrap()[0].clone();

    assert!(!new.resolution_effects.is_empty());

    let mut of = OracleFrame::new();
    of.o.insert(new);

    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert_has_test_mana(&gf.g.players[0].mana_pool);
    assert!(gf.g.players[1].mana_pool.is_empty());
}
