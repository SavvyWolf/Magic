#[cfg(test)]
use super::*;
use filter::Filter::*;
use filter::Placeholder::This;
use filter::Selectable;
use filter::SelectableKind;
use gamestate::phases::Step::*;
use gamestate::ZoneType::*;
use parse::savmtg;
use rules::*;

use std::ops::Deref;

#[test]
fn single() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(DrawCardAction::new(vec![ControllerOf(
            Selectable::Placeholder(This),
        )]))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 8);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
}

#[test]
fn none() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(vec![], vec![Box::new(DrawCardAction::new(vec![Empty]))]);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
}

#[test]
fn multiple() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(DrawCardAction::new(vec![IsKind(
            SelectableKind::Player,
        )]))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 8);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 8);
}

#[test]
fn description() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(DrawCardAction::new(vec![ControllerOf(
            Selectable::Placeholder(This),
        )]))],
    );

    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);
    gf.put_cast(&spell_name);

    let card = &gf.g.find_zone(Stack)[0];
    assert_eq!(
        card.borrow().card.resolution_effects[0].description(&gf.g, Some(card), false),
        "Player 0 draws a card"
    );
}

#[test]
fn json() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_instant(
        vec![],
        vec![Box::new(DrawCardAction::new(vec![ControllerOf(
            Selectable::Placeholder(This),
        )]))],
    );
    let old = of.o.get_card_by_name(&spell_name).unwrap();

    let json = savmtg::dump_string(&[old.deref().clone()]).unwrap();
    let new = savmtg::load_str(&json).unwrap()[0].clone();

    assert!(!new.resolution_effects.is_empty());

    let mut of = OracleFrame::new();
    of.o.insert(new);

    let mut gf = GameFrame::new(2, of);
    gf.g.begin();
    gf.advance_to_given_state(Upkeep, 0, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);

    gf.put_cast(&spell_name);
    gf.resolve_all();

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 8);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
}
