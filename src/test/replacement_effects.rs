#[cfg(test)]
use super::*;
use actions::ActionHookType;
use filter::Filter::*;
use filter::MathFn;
use filter::Placeholder::*;
use filter::Selectable;
use gamestate::phases::Step::*;
use gamestate::ZoneType::*;
use rules::*;

#[test]
fn basic_support() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_enchantment(
        vec![],
        vec![Box::new(ReplacementEffect::new(
            vec![],
            ActionHookType::Draw,
            false,
            vec![Box::new(GainLifeAction::new(
                vec![Is(Selectable::Template(PPlayer))],
                vec![Literal(Selectable::Value(3))],
            ))],
        ))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.g.insert_by_name(Battlefield, &spell_name, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 20);
    assert_eq!(gf.g.players[1].life, 20);

    gf.advance_to_given_state(Draw, 0, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 23);
    assert_eq!(gf.g.players[1].life, 20);

    gf.advance_to_given_state(Draw, 1, 1);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 23);
    assert_eq!(gf.g.players[1].life, 23);
}

#[test]
fn filter() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_enchantment(
        vec![],
        vec![Box::new(ReplacementEffect::new(
            vec![ControllerOf(Selectable::Placeholder(This))],
            ActionHookType::Draw,
            false,
            vec![Box::new(GainLifeAction::new(
                vec![Is(Selectable::Template(PPlayer))],
                vec![Literal(Selectable::Value(3))],
            ))],
        ))],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.g.insert_by_name(Battlefield, &spell_name, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 20);
    assert_eq!(gf.g.players[1].life, 20);

    gf.advance_to_given_state(Draw, 0, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 23);
    assert_eq!(gf.g.players[1].life, 20);

    gf.advance_to_given_state(Draw, 1, 1);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 8);
    assert_eq!(gf.g.players[0].life, 23);
    assert_eq!(gf.g.players[1].life, 20);
}

#[test]
fn multiple() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_enchantment(
        vec![],
        vec![
            Box::new(ReplacementEffect::new(
                vec![],
                ActionHookType::Draw,
                false,
                vec![Box::new(GainLifeAction::new(
                    vec![Is(Selectable::Template(PPlayer))],
                    vec![Literal(Selectable::Value(3))],
                ))],
            )),
            Box::new(ReplacementEffect::new(
                vec![Bind(A)],
                ActionHookType::GainLife,
                false,
                vec![Box::new(GainLifeAction::new(
                    vec![Is(Selectable::Template(PPlayer))],
                    vec![
                        Literal(Selectable::Template(PLife)),
                        Operation(MathFn::Add, Selectable::Value(3)),
                    ],
                ))],
            )),
        ],
    );
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.g.insert_by_name(Battlefield, &spell_name, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 20);
    assert_eq!(gf.g.players[1].life, 20);

    gf.advance_to_given_state(Draw, 0, 0);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 26);
    assert_eq!(gf.g.players[1].life, 20);

    gf.advance_to_given_state(Draw, 1, 1);

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    assert_eq!(gf.g.players[0].life, 26);
    assert_eq!(gf.g.players[1].life, 26);
}
