#[cfg(test)]
use super::*;
use cards::mana::Colour;
use cards::mana::Colour::*;
use cards::mana::CostEntry::*;
use cards::mana::Mana;
use cards::properties::{CardSubtype, CardType};
use cards::Card;
use gamestate::ManaPool;
use gamestate::ZoneType::*;
use utils::{can_tap_for, could_pay_from_board, could_pay_from_pool};

fn m(c: Colour) -> Mana {
    Mana::new_unconditional(c)
}

fn snow(c: Colour) -> Mana {
    Mana::new_snow(c)
}

#[test]
fn pool_coloured() {
    assert!(could_pay_from_pool(
        &[Basic(White)],
        ManaPool::from(&[m(White)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[Basic(White)],
        ManaPool::from(&[m(Red)]),
        20
    ));

    assert!(could_pay_from_pool(
        &[Basic(Colorless)],
        ManaPool::from(&[m(Colorless)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[Basic(Colorless)],
        ManaPool::from(&[m(White)]),
        20
    ));
}

#[test]
fn pool_multiple() {
    assert!(could_pay_from_pool(
        &[Basic(White), Basic(White), Basic(White)],
        ManaPool::from(&[m(White), m(White), m(White)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[Basic(White), Basic(White), Basic(White)],
        ManaPool::from(&[m(White)]),
        20
    ));

    assert!(could_pay_from_pool(
        &[Basic(White), Basic(Blue), Basic(Red)],
        ManaPool::from(&[m(Red), m(Blue), m(White)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[Basic(White), Basic(Blue), Basic(Red)],
        ManaPool::from(&[m(White)]),
        20
    ));
}

#[test]
fn pool_hybrid() {
    assert!(could_pay_from_pool(
        &[Hybrid(White, Green)],
        ManaPool::from(&[m(White)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[Hybrid(White, Green)],
        ManaPool::from(&[m(Green)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[Hybrid(White, Green)],
        ManaPool::from(&[m(Red)]),
        20
    ));

    assert!(could_pay_from_pool(
        &[Hybrid(White, Green), Hybrid(Green, Red)],
        ManaPool::from(&[m(White), m(Green)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[Hybrid(White, Green), Hybrid(Green, Red)],
        ManaPool::from(&[m(White), m(Red)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[Hybrid(White, Green), Hybrid(Green, Red)],
        ManaPool::from(&[m(Green), m(Green)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[Hybrid(White, Green), Hybrid(Green, Red)],
        ManaPool::from(&[m(Green), m(Red)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[Hybrid(White, Green), Hybrid(Green, Red)],
        ManaPool::from(&[m(Green), m(Green)]),
        20
    ));

    assert!(!could_pay_from_pool(
        &[Hybrid(White, Green), Hybrid(Green, Red)],
        ManaPool::from(&[m(Blue), m(Red)]),
        20
    ));
}

#[test]
fn pool_generic_hybrid() {
    assert!(could_pay_from_pool(
        &[GenericHybrid(White)],
        ManaPool::from(&[m(White)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[GenericHybrid(White)],
        ManaPool::from(&[m(Green), m(Green)]),
        20
    ));

    assert!(could_pay_from_pool(
        &[GenericHybrid(White), GenericHybrid(White)],
        ManaPool::from(&[m(Green), m(Green), m(Green), m(Green)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[GenericHybrid(White), GenericHybrid(White)],
        ManaPool::from(&[m(White), m(Green), m(Green)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[GenericHybrid(White), GenericHybrid(White)],
        ManaPool::from(&[m(White), m(White)]),
        20
    ));

    assert!(!could_pay_from_pool(
        &[GenericHybrid(White)],
        ManaPool::from(&[m(Green)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[GenericHybrid(White), GenericHybrid(White)],
        ManaPool::from(&[m(Green), m(Green)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[GenericHybrid(White), GenericHybrid(White)],
        ManaPool::from(&[m(Green), m(Green), m(Green)]),
        20
    ));
    assert!(!could_pay_from_pool(
        &[GenericHybrid(White), GenericHybrid(White)],
        ManaPool::from(&[m(White), m(Green)]),
        20
    ));
}

#[test]
fn pool_phyrexian() {
    assert!(could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[]),
        2
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[]),
        1
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[]),
        0
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[]),
        -20
    ));

    assert!(could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[]),
        4
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[]),
        3
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[]),
        0
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[]),
        -20
    ));

    assert!(could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        2
    ));
    assert!(could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        1
    ));
    assert!(could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        0
    ));
    assert!(could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        -20
    ));

    assert!(could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(White)]),
        2
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(White)]),
        1
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(White)]),
        0
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black)],
        ManaPool::from(&[m(White)]),
        -20
    ));

    assert!(could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        2
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        1
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        0
    ));
    assert!(!could_pay_from_pool(
        &[Phyrexian(Black), Phyrexian(Black)],
        ManaPool::from(&[m(Black)]),
        -20
    ));
}

#[test]
fn pool_generic() {
    assert!(could_pay_from_pool(&[Generic(0)], ManaPool::from(&[]), 20));
    assert!(could_pay_from_pool(
        &[Generic(1)],
        ManaPool::from(&[m(White)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[Generic(2)],
        ManaPool::from(&[m(White), m(White)]),
        20
    ));

    assert!(!could_pay_from_pool(
        &[Generic(3)],
        ManaPool::from(&[m(White), m(White)]),
        20
    ));
}

#[test]
fn pool_x() {
    assert!(could_pay_from_pool(&[X], ManaPool::from(&[]), 20));
    assert!(could_pay_from_pool(&[X], ManaPool::from(&[m(White)]), 20));
}

#[test]
fn pool_snow() {
    assert!(could_pay_from_pool(
        &[Snow],
        ManaPool::from(&[snow(White)]),
        20
    ));
    assert!(could_pay_from_pool(
        &[Basic(White), Snow],
        ManaPool::from(&[snow(White), m(White)]),
        20
    ));

    assert!(!could_pay_from_pool(
        &[Snow],
        ManaPool::from(&[m(White)]),
        20
    ));
}

#[test]
fn plains() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Plains", 0).clone();

    gf.advance_to_next_option();

    assert_eq!(
        can_tap_for(&land, &gf.g),
        vec![vec![Mana::new_unconditional(White)]]
    );
}

#[test]
fn island() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Island", 0).clone();

    gf.advance_to_next_option();

    assert_eq!(
        can_tap_for(&land, &gf.g),
        vec![vec![Mana::new_unconditional(Blue)]]
    );
}

#[test]
fn swamp() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Swamp", 0).clone();

    gf.advance_to_next_option();

    assert_eq!(
        can_tap_for(&land, &gf.g),
        vec![vec![Mana::new_unconditional(Black)]]
    );
}

#[test]
fn mountain() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Mountain", 0).clone();

    gf.advance_to_next_option();

    assert_eq!(
        can_tap_for(&land, &gf.g),
        vec![vec![Mana::new_unconditional(Red)]]
    );
}

#[test]
fn forest() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Forest", 0).clone();

    gf.advance_to_next_option();

    assert_eq!(
        can_tap_for(&land, &gf.g),
        vec![vec![Mana::new_unconditional(Green)]]
    );
}

#[test]
fn multiple() {
    let mut of = OracleFrame::new();
    of.o.insert(Card {
        name: "Land".to_string(),
        types: vec![CardType::Land],
        subtypes: vec![CardSubtype::Plains, CardSubtype::Forest],
        ..Default::default()
    });

    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Land", 0).clone();

    gf.advance_to_next_option();

    assert_eq!(
        can_tap_for(&land, &gf.g),
        vec![
            vec![Mana::new_unconditional(White)],
            vec![Mana::new_unconditional(Green)]
        ]
    );
}

#[test]
fn board() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.g.insert_by_name(Battlefield, "Forest", 0);

    gf.advance_to_next_option();

    assert!(could_pay_from_board(&[Basic(Green)], &gf.g, 0));
    assert!(!could_pay_from_board(
        &[Basic(Green), Basic(Green)],
        &gf.g,
        0
    ));
    assert!(!could_pay_from_board(&[Basic(Red)], &gf.g, 0));
}

#[test]
fn board_tapped() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Forest", 0).clone();

    gf.advance_to_next_option();
    land.borrow_mut().tapped = true;

    assert!(!could_pay_from_board(&[Basic(Green)], &gf.g, 0));
}

#[test]
fn board_and_pool() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.g.insert_by_name(Battlefield, "Forest", 0);

    gf.advance_to_next_option();
    gf.g.players[0]
        .mana_pool
        .insert(Mana::new_unconditional(Green));

    assert!(could_pay_from_board(&[Generic(1), Basic(Green)], &gf.g, 0));
}

#[test]
fn board_multi() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.g.insert_by_name(Battlefield, "Forest", 0);
    gf.g.insert_by_name(Battlefield, "Forest", 0);
    gf.g.insert_by_name(Battlefield, "Forest", 0);
    gf.g.insert_by_name(Battlefield, "Forest", 0);

    gf.advance_to_next_option();

    assert!(could_pay_from_board(&[Generic(3), Basic(Green)], &gf.g, 0));
}

#[test]
fn board_options() {
    let mut of = OracleFrame::new();
    of.o.insert(Card {
        name: "Land".to_string(),
        types: vec![CardType::Land],
        subtypes: vec![CardSubtype::Plains, CardSubtype::Forest],
        ..Default::default()
    });
    of.o.insert(Card {
        name: "Land 2".to_string(),
        types: vec![CardType::Land],
        subtypes: vec![CardSubtype::Plains, CardSubtype::Mountain],
        ..Default::default()
    });

    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.g.insert_by_name(Battlefield, "Land", 0);
    gf.g.insert_by_name(Battlefield, "Land 2", 0);

    gf.advance_to_next_option();

    assert!(could_pay_from_board(&[Basic(Green)], &gf.g, 0));
    assert!(could_pay_from_board(&[Basic(White)], &gf.g, 0));
    assert!(could_pay_from_board(&[Basic(Green), Basic(Red)], &gf.g, 0));
    assert!(could_pay_from_board(
        &[Basic(Green), Basic(White)],
        &gf.g,
        0
    ));

    assert!(!could_pay_from_board(
        &[Basic(White), Basic(Green), Basic(White)],
        &gf.g,
        0
    ));
    assert!(!could_pay_from_board(&[Basic(Red), Basic(Red)], &gf.g, 0));
}
