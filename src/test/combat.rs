#[cfg(test)]
use super::*;
use gamestate::phases::Step::*;
use gamestate::AttackTarget;
use gamestate::ZoneType::*;

#[test]
fn declare_attacks() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Cast the creature spell
    gf.advance_to_given_state(Main, 0, 0);
    gf.put_cast(&spell_name);
    gf.resolve_all();

    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Summoning sick, can't attack
    gf.expect_not_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.expect_not_no_obj(&format!("{} (0) -> Player 0", spell_name).to_owned());
    gf.decline_attackers();

    // Next turn
    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Check can only attack player 1
    gf.expect_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.expect_not_no_obj(&format!("{} (0) -> Player 0", spell_name).to_owned());
}

#[test]
fn declare_blockers() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Cast the creature spell of the attacker
    gf.advance_to_given_state(Main, 0, 0);
    gf.put_cast(&spell_name);
    gf.resolve_all();

    // And the blocker
    gf.put_cast_opponent(&spell_name);
    gf.resolve_all();

    assert_eq!(gf.g.battlefield[0].borrow().owner, 1);
    assert_eq!(gf.g.battlefield[1].borrow().owner, 0);

    // Next turn
    gf.advance_to_given_state(Upkeep, 0, 0);
    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");
    gf.advance_to_next_option();

    // Pass priority to get to blockers
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Check blockers
    gf.expect_no_obj(&format!("{} (0) -> {} (1)", spell_name, spell_name).to_owned());
    gf.expect_not_no_obj(&format!("{} (0) -> {} (0)", spell_name, spell_name).to_owned());
}

#[test]
fn attacking_state() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Cast the creature spell
    gf.advance_to_given_state(Main, 0, 0);
    gf.put_cast(&spell_name);
    gf.resolve_all();

    // Skip to the next turn so we can attack
    gf.advance_to_given_state(Upkeep, 0, 0);
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    let creature = gf.get_battlefield(&spell_name);

    assert_eq!(creature.borrow().attacking, AttackTarget::None);

    // Attack with the creature
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");
    gf.advance_to_next_option();
    gf.advance_to_given_state(DeclareBlockers, 0, 0);

    // Is it attacking?
    assert_eq!(creature.borrow().attacking, AttackTarget::Player(1));

    // Is it removed from combat?
    gf.advance_to_given_state(Main, 0, 0);
    assert_eq!(creature.borrow().attacking, AttackTarget::None);
}

#[test]
fn attacking_phases() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Cast the creature spell
    gf.advance_to_given_state(Main, 0, 0);
    gf.put_cast(&spell_name);
    gf.resolve_all();

    // Skip to the next turn so we can attack
    gf.advance_to_given_state(Upkeep, 0, 0);
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Check can only attack player 1
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");
    gf.advance_to_next_option();

    // Check that combat phases work
    assert_eq!(gf.get_game_state(), (DeclareAttackers, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (DeclareAttackers, 0, 1));
    gf.pass_priority_then_advance();
    gf.decline_blockers();
    assert_eq!(gf.get_game_state(), (DeclareBlockers, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (DeclareBlockers, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (CombatDamage, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (CombatDamage, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (EndOfCombat, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (EndOfCombat, 0, 1));
}

#[test]
fn multi_block() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Load the creatures
    let attacker = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    let blocker_1 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();
    let blocker_2 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();
    let blocker_3 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();

    // Go to attack
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack with the creature
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");
    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    assert!(attacker.borrow().attack_assignment_order.is_empty());

    // Block
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (1)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (2)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (3)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish declaring blockers");

    // Damage assignment order
    assert!(attacker.borrow().attack_assignment_order.is_empty());
    assert!(blocker_1.borrow().attack_assignment_order.is_empty());
    assert!(blocker_2.borrow().attack_assignment_order.is_empty());
    assert!(blocker_3.borrow().attack_assignment_order.is_empty());

    // First 1
    gf.advance_to_next_option();
    gf.expect_not(Some(attacker.clone()), &format!("{} (0)", spell_name));
    gf.expect(Some(attacker.clone()), &format!("{} (2)", spell_name));
    gf.expect(Some(attacker.clone()), &format!("{} (3)", spell_name));
    gf.expect_and_perform(Some(attacker.clone()), &format!("{} (1)", spell_name));
    gf.advance_to_next_option();

    assert!(blocker_1.borrow().attack_assignment_order.is_empty());
    assert!(blocker_2.borrow().attack_assignment_order.is_empty());
    assert!(!attacker.borrow().attack_assignment_order.is_empty());
    assert_eq!(attacker.borrow().attack_assignment_order, [1]);

    // Then 2
    gf.expect_not(Some(attacker.clone()), &format!("{} (0)", spell_name));
    gf.expect_not(Some(attacker.clone()), &format!("{} (1)", spell_name));
    gf.expect_and_perform(Some(attacker.clone()), &format!("{} (2)", spell_name));
    gf.advance_to_next_option();

    assert!(blocker_1.borrow().attack_assignment_order.is_empty());
    assert!(blocker_2.borrow().attack_assignment_order.is_empty());
    assert!(!attacker.borrow().attack_assignment_order.is_empty());
    assert_eq!(attacker.borrow().attack_assignment_order, [1, 2]);

    // Then 3
    gf.expect_not(Some(attacker.clone()), &format!("{} (0)", spell_name));
    gf.expect_not(Some(attacker.clone()), &format!("{} (1)", spell_name));
    gf.expect_not(Some(attacker.clone()), &format!("{} (2)", spell_name));
    gf.expect_and_perform(Some(attacker.clone()), &format!("{} (3)", spell_name));
    gf.advance_to_next_option();

    assert!(blocker_1.borrow().attack_assignment_order.is_empty());
    assert!(blocker_2.borrow().attack_assignment_order.is_empty());
    assert!(!attacker.borrow().attack_assignment_order.is_empty());
    assert_eq!(attacker.borrow().attack_assignment_order, [1, 2, 3]);

    gf.advance_to_next_option();
    gf.expect_no_obj("Pass priority");
}

#[test]
fn no_blocks() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Load the creatures
    let attacker = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    let blocker_1 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();
    let blocker_2 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();

    // Go to attack
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack with the creature
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");
    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    assert!(attacker.borrow().attack_assignment_order.is_empty());

    // Don't block
    gf.expect_and_perform_no_obj(&"Finish declaring blockers");

    // Damage assignment order
    assert!(attacker.borrow().attack_assignment_order.is_empty());
    assert!(blocker_1.borrow().attack_assignment_order.is_empty());
    assert!(blocker_2.borrow().attack_assignment_order.is_empty());

    gf.advance_to_next_option();
    gf.expect_no_obj("Pass priority");
}

#[test]
fn one_multi_one_not() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Load the creatures
    let attacker_1 = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    let attacker_2 = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    gf.g.insert_by_name(Battlefield, &spell_name, 1);
    gf.g.insert_by_name(Battlefield, &spell_name, 1);

    // Go to attack
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack with the creatures
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (1) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");

    // Go to declare blockers
    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Block
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (2)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (3)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish declaring blockers");

    // Damage assignment order
    assert!(attacker_1.borrow().attack_assignment_order.is_empty());
    assert!(attacker_2.borrow().attack_assignment_order.is_empty());

    // Blocks
    gf.advance_to_next_option();
    gf.expect_not(Some(attacker_1.clone()), &format!("{} (0)", spell_name));
    gf.expect_not(Some(attacker_2.clone()), &format!("{} (1)", spell_name));
    gf.expect_and_perform(Some(attacker_1.clone()), &format!("{} (2)", spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(attacker_1.clone()), &format!("{} (3)", spell_name));
    gf.advance_to_next_option();

    assert!(!attacker_1.borrow().attack_assignment_order.is_empty());
    assert!(attacker_2.borrow().attack_assignment_order.is_empty());

    gf.advance_to_next_option();
    gf.expect_no_obj("Pass priority");
}

#[test]
fn two_blocks() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Load the creatures
    let attacker_1 = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    let attacker_2 = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    gf.g.insert_by_name(Battlefield, &spell_name, 1);
    gf.g.insert_by_name(Battlefield, &spell_name, 1);

    // Go to attack
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack with the creatures
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (1) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");

    // Go to declare blockers
    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Block
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (2)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (1) -> {} (3)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish declaring blockers");

    // Damage assignment order
    assert!(attacker_1.borrow().attack_assignment_order.is_empty());
    assert!(attacker_2.borrow().attack_assignment_order.is_empty());

    // Blocks
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(attacker_2.clone()), &format!("{} (3)", spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(attacker_1.clone()), &format!("{} (2)", spell_name));
    gf.advance_to_next_option();

    assert!(!attacker_1.borrow().attack_assignment_order.is_empty());
    assert!(!attacker_2.borrow().attack_assignment_order.is_empty());
    assert_eq!(attacker_1.borrow().attack_assignment_order, [2]);
    assert_eq!(attacker_2.borrow().attack_assignment_order, [3]);

    gf.advance_to_next_option();
    gf.expect_no_obj("Pass priority");
}

#[test]
fn damage_assignment() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature_pt(2, 1);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Load the creatures
    let attacker = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    let blocker_1 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();
    let blocker_2 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();

    // Go to attack
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack with the creatures
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");

    // Go to declare blockers
    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Block
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (1)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (2)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish declaring blockers");

    // Block order
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(attacker.clone()), &format!("{} (2)", spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(attacker.clone()), &format!("{} (1)", spell_name));
    gf.advance_to_next_option();

    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Damage assignment
    assert_eq!(gf.get_range(), (1, 3));
    gf.use_range(1);
    gf.advance_to_next_option();

    assert_eq!(attacker.borrow().attack_damage_remaining, 0);
    assert_eq!(attacker.borrow().attack_damage_values, vec![1, 1]);
    assert_eq!(attacker.borrow().marked_damage, 4);
    assert_eq!(blocker_1.borrow().marked_damage, 1);
    assert_eq!(blocker_2.borrow().marked_damage, 1);
}

#[test]
fn damage_assignment_all_first() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature_pt(2, 1);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Load the creatures
    let attacker = gf.g.insert_by_name(Battlefield, &spell_name, 0).clone();
    let blocker_1 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();
    let blocker_2 = gf.g.insert_by_name(Battlefield, &spell_name, 1).clone();

    // Go to attack
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack with the creatures
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");

    // Go to declare blockers
    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Block
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (1)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (0) -> {} (2)", spell_name, spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish declaring blockers");

    // Block order
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(attacker.clone()), &format!("{} (2)", spell_name));
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(attacker.clone()), &format!("{} (1)", spell_name));
    gf.advance_to_next_option();

    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Damage assignment
    assert_eq!(gf.get_range(), (1, 3));
    gf.use_range(2);
    gf.advance_to_next_option();

    assert_eq!(attacker.borrow().attack_damage_remaining, 0);
    assert_eq!(attacker.borrow().attack_damage_values, vec![2, 0]);
    assert_eq!(attacker.borrow().marked_damage, 4);
    assert_eq!(blocker_1.borrow().marked_damage, 0);
    assert_eq!(blocker_2.borrow().marked_damage, 2);
}

#[test]
fn player_damage() {
    let mut of = OracleFrame::new();
    let spell_name = of.add_vanilla_creature_pt(2, 1);
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    // Load the creatures
    gf.g.insert_by_name(Battlefield, &spell_name, 0);
    gf.g.insert_by_name(Battlefield, &spell_name, 0);

    // Go to attack
    gf.advance_to_given_state(DeclareAttackers, 0, 0);

    // Attack with the creatures
    gf.expect_and_perform_no_obj(&format!("{} (0) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&format!("{} (1) -> Player 1", spell_name).to_owned());
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj("Finish declaring attackers");

    // Go to declare blockers
    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Block
    gf.advance_to_next_option();
    gf.expect_and_perform_no_obj(&"Finish declaring blockers");

    gf.advance_to_next_option();
    gf.pass_priority_then_advance();
    gf.pass_priority_then_advance();

    // Damage assignment (none)
    gf.advance_to_next_option();

    assert_eq!(gf.g.players[0].life, 20);
    assert_eq!(gf.g.players[1].life, 16);
}
