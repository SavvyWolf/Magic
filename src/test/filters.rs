#[cfg(test)]
use super::*;
use cards::properties::{CardSubtype, CardSupertype, CardType};
use cards::Card;
use filter::Filter::*;
use filter::Placeholder::*;
use filter::Selectable::*;
use filter::{
    filter_to_json, json_to_filter, run_filter_on, sstype, FilterList, MathFn, PlaceholderData,
    Selectable, SelectableKind, SelectionSet,
};
use gamestate::ZoneType;

fn ms(vec: &[Selectable]) -> SelectionSet<sstype::Resolved> {
    vec.iter().cloned().collect()
}

#[test]
fn is_kind() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let creature = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();

    let set = vec![GameObject(creature), Player(0), Value(42)];

    assert_eq!(
        run_filter_on(
            &[IsKind(SelectableKind::GameObject)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[0].clone()])
    );
    assert_eq!(
        run_filter_on(
            &[IsKind(SelectableKind::Player)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
    assert_eq!(
        run_filter_on(
            &[IsKind(SelectableKind::Value)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[2].clone()])
    );
}

#[test]
fn has_type() {
    let mut of = OracleFrame::new();
    of.o.insert(Card {
        name: "Has".to_owned(),
        types: vec![CardType::Land],
        ..Default::default()
    });
    of.o.insert(Card {
        name: "Has Not".to_owned(),
        types: vec![CardType::Creature],
        ..Default::default()
    });
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let with_type =
        gf.g.insert_by_name(ZoneType::Battlefield, &"Has", 0)
            .clone();
    let without_type =
        gf.g.insert_by_name(ZoneType::Battlefield, &"Has Not", 0)
            .clone();

    let set = vec![Player(0), GameObject(with_type), GameObject(without_type)];

    assert_eq!(
        run_filter_on(
            &[HasType(CardType::Land)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn has_subtype() {
    let mut of = OracleFrame::new();
    of.o.insert(Card {
        name: "Has".to_owned(),
        subtypes: vec![CardSubtype::Cat],
        types: vec![CardType::Creature],
        ..Default::default()
    });
    of.o.insert(Card {
        name: "Has Not".to_owned(),
        subtypes: vec![CardSubtype::Wolf],
        types: vec![CardType::Creature],
        ..Default::default()
    });
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let with_type =
        gf.g.insert_by_name(ZoneType::Battlefield, &"Has", 0)
            .clone();
    let without_type =
        gf.g.insert_by_name(ZoneType::Battlefield, &"Has Not", 0)
            .clone();

    let set = vec![Player(0), GameObject(with_type), GameObject(without_type)];

    assert_eq!(
        run_filter_on(
            &[HasSubtype(CardSubtype::Cat)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn has_supertype() {
    let mut of = OracleFrame::new();
    of.o.insert(Card {
        name: "Has".to_owned(),
        supertypes: vec![CardSupertype::Legendary],
        types: vec![CardType::Creature],
        ..Default::default()
    });
    of.o.insert(Card {
        name: "Has Not".to_owned(),
        supertypes: vec![CardSupertype::Snow],
        types: vec![CardType::Creature],
        ..Default::default()
    });
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let with_type =
        gf.g.insert_by_name(ZoneType::Battlefield, &"Has", 0)
            .clone();
    let without_type =
        gf.g.insert_by_name(ZoneType::Battlefield, &"Has Not", 0)
            .clone();

    let set = vec![Player(0), GameObject(with_type), GameObject(without_type)];

    assert_eq!(
        run_filter_on(
            &[HasSupertype(CardSupertype::Legendary)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn in_zone() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let creature = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();
    let card = gf.g.insert_by_name(ZoneType::Hand(0), &vc, 0).clone();

    let set = vec![Player(0), GameObject(creature), GameObject(card)];

    assert_eq!(
        run_filter_on(
            &[InZone(ZoneType::Battlefield)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
    assert_eq!(
        run_filter_on(
            &[InZone(ZoneType::Hand(0))],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[2].clone()])
    );
}

#[test]
fn tapped() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let tapped = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();
    tapped.borrow_mut().tapped = true;
    let untapped = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();

    let set = vec![Player(0), GameObject(tapped), GameObject(untapped)];

    assert_eq!(
        run_filter_on(&[Tapped], &gf.g, None, ms(&set), Default::default()).0,
        ms(&[set[1].clone()])
    );
    assert_eq!(
        run_filter_on(&[Untapped], &gf.g, None, ms(&set), Default::default()).0,
        ms(&[set[2].clone()])
    );
}

#[test]
fn is() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let a = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();
    let b = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();

    let set = vec![Player(0), GameObject(a.clone()), GameObject(b.clone())];

    assert_eq!(
        run_filter_on(
            &[Is(Selectable::Placeholder(This))],
            &gf.g,
            Some(&a),
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );

    let mut data: PlaceholderData = Default::default();
    data.insert(A, SelectionSet::new_gameobject(a));
    data.insert(B, SelectionSet::new_gameobject(b));
    assert_eq!(
        run_filter_on(
            &[Is(Selectable::Placeholder(A))],
            &gf.g,
            None,
            ms(&set),
            data
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn is_absolute_player() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let set = vec![Value(42), Player(0), Player(1)];

    assert_eq!(
        run_filter_on(
            &[IsAbsolutePlayer(0)],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn is_player() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let set = vec![Value(42), Player(0), Player(1)];

    assert_eq!(
        run_filter_on(
            &[Is(Selectable::Player(0))],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
    let mut data: PlaceholderData = Default::default();
    data.insert(A, SelectionSet::new_player(0));
    data.insert(B, SelectionSet::new_player(1));
    assert_eq!(
        run_filter_on(
            &[Is(Selectable::Placeholder(A))],
            &gf.g,
            None,
            ms(&set),
            data
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn controller() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let p0 = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();
    let p1 = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 1).clone();

    let set = vec![Player(0), GameObject(p0), GameObject(p1)];

    assert_eq!(
        run_filter_on(
            &[Controller(Selectable::Player(0))],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn controller_of() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let p0 = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();
    let p1 = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 1).clone();

    let set = vec![Player(0), Player(1), GameObject(p0.clone()), GameObject(p1)];

    assert_eq!(
        run_filter_on(
            &[ControllerOf(Selectable::GameObject(p0))],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[Player(0)])
    );
}

#[test]
fn bind() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let p0 = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();
    let p1 = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 1).clone();

    let set = vec![Player(0), GameObject(p0.clone()), GameObject(p1)];

    let output = run_filter_on(
        &[Controller(Selectable::Player(0)), Bind(A)],
        &gf.g,
        None,
        ms(&set),
        Default::default(),
    )
    .1;

    assert_eq!(
        *output[&A].only().unwrap_gameobject().borrow(),
        *p0.borrow()
    );
}

#[test]
fn literal() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let set = vec![];

    assert_eq!(
        run_filter_on(
            &[Literal(Selectable::Value(42))],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[Value(42)])
    );
}

#[test]
fn operation() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let set = vec![Player(0), Value(1), Value(2)];

    assert_eq!(
        run_filter_on(
            &[Operation(MathFn::Add, Selectable::Value(10))],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[Value(11), Value(12)])
    );
}

#[test]
fn empty() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let set = vec![Player(0), Value(1), Value(2)];

    assert_eq!(
        run_filter_on(&[Empty], &gf.g, None, ms(&set), Default::default()).0,
        ms(&[])
    );
}

#[test]
fn not() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let set = vec![Player(0), Player(1)];

    assert_eq!(
        run_filter_on(
            &[Not(Box::new(IsAbsolutePlayer(0)))],
            &gf.g,
            None,
            ms(&set),
            Default::default()
        )
        .0,
        ms(&[set[1].clone()])
    );
}

#[test]
fn json() {
    let filters = vec![
        Not(Box::new(Untapped)),
        IsKind(SelectableKind::Player),
        HasType(CardType::Instant),
        HasSubtype(CardSubtype::Cat),
        HasSupertype(CardSupertype::Snow),
        InZone(ZoneType::Stack),
        Untapped,
        Tapped,
        Is(Selectable::Placeholder(This)),
        IsAbsolutePlayer(42),
        Controller(Selectable::Player(1)),
        ControllerOf(Selectable::Placeholder(This)),
        Bind(PLife),
        Literal(Selectable::Value(42)),
        Operation(MathFn::Add, Selectable::Template(This)),
        Empty,
    ];

    let to_json = filter_to_json(&filters);
    let loaded = json_to_filter(&to_json).unwrap();

    assert_eq!(filters, loaded);
}

#[test]
fn template() {
    let mut of = OracleFrame::new();
    let vc = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let creature = gf.g.insert_by_name(ZoneType::Battlefield, &vc, 0).clone();

    let filters_in = vec![
        Controller(Selectable::Template(B)),
        ControllerOf(Selectable::Template(A)),
        Literal(Selectable::Template(C)),
        Operation(MathFn::Add, Selectable::Template(C)),
        Is(Selectable::Template(B)),
    ];
    let filters_check = vec![
        Controller(Selectable::Player(0)),
        ControllerOf(Selectable::GameObject(creature.clone())),
        Literal(Selectable::Value(42)),
        Operation(MathFn::Add, Selectable::Value(42)),
        Is(Selectable::Player(0)),
    ];

    let mut data: PlaceholderData = Default::default();
    data.insert(A, SelectionSet::new_gameobject(creature));
    data.insert(B, SelectionSet::new_player(0));
    data.insert(C, SelectionSet::new_value(42));

    let filters_mapped: FilterList = filters_in
        .iter()
        .cloned()
        .map(|x| x.apply_template(&data))
        .collect();

    assert_eq!(filters_mapped, filters_check);
}
