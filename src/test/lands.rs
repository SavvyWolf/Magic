#[cfg(test)]
use super::*;
use cards::mana::Colour::*;
use cards::mana::Mana;
use gamestate::phases::Step::*;
use gamestate::ZoneType::*;

#[test]
fn play_lands() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Hand(0), "Forest", 0).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    gf.expect_and_perform(Some(land), "Play Forest (7) as a land");
    gf.advance_to_next_option();

    assert!(!gf.g.find_zone(Battlefield).is_empty());
    assert_eq!(gf.g.find_zone(Hand(0)).len(), 8); // Remember card drawn during draw step
}

#[test]
fn one_land_per_turn() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land_a = gf.g.insert_by_name(Hand(0), "Forest", 0).clone();
    let land_b = gf.g.insert_by_name(Hand(0), "Forest", 0).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    gf.expect(Some(land_a.clone()), "Play Forest (7) as a land");
    gf.expect_and_perform(Some(land_b.clone()), "Play Forest (8) as a land");
    gf.advance_to_next_option();
    gf.expect_not(Some(land_a), "Play Forest (7) as a land");
    gf.expect_not(Some(land_b), "Play Forest (8) as a land");
}

#[test]
fn on_turn_only() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Hand(1), "Forest", 1).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    gf.expect_not(Some(land.clone()), "Play Forest (7) as a land");

    gf.advance_to_given_state(Main, 1, 1);
    gf.advance_to_next_option();
    gf.expect(Some(land.clone()), "Play Forest (7) as a land");
}

#[test]
fn tap_plains() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Plains", 0).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    assert!(!land.borrow().tapped);
    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.expect_and_perform(Some(land.clone()), "{T}: Add {W}");
    gf.advance_to_next_option();

    assert!(land.borrow().tapped);
    assert!(!gf.g.players[0].mana_pool.is_empty());
    assert_eq!(
        gf.g.players[0]
            .mana_pool
            .get(Mana::new_unconditional(White)),
        1
    );
    assert!(gf.g.players[1].mana_pool.is_empty());
}

#[test]
fn tap_island() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Island", 0).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    assert!(!land.borrow().tapped);
    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.expect_and_perform(Some(land.clone()), "{T}: Add {U}");
    gf.advance_to_next_option();

    assert!(land.borrow().tapped);
    assert!(!gf.g.players[0].mana_pool.is_empty());
    assert_eq!(
        gf.g.players[0].mana_pool.get(Mana::new_unconditional(Blue)),
        1
    );
    assert!(gf.g.players[1].mana_pool.is_empty());
}

#[test]
fn tap_swamp() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Swamp", 0).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    assert!(!land.borrow().tapped);
    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.expect_and_perform(Some(land.clone()), "{T}: Add {B}");
    gf.advance_to_next_option();

    assert!(land.borrow().tapped);
    assert!(!gf.g.players[0].mana_pool.is_empty());
    assert_eq!(
        gf.g.players[0]
            .mana_pool
            .get(Mana::new_unconditional(Black)),
        1
    );
    assert!(gf.g.players[1].mana_pool.is_empty());
}

#[test]
fn tap_mountain() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Mountain", 0).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    assert!(!land.borrow().tapped);
    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.expect_and_perform(Some(land.clone()), "{T}: Add {R}");
    gf.advance_to_next_option();

    assert!(land.borrow().tapped);
    assert!(!gf.g.players[0].mana_pool.is_empty());
    assert_eq!(
        gf.g.players[0].mana_pool.get(Mana::new_unconditional(Red)),
        1
    );
    assert!(gf.g.players[1].mana_pool.is_empty());
}

#[test]
fn tap_forest() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    let land = gf.g.insert_by_name(Battlefield, "Forest", 0).clone();

    gf.advance_to_given_state(Main, 0, 0);
    gf.advance_to_next_option();
    assert!(!land.borrow().tapped);
    assert!(gf.g.players[0].mana_pool.is_empty());
    assert!(gf.g.players[1].mana_pool.is_empty());

    gf.expect_and_perform(Some(land.clone()), "{T}: Add {G}");
    gf.advance_to_next_option();

    assert!(land.borrow().tapped);
    assert!(!gf.g.players[0].mana_pool.is_empty());
    assert_eq!(
        gf.g.players[0]
            .mana_pool
            .get(Mana::new_unconditional(Green)),
        1
    );
    assert!(gf.g.players[1].mana_pool.is_empty());
}
