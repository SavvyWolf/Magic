use abilities::StaticEffect;
use actions::{Action, ExpectedUserInput, UserInputKind};
use cards::mana::Cost;
use cards::mana::CostEntry::*;
use cards::properties::CardType::*;
use cards::Card;
use cards::Oracle;
use game::Game;
use gamestate::phases::Step;
use gamestate::PlayerId;
use gamestate::RcGameObject;
use gamestate::ZoneType;

use std::path::PathBuf;
use std::sync::atomic::{AtomicUsize, Ordering};

static COUNTER: AtomicUsize = AtomicUsize::new(0);

pub struct GameFrame {
    pub g: Game,
}

impl GameFrame {
    pub fn new(players: usize, of: OracleFrame) -> GameFrame {
        let mut ret = GameFrame {
            g: Game::new_no_teams(2, PathBuf::from("")),
        };
        ret.g.oracle = of.o;
        for i in 0..players {
            ret.g.submit_library(i, vec![(30, "__Blank")]);
        }
        ret
    }

    pub fn get_game_state(&self) -> (Step, PlayerId, PlayerId) {
        (
            self.g.state.step,
            self.g.state.active_player,
            self.g.state.priority,
        )
    }

    pub fn expect(&mut self, obj: Option<RcGameObject>, action: &str) {
        let (_, actions) = self.g.get_game_actions();
        for a in &actions {
            if a.object == obj && a.action.description(&self.g, a.object.as_ref(), false) == action
            {
                return;
            }
        }
        self.print_actions();
        panic!(
            "Expected game action '{}' on '{}' not found!",
            action,
            obj.unwrap().borrow().sig()
        );
    }

    pub fn expect_no_obj(&mut self, action: &str) {
        let (_, actions) = self.g.get_game_actions();
        for a in &actions {
            if a.action.description(&self.g, a.object.as_ref(), false) == action {
                return;
            }
        }
        self.print_actions();
        panic!("Expected game action '{}' not found!", action);
    }

    pub fn expect_not(&mut self, obj: Option<RcGameObject>, action: &str) {
        let (_, actions) = self.g.get_game_actions();
        for a in &actions {
            if a.object == obj && a.action.description(&self.g, a.object.as_ref(), false) == action
            {
                panic!(
                    "Undesired game action '{}' on '{}' was found!",
                    action,
                    obj.unwrap().borrow().sig()
                );
            }
        }
    }

    pub fn expect_not_no_obj(&mut self, action: &str) {
        let (_, actions) = self.g.get_game_actions();
        for a in &actions {
            if a.action.description(&self.g, a.object.as_ref(), false) == action {
                panic!("Undesired game action '{}' was found!", action);
            }
        }
    }

    pub fn expect_and_perform_no_obj(&mut self, action: &str) {
        let (_, actions) = self.g.get_game_actions();
        for a in &actions {
            if a.action.description(&self.g, a.object.as_ref(), false) == action {
                self.g.use_game_action(a.clone());
                return;
            }
        }
        self.print_actions();
        panic!("Expected game action '{}' not found!", action);
    }

    pub fn expect_and_perform(&mut self, obj: Option<RcGameObject>, action: &str) {
        let (_, actions) = self.g.get_game_actions();
        for a in &actions {
            if a.object == obj && a.action.description(&self.g, a.object.as_ref(), false) == action
            {
                self.g.use_game_action(a.clone());
                return;
            }
        }
        self.print_actions();
        panic!(
            "Expected game action '{}' on '{}' not found!",
            action,
            obj.unwrap().borrow().sig()
        );
    }

    pub fn get_range(&self) -> (i32, i32) {
        match self.g.get_input_kind().1 {
            UserInputKind::Range(a, b) => (a, b),
            _ => panic!("Requested range, but input kind is wrong!"),
        }
    }

    pub fn use_range(&mut self, val: i32) {
        self.g.use_range(val);
    }

    pub fn print_actions(&self) {
        let (_, actions) = self.g.get_game_actions();
        eprintln!("Available game actions: ");
        for a in &actions {
            eprintln!(
                "{}",
                a.action.description(&self.g, a.object.as_ref(), false)
            );
        }
    }

    pub fn advance_to_next_option(&mut self) {
        loop {
            match self.g.get_input_kind().1 {
                UserInputKind::ActionChoice => {
                    let (_, actions) = self.g.get_game_actions();
                    if !actions.is_empty() {
                        return;
                    }
                }
                UserInputKind::Range(_, _) => return,
                UserInputKind::None => (),
            }
            self.g.perform_top_action();
        }
    }

    pub fn pass_priority_then_advance(&mut self) {
        self.expect_and_perform_no_obj("Pass priority");
        self.advance_to_next_option();
    }

    pub fn decline_attackers(&mut self) {
        self.advance_to_next_option();
        self.expect_and_perform_no_obj("Finish declaring attackers");
        self.advance_to_next_option();
    }

    pub fn decline_blockers(&mut self) {
        self.advance_to_next_option();
        self.expect_and_perform_no_obj("Finish declaring blockers");
        self.advance_to_next_option();
    }

    pub fn advance_to_given_state(&mut self, step: Step, ap: PlayerId, pp: PlayerId) {
        self.advance_to_next_option();
        while self.get_game_state() != (step, ap, pp) {
            let wanted = self.g.get_expected_user_input().1;
            if wanted == ExpectedUserInput::DeclareAttackers {
                self.decline_attackers();
            } else if wanted == ExpectedUserInput::DeclareBlockers {
                self.decline_blockers();
            } else {
                self.pass_priority_then_advance();
            }
        }
    }

    pub fn put_cast(&mut self, card: &str) -> RcGameObject {
        self.g.insert_by_name(ZoneType::Stack, card, 0).clone()
    }

    pub fn put_cast_opponent(&mut self, card: &str) -> RcGameObject {
        self.g.insert_by_name(ZoneType::Stack, card, 1).clone()
    }

    pub fn resolve_all(&mut self) {
        while !self.g.stack_is_empty() {
            self.pass_priority_then_advance();
        }
    }

    pub fn get_battlefield(&self, name: &str) -> RcGameObject {
        let f: Vec<&RcGameObject> = self
            .g
            .battlefield
            .iter()
            .filter(|x| x.borrow().name == name)
            .collect();
        assert_eq!(
            f.len(),
            1,
            "More or less than 1 objects were found when trying to get an object."
        );
        f[0].clone()
    }
}

pub struct OracleFrame {
    pub o: Oracle,
}

impl OracleFrame {
    pub fn new() -> OracleFrame {
        OracleFrame::new_from(PathBuf::from("cards/"))
    }

    pub fn new_from(path: PathBuf) -> OracleFrame {
        let mut ret = OracleFrame {
            o: Oracle::new(path),
        };
        ret.o.insert(Card {
            name: "__Blank".to_owned(),
            ..Default::default()
        });
        ret
    }

    pub fn add_vanilla_creature(&mut self) -> String {
        let name = "__Vanilla".to_owned();
        self.o.insert(Card {
            name: name.clone(),
            types: vec![Creature],
            cost: vec![Generic(0)],
            toughness: 1,
            ..Default::default()
        });
        name
    }

    pub fn add_vanilla_creature_pt(&mut self, p: i32, t: i32) -> String {
        let name = "__Vanilla".to_owned();
        self.o.insert(Card {
            name: name.clone(),
            types: vec![Creature],
            cost: vec![Generic(0)],
            power: p,
            toughness: t,
            ..Default::default()
        });
        name
    }

    pub fn add_instant(&mut self, cost: Cost, effects: Vec<Box<dyn Action>>) -> String {
        let name = format!("__Instant_{}", COUNTER.fetch_add(1, Ordering::SeqCst)).to_owned();
        self.o.insert(Card {
            name: name.clone(),
            cost,
            types: vec![Instant],
            colours: vec![],
            resolution_effects: effects,
            ..Default::default()
        });
        name
    }

    pub fn add_sorcery(&mut self, cost: Cost, effects: Vec<Box<dyn Action>>) -> String {
        let name = format!("__Sorcery_{}", COUNTER.fetch_add(1, Ordering::SeqCst)).to_owned();
        self.o.insert(Card {
            name: name.clone(),
            cost,
            types: vec![Sorcery],
            colours: vec![],
            resolution_effects: effects,
            ..Default::default()
        });
        name
    }

    pub fn add_enchantment(&mut self, cost: Cost, effects: Vec<Box<dyn StaticEffect>>) -> String {
        let name = format!("__Enchantment_{}", COUNTER.fetch_add(1, Ordering::SeqCst)).to_owned();
        self.o.insert(Card {
            name: name.clone(),
            cost,
            types: vec![Enchantment],
            colours: vec![],
            static_effects: effects,
            ..Default::default()
        });
        name
    }
}
impl Default for OracleFrame {
    fn default() -> Self {
        Self::new()
    }
}
