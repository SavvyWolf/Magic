#[cfg(test)]
use cards::mana::Colour::*;
use cards::mana::CostEntry::*;
use cards::properties::*;

#[test]
fn mana_costs() {
    assert_eq!(format!("{}", Basic(White)), "{W}");
    assert_eq!(format!("{}", Basic(Blue)), "{U}");
    assert_eq!(format!("{}", Basic(Black)), "{B}");
    assert_eq!(format!("{}", Basic(Red)), "{R}");
    assert_eq!(format!("{}", Basic(Green)), "{G}");
    assert_eq!(format!("{}", Basic(Colorless)), "{C}");

    assert_eq!(format!("{}", Hybrid(White, Green)), "{W/G}");
    assert_eq!(format!("{}", GenericHybrid(White)), "{2/W}");
    assert_eq!(format!("{}", Phyrexian(White)), "{W/P}");

    assert_eq!(format!("{}", Generic(0)), "{0}");
    assert_eq!(format!("{}", Generic(2)), "{2}");

    assert_eq!(format!("{}", X), "{X}");
    assert_eq!(format!("{}", Snow), "{S}");
}

#[test]
fn types() {
    assert_eq!(format!("{}", CardType::Land), "Land");
    assert_eq!(format!("{}", CardSubtype::Plains), "Plains");
    assert_eq!(format!("{}", CardSupertype::Basic), "Basic");
}
