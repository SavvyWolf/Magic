#[cfg(test)]
use parse::mtgjson;

use std::path::PathBuf;

#[test]
#[ignore]
fn import() {
    println!("NOTE: This tests expects AllSets.json to be in cards/AllSets.json");

    mtgjson::load(PathBuf::from("cards/AllSets.json")).unwrap();
}

#[test]
#[ignore]
fn import_set() {
    println!("NOTE: This tests expects AllSets.json to be in cards/AllSets.json");

    mtgjson::load_set(PathBuf::from("cards/AllSets.json"), "AKH").unwrap();
}
