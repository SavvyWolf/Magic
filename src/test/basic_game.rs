#[cfg(test)]
use super::*;
use gamestate::phases::Step::*;
use gamestate::ZoneType::*;

#[test]
fn game_created() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    assert_eq!(gf.g.num_players(), 2);
    assert!(gf.g.stack_is_empty());
    assert!(!gf.g.can_cast_sorceries(0));
    assert!(gf.g.can_cast_instants(0));
    assert!(!gf.g.can_cast_instants(1));
}

#[test]
fn zones() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    assert!(gf.g.battlefield.get_type().is_battlefield());
    assert!(gf.g.stack.get_type().is_stack());
    assert!(gf.g.rules.get_type().is_rules());

    assert!(gf.g.hands[0].get_type().is_hand());
    assert_eq!(gf.g.hands[0].get_type().owner().unwrap(), 0);
    assert!(gf.g.hands[1].get_type().is_hand());
    assert_eq!(gf.g.hands[1].get_type().owner().unwrap(), 1);

    assert!(gf.g.libraries[0].get_type().is_library());
    assert_eq!(gf.g.libraries[0].get_type().owner().unwrap(), 0);
    assert!(gf.g.libraries[1].get_type().is_library());
    assert_eq!(gf.g.libraries[1].get_type().owner().unwrap(), 1);
}

#[test]
fn step_advancement() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.advance_to_next_option();
    assert_eq!(gf.get_game_state(), (Upkeep, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Upkeep, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Draw, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Draw, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Main, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Main, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (BeginningOfCombat, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (BeginningOfCombat, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (DeclareAttackers, 0, 0));
    gf.decline_attackers();
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (DeclareAttackers, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (EndOfCombat, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (EndOfCombat, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Main, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Main, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (End, 0, 0));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (End, 0, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Upkeep, 1, 1));
    gf.pass_priority_then_advance();
    assert_eq!(gf.get_game_state(), (Upkeep, 1, 0));
}

#[test]
fn tba_draw() {
    let of = OracleFrame::new();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    assert_eq!(gf.g.find_zone(Hand(0)).len(), 7);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    gf.advance_to_given_state(Draw, 0, 0);
    assert_eq!(gf.g.find_zone(Hand(0)).len(), 8);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 7);
    gf.advance_to_given_state(Draw, 1, 0);
    assert_eq!(gf.g.find_zone(Hand(1)).len(), 8);
}

#[test]
fn tba_untap() {
    let mut of = OracleFrame::new();
    let van = of.add_vanilla_creature();
    let mut gf = GameFrame::new(2, of);
    gf.g.begin();

    gf.advance_to_given_state(Main, 0, 0);
    let cre = gf.g.insert_by_name(Battlefield, &van, 0).clone();
    cre.borrow_mut().tapped = true;
    gf.advance_to_given_state(Upkeep, 1, 1);
    assert_eq!(cre.borrow_mut().tapped, true); // Does not untap on opponents turn
    gf.advance_to_given_state(Upkeep, 0, 0);
    assert_eq!(cre.borrow_mut().tapped, false);
}
