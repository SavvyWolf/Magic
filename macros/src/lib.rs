extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;

use proc_macro::TokenStream;

#[proc_macro_derive(TypedJson)]
pub fn typed_json_derive(input: TokenStream) -> TokenStream {
    // Construct a represntation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_typed_json_macro(&ast)
}

fn impl_typed_json_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl TypedJson for #name {
            fn type_name(&self) -> &'static str {
                stringify!(#name)
            }
        }
    };
    gen.into()
}


#[proc_macro_derive(ActionBoxClone)]
pub fn action_box_clone_derive(input: TokenStream) -> TokenStream {
    // Construct a represntation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_action_box_clone_macro(&ast)
}

fn impl_action_box_clone_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl ActionBoxClone for #name {
            fn box_clone(&self) -> Box<dyn Action> {
                Box::new(self.clone())
            }

            fn as_any(&self) -> &dyn Any {
                self
            }
        }
    };
    gen.into()
}


#[proc_macro_derive(SEBoxClone)]
pub fn se_clone_derive(input: TokenStream) -> TokenStream {
    // Construct a represntation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_se_box_clone_macro(&ast)
}

fn impl_se_box_clone_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl SEBoxClone for #name {
            fn box_clone(&self) -> Box<dyn StaticEffect> {
                Box::new(self.clone())
            }
        }
    };
    gen.into()
}
